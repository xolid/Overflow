/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Actividad;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class ActividadFacade {

    private Session session;

    public ActividadFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Actividad getActividadByID(int idactividad) {
        Actividad actividad = (Actividad) session.get(Actividad.class, idactividad);
        return actividad;
    }

    public Integer saveActividad(Actividad actividad) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(actividad);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateActividad(Actividad actividad) {
        try {
            session.getTransaction().begin();
            Actividad o = getActividadByID(actividad.getIdactividad());
            o.setOrden(actividad.getOrden());
            o.setNombre(actividad.getNombre());
            o.setDescripcion(actividad.getDescripcion());
            o.setConsideraciones(actividad.getConsideraciones());
            o.setEntregables(actividad.getEntregables());
            o.setDuracion(actividad.getDuracion());
            o.setProceso(actividad.getProceso());
            o.setGrupo(actividad.getGrupo());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteActividad(Actividad actividad) {
        try {
            session.getTransaction().begin();
            Actividad o = getActividadByID(actividad.getIdactividad());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }
    
    public List<Actividad> getActividadByProceso(int idproceso) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Actividad as actividad where proceso_idproceso = :idproceso order by orden");
        q.setInteger("idproceso", idproceso);
        List<Actividad> actividads = (List<Actividad>) q.list();
        session.getTransaction().commit();
        return actividads;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Actividad as actividad");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Actividad> getActividadForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Actividad as actividad where ((nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Actividad> actividads = (List<Actividad>) q.list();
        session.getTransaction().commit();
        return actividads;
    }
    
    public Long getActividadForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Actividad as actividad where ((nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Actividad> getActividadForProcessByProceso(String buscar, int idproceso, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Actividad as actividad where ((nombre like :buscar)) and proceso_idproceso = :idproceso order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idproceso",idproceso);
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Actividad> actividads = (List<Actividad>) q.list();
        session.getTransaction().commit();
        return actividads;
    }
    
    public Long getActividadForProcessCountByProceso(String buscar, int idproceso) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Actividad as actividad where ((nombre like :buscar)) and proceso_idproceso = :idproceso");
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idproceso",idproceso);
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }    

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

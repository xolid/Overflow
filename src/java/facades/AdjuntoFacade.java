/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Adjunto;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class AdjuntoFacade {

    private Session session;

    public AdjuntoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Adjunto getAdjuntoByID(int idadjunto) {
        Adjunto adjunto = (Adjunto) session.get(Adjunto.class, idadjunto);
        return adjunto;
    }

    public Integer saveAdjunto(Adjunto adjunto) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(adjunto);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateAdjunto(Adjunto adjunto) {
        try {
            session.getTransaction().begin();
            Adjunto o = getAdjuntoByID(adjunto.getIdadjunto());
            o.setNombre(adjunto.getNombre());
            o.setContenido(adjunto.getContenido());
            o.setFecha(adjunto.getFecha());
            o.setFlujo(adjunto.getFlujo());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteAdjunto(Adjunto adjunto) {
        try {
            session.getTransaction().begin();
            Adjunto o = getAdjuntoByID(adjunto.getIdadjunto());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Adjunto as adjunto");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Adjunto> getAdjuntoForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Adjunto as adjunto where ((nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Adjunto> adjuntos = (List<Adjunto>) q.list();
        session.getTransaction().commit();
        return adjuntos;
    }
    
    public Long getAdjuntoForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Adjunto as adjunto where ((nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Adjunto> getAdjuntoByFlujo(int idflujo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Adjunto as adjunto where flujo_idflujo = :idflujo");
        q.setInteger("idflujo", idflujo);
        List<Adjunto> adjuntos = (List<Adjunto>) q.list();
        session.getTransaction().commit();
        return adjuntos;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

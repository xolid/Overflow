/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Asignacion;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class AsignacionFacade {

    private Session session;

    public AsignacionFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Asignacion getAsignacionByID(int idasignacion) {
        Asignacion asignacion = (Asignacion) session.get(Asignacion.class, idasignacion);
        return asignacion;
    }

    public Integer saveAsignacion(Asignacion asignacion) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(asignacion);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateAsignacion(Asignacion asignacion) {
        try {
            session.getTransaction().begin();
            Asignacion o = getAsignacionByID(asignacion.getIdasignacion());
            o.setFinicio(asignacion.getFinicio());
            o.setFtermino(asignacion.getFtermino());
            o.setGrupo(asignacion.getGrupo());
            o.setUsuario(asignacion.getUsuario());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteAsignacion(Asignacion asignacion) {
        try {
            session.getTransaction().begin();
            Asignacion o = getAsignacionByID(asignacion.getIdasignacion());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }
    
    public List<Asignacion> getAsignacionByGrupo(int idgrupo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Asignacion as asignacion where grupo_idgrupo = :idgrupo");
        q.setInteger("idgrupo", idgrupo);
        List<Asignacion> asignacions = (List<Asignacion>) q.list();
        session.getTransaction().commit();
        return asignacions;
    }
    
    public List<Asignacion> getAsignacionByUsuario(int idusuario) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Asignacion as asignacion where usuario_idusuario = :idusuario");
        q.setInteger("idusuario", idusuario);
        List<Asignacion> asignacions = (List<Asignacion>) q.list();
        session.getTransaction().commit();
        return asignacions;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Asignacion as asignacion");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Asignacion> getAsignacionForProcessByGrupo(String buscar, int idgrupo, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Asignacion as asignacion where ((finicio like :buscar) or (ftermino like :buscar)) and grupo_idgrupo = :idgrupo order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idgrupo",idgrupo);
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Asignacion> asignacions = (List<Asignacion>) q.list();
        session.getTransaction().commit();
        return asignacions;
    }
    
    public Long getAsignacionForProcessCountByGrupo(String buscar, int idgrupo) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Asignacion as asignacion where ((finicio like :buscar) or (ftermino like :buscar)) and grupo_idgrupo = :idgrupo");
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idgrupo",idgrupo);
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }    
    
    public List<String> getAsignacionListMailByGrupo(int idgrupo) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT " +
                "usuario.mail " +
                "FROM " +
                "asignacion " +
                "inner join grupo on asignacion.grupo_idgrupo = grupo.idgrupo " +
                "inner join usuario on asignacion.usuario_idusuario = usuario.idusuario " +
                "WHERE " +
                "grupo.idgrupo = :idgrupo");
        q.setInteger("idgrupo", idgrupo);
        List<String> asignaciones = (List<String>) q.list();
        session.getTransaction().commit();
        return asignaciones;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Converzacion;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class ConverzacionFacade {

    private Session session;

    public ConverzacionFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Converzacion getConverzacionByID(int idconverzacion) {
        Converzacion converzacion = (Converzacion) session.get(Converzacion.class, idconverzacion);
        return converzacion;
    }

    public Integer saveConverzacion(Converzacion converzacion) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(converzacion);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateConverzacion(Converzacion converzacion) {
        try {
            session.getTransaction().begin();
            Converzacion o = getConverzacionByID(converzacion.getIdconverzacion());
            o.setFcreacion(converzacion.getFcreacion());
            o.setFtermino(converzacion.getFtermino());
            o.setPrioridad(converzacion.getPrioridad());
            o.setMotivo(converzacion.getMotivo());
            o.setUsuario(converzacion.getUsuario());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteConverzacion(Converzacion converzacion) {
        try {
            session.getTransaction().begin();
            Converzacion o = getConverzacionByID(converzacion.getIdconverzacion());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Converzacion> getConverzacionAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Converzacion as converzacion order by fcreacion desc");
        List<Converzacion> converzacions = (List<Converzacion>) q.list();
        session.getTransaction().commit();
        return converzacions;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Converzacion as converzacion");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Object[]> getConverzacionByPrioridadANDUsuario(int prioridad, int idusuario) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("select " +
                    "converzacion.idconverzacion, " +
                    "converzacion.fcreacion, " +
                    "converzacion.motivo, " +
                    "converzacion.usuario_idusuario " +
                    "from " +
                    "converzacion " +
                    "left join " +
                    "invitado on invitado.converzacion_idconverzacion = converzacion.idconverzacion " +
                    "where " +
                    "converzacion.prioridad = :prioridad and " +
                    "converzacion.ftermino is null and " +
                    "(converzacion.usuario_idusuario = :idusuario  or invitado.usuario_idusuario = :idusuario) " +
                    "group by " +
                    "converzacion.idconverzacion " +
                    "order by " +
                    "converzacion.fcreacion desc");
        q.setInteger("prioridad",prioridad);
        q.setInteger("idusuario",idusuario);
        List<Object[]> converzacions = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return converzacions;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

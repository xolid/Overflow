/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Flujo;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import java.util.Date;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class FlujoFacade {

    private Session session;

    public FlujoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Flujo getFlujoByID(int idflujo) {
        Flujo flujo = (Flujo) session.get(Flujo.class, idflujo);
        return flujo;
    }

    public Integer saveFlujo(Flujo flujo) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(flujo);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateFlujo(Flujo flujo) {
        try {
            session.getTransaction().begin();
            Flujo o = getFlujoByID(flujo.getIdflujo());
            o.setNombre(flujo.getNombre());
            o.setDescripcion(flujo.getDescripcion());
            o.setFcreado(flujo.getFcreado());
            o.setFrequerido(flujo.getFrequerido());
            o.setFsolicitado(flujo.getFsolicitado());
            o.setFentregado(flujo.getFentregado());
            o.setFterminado(flujo.getFterminado());
            o.setEstado(flujo.getEstado());
            o.setUsuario(flujo.getUsuario());
            o.setProceso(flujo.getProceso());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    public void deleteFlujo(Flujo flujo) {
        try {
            session.getTransaction().begin();
            Flujo o = getFlujoByID(flujo.getIdflujo());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Flujo as flujo");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Flujo> getFlujoForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Flujo as flujo where ((nombre like :buscar) or (descripcion like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Flujo> flujos = (List<Flujo>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public Long getFlujoForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Flujo as flujo where ((nombre like :buscar) or (descripcion like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Flujo> getFlujoForProcessByUsuario(String buscar, int idusuario, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Flujo as flujo where (CONVERT(idflujo,char) LIKE :buscar or (nombre like :buscar) or (descripcion like :buscar) or (fsolicitado like :buscar)) and usuario_idusuario = :idusuario and estado < 6 order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idusuario",idusuario);
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Flujo> flujos = (List<Flujo>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public Long getFlujoForProcessCountByUsuario(String buscar, int idusuario) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Flujo as flujo where (CONVERT(idflujo,char) LIKE :buscar or (nombre like :buscar) or (descripcion like :buscar) or (fsolicitado like :buscar)) and usuario_idusuario = :idusuario and estado < 6");
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idusuario",idusuario);
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }    
    
    public List<Flujo> getFlujoForProcessByUsuarioFINISHED(String buscar, int idusuario, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Flujo as flujo where (CONVERT(idflujo,char) LIKE :buscar or (nombre like :buscar) or (descripcion like :buscar) or (fsolicitado like :buscar)) and usuario_idusuario = :idusuario and estado > 5 order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idusuario",idusuario);
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Flujo> flujos = (List<Flujo>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public Long getFlujoForProcessCountByUsuarioFINISHED(String buscar, int idusuario) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Flujo as flujo where (CONVERT(idflujo,char) LIKE :buscar or (nombre like :buscar) or (descripcion like :buscar) or (fsolicitado like :buscar)) and usuario_idusuario = :idusuario and estado > 5");
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idusuario",idusuario);
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Flujo> getFlujoForProcessFINISHED(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Flujo as flujo where (CONVERT(idflujo,char) LIKE :buscar or (nombre like :buscar) or (descripcion like :buscar) or (fsolicitado like :buscar)) and estado > 5 order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Flujo> flujos = (List<Flujo>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public Long getFlujoForProcessCountFINISHED(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Flujo as flujo where (CONVERT(idflujo,char) LIKE :buscar or (nombre like :buscar) or (descripcion like :buscar) or (fsolicitado like :buscar)) and estado > 5");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Flujo> getFlujoByUsuario(int idusuario) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Flujo as flujo where usuario_idusuario = :idusuario and estado < 6");
        q.setInteger("idusuario",idusuario);
        List<Flujo> flujos = (List<Flujo>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public List<Object[]> getFlujoByEstadoCountANDGroupBy(int estado) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT proceso.nombre,count(*) FROM flujo inner join proceso on flujo.proceso_idproceso = proceso.idproceso where estado = :estado group by proceso.idproceso");
        q.setInteger("estado",estado);
        java.util.List<Object[]> flujos = (java.util.List<Object[]>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public List<Object[]> getFlujoInProgressCountANDGroupBy() {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT estado,count(*) FROM flujo where estado < 6 group by estado");
        java.util.List<Object[]> flujos = (java.util.List<Object[]>) q.list();
        session.getTransaction().commit();
        return flujos;
    }
    
    public List<Flujo> getListFlowExpire1Day(Date antes,Date ahora){
        session.getTransaction().begin();
        Query q = session.createQuery("FROM Flujo flujo WHERE estado < 6 AND frequerido BETWEEN :antes AND :ahora");
        q.setDate("antes",antes);
        q.setDate("ahora",ahora);
        java.util.List<Flujo> flujos = (java.util.List<Flujo>) q.list();
        session.getTransaction().commit();
        return flujos;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

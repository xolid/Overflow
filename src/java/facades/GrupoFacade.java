/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Grupo;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class GrupoFacade {

    private Session session;

    public GrupoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Grupo getGrupoByID(int idgrupo) {
        Grupo grupo = (Grupo) session.get(Grupo.class, idgrupo);
        return grupo;
    }

    public Integer saveGrupo(Grupo grupo) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(grupo);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateGrupo(Grupo grupo) {
        try {
            session.getTransaction().begin();
            Grupo o = getGrupoByID(grupo.getIdgrupo());
            o.setNombre(grupo.getNombre());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteGrupo(Grupo grupo) {
        try {
            session.getTransaction().begin();
            Grupo o = getGrupoByID(grupo.getIdgrupo());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Grupo> getGrupoAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Grupo as grupo order by nombre");
        List<Grupo> grupos = (List<Grupo>) q.list();
        session.getTransaction().commit();
        return grupos;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Grupo as grupo");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Grupo> getGrupoForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Grupo as grupo where ((nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Grupo> grupos = (List<Grupo>) q.list();
        session.getTransaction().commit();
        return grupos;
    }
    
    public Long getGrupoForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Grupo as grupo where ((nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Invitado;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class InvitadoFacade {

    private Session session;

    public InvitadoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Invitado getInvitadoByID(int idinvitado) {
        Invitado invitado = (Invitado) session.get(Invitado.class, idinvitado);
        return invitado;
    }

    public Integer saveInvitado(Invitado invitado) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(invitado);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateInvitado(Invitado invitado) {
        try {
            session.getTransaction().begin();
            Invitado o = getInvitadoByID(invitado.getIdinvitado());
            o.setConverzacion(invitado.getConverzacion());
            o.setUsuario(invitado.getUsuario());
            o.setFingreso(invitado.getFingreso());
            o.setFegreso(invitado.getFegreso());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteInvitado(Invitado invitado) {
        try {
            session.getTransaction().begin();
            Invitado o = getInvitadoByID(invitado.getIdinvitado());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Invitado> getInvitadoAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Invitado as invitado");
        List<Invitado> invitados = (List<Invitado>) q.list();
        session.getTransaction().commit();
        return invitados;
    }
    
    public List<Invitado> getInvitadoByConverzacionALL(int idconverzacion) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Invitado as invitado where converzacion_idconverzacion = :idconverzacion order by fingreso");
        q.setInteger("idconverzacion", idconverzacion);
        List<Invitado> invitados = (List<Invitado>) q.list();
        session.getTransaction().commit();
        return invitados;
    }
    
    public List<Invitado> getInvitadoByConverzacionONLYEnabled(int idconverzacion) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Invitado as invitado where converzacion_idconverzacion = :idconverzacion and fegreso is null order by fingreso");
        q.setInteger("idconverzacion", idconverzacion);
        List<Invitado> invitados = (List<Invitado>) q.list();
        session.getTransaction().commit();
        return invitados;
    }
    
    public Invitado getInvitadoByConverzacionANDUsuario(int idconverzacion, int idusuario) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Invitado as invitado where converzacion_idconverzacion = :idconverzacion and usuario_idusuario = :idusuario");
        q.setInteger("idconverzacion", idconverzacion);
        q.setInteger("idusuario", idusuario);
        Invitado invitados = (Invitado) q.uniqueResult();
        session.getTransaction().commit();
        return invitados;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Invitado as invitado");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

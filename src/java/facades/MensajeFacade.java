/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Mensaje;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class MensajeFacade {

    private Session session;

    public MensajeFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Mensaje getMensajeByID(int idmensaje) {
        Mensaje mensaje = (Mensaje) session.get(Mensaje.class, idmensaje);
        return mensaje;
    }

    public Integer saveMensaje(Mensaje mensaje) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(mensaje);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateMensaje(Mensaje mensaje) {
        try {
            session.getTransaction().begin();
            Mensaje o = getMensajeByID(mensaje.getIdmensaje());
            o.setConverzacion(mensaje.getConverzacion());
            o.setUsuario(mensaje.getUsuario());
            o.setFecha(mensaje.getFecha());
            o.setMensaje(mensaje.getMensaje());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteMensaje(Mensaje mensaje) {
        try {
            session.getTransaction().begin();
            Mensaje o = getMensajeByID(mensaje.getIdmensaje());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Mensaje> getMensajeAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Mensaje as mensaje order by fecha desc");
        List<Mensaje> mensajes = (List<Mensaje>) q.list();
        session.getTransaction().commit();
        return mensajes;
    }
    
    public List<Mensaje> getMensajeByConverzacion(int idconverzacion) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Mensaje as mensaje where converzacion_idconverzacion = :idconverzacion order by fecha desc");
        q.setInteger("idconverzacion", idconverzacion);
        List<Mensaje> mensajes = (List<Mensaje>) q.list();
        session.getTransaction().commit();
        return mensajes;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Mensaje as mensaje");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

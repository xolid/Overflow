/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Proceso;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class ProcesoFacade {

    private Session session;

    public ProcesoFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Proceso getProcesoByID(int idproceso) {
        Proceso proceso = (Proceso) session.get(Proceso.class, idproceso);
        return proceso;
    }

    public Integer saveProceso(Proceso proceso) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(proceso);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateProceso(Proceso proceso) {
        try {
            session.getTransaction().begin();
            Proceso o = getProcesoByID(proceso.getIdproceso());
            o.setNombre(proceso.getNombre());
            o.setDescripcion(proceso.getDescripcion());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteProceso(Proceso proceso) {
        try {
            session.getTransaction().begin();
            Proceso o = getProcesoByID(proceso.getIdproceso());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Proceso> getProcesoAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Proceso as proceso order by nombre");
        List<Proceso> procesos = (List<Proceso>) q.list();
        session.getTransaction().commit();
        return procesos;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Proceso as proceso");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public Integer getSiguienteOrdenByProceso(int idproceso) {
        session.getTransaction().begin();
        Query q = session.createQuery("select max(orden) from Actividad as actividad where proceso_idproceso = :idproceso order by nombre");
        q.setInteger("idproceso", idproceso);
        Integer orden = (Integer) q.uniqueResult();
        session.getTransaction().commit();
        return orden;
    }
 
    public List<Proceso> getProcesoForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Proceso as proceso where ((nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Proceso> procesos = (List<Proceso>) q.list();
        session.getTransaction().commit();
        return procesos;
    }
    
    public Long getProcesoForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Proceso as proceso where ((nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

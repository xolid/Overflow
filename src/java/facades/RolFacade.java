/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Rol;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;

/**
 *
 * @author solid
 */
public class RolFacade {

    private Session session;

    public RolFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Rol getRolByID(int idrol) {
        Rol rol = (Rol) session.get(Rol.class, idrol);
        return rol;
    }

    public Integer saveRol(Rol rol) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(rol);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateRol(Rol rol) {
        try {
            session.getTransaction().begin();
            Rol o = getRolByID(rol.getIdrol());
            o.setNombre(rol.getNombre());
            o.setConsulta(rol.getConsulta());
            o.setEjecuta(rol.getEjecuta());
            o.setDelega(rol.getDelega());
            o.setRedacta(rol.getRedacta());
            o.setAdministra(rol.getAdministra());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteRol(Rol rol) {
        try {
            session.getTransaction().begin();
            Rol o = getRolByID(rol.getIdrol());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public List<Rol> getRolAll() {
        session.getTransaction().begin();
        Query q = session.createQuery("from Rol as rol order by nombre");
        List<Rol> rols = (List<Rol>) q.list();
        session.getTransaction().commit();
        return rols;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Rol as rol");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
 
    public List<Rol> getRolForProcess(String buscar, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Rol as rol where ((nombre like :buscar)) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Rol> rols = (List<Rol>) q.list();
        session.getTransaction().commit();
        return rols;
    }
    
    public Long getRolForProcessCount(String buscar) {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Rol as rol where ((nombre like :buscar))");
        q.setString("buscar", "%"+buscar+"%");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

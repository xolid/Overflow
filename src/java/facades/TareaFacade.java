/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import java.util.List;
import beans.Tarea;
import org.hibernate.Query;
import org.hibernate.Session;
import hibernate.HibernateUtil;
import java.util.Date;
import org.hibernate.SQLQuery;

/**
 *
 * @author solid
 */
public class TareaFacade {

    private Session session;

    public TareaFacade() {
        session = HibernateUtil.getSessionFactory().openSession();
    }

    public Tarea getTareaByID(int idtarea) {
        Tarea tarea = (Tarea) session.get(Tarea.class, idtarea);
        return tarea;
    }

    public Integer saveTarea(Tarea tarea) {
        Integer identifier = 0;
        try {
            session.getTransaction().begin();
            identifier = (Integer) session.save(tarea);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return identifier;
    }

    public void updateTarea(Tarea tarea) {
        try {
            session.getTransaction().begin();
            Tarea o = getTareaByID(tarea.getIdtarea());
            o.setEstado(tarea.getEstado());
            o.setFrecibido(tarea.getFrecibido());
            o.setFasignado(tarea.getFasignado());
            o.setFproceso(tarea.getFproceso());
            o.setFliberado(tarea.getFliberado());
            o.setFlujo(tarea.getFlujo());
            o.setUsuario(tarea.getUsuario());
            o.setActividad(tarea.getActividad());
            session.update(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteTarea(Tarea tarea) {
        try {
            session.getTransaction().begin();
            Tarea o = getTareaByID(tarea.getIdtarea());
            session.delete(o);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }
    
    public List<Tarea> getTareaByFlujo(int idflujo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tarea as tarea where flujo_idflujo = :idflujo");
        q.setInteger("idflujo", idflujo);
        List<Tarea> tareas = (List<Tarea>) q.list();
        session.getTransaction().commit();
        return tareas;
    }
    
    public Tarea getNextTareaByFlujo(int idflujo, int idtarea) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tarea as tarea where flujo_idflujo = :idflujo and idtarea > :idtarea");
        q.setInteger("idflujo", idflujo);
        q.setInteger("idtarea", idtarea);
        q.setMaxResults(1);
        Tarea tarea = (Tarea) q.uniqueResult();
        session.getTransaction().commit();
        return tarea;
    }
    
    public Tarea getFirstTareaByFlujo(int idflujo) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tarea as tarea where flujo_idflujo = :idflujo order by idtarea");
        q.setInteger("idflujo", idflujo);
        q.setMaxResults(1);
        Tarea tarea = (Tarea) q.uniqueResult();
        session.getTransaction().commit();
        return tarea;
    }
    
    public Tarea getPreviousTareaByFlujo(int idflujo, int idtarea) {
        session.getTransaction().begin();
        Query q = session.createQuery("from Tarea as tarea where flujo_idflujo = :idflujo and idtarea < :idtarea order by idtarea DESC");
        q.setInteger("idflujo", idflujo);
        q.setInteger("idtarea", idtarea);
        q.setMaxResults(1);
        Tarea tarea = (Tarea) q.uniqueResult();
        session.getTransaction().commit();
        return tarea;
    }

    public long getTotal() {
        session.getTransaction().begin();
        Query q = session.createQuery("select count(*) from Tarea as tarea");
        long total = (Long) q.uniqueResult();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Object[]> getTareaProcessEjecutar(String buscar, int idusuario, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT " +
                    "tarea.idtarea as idtarea, " +
                    "proceso.nombre as pnombre," +
                    "actividad.nombre as anombre," +
                    "tarea.fasignado as fasignado, " +
                    "tarea.fproceso as fproceso, " +
                    "tarea.estado as estado " +
                    "FROM " +
                    "tarea " +
                    "inner join flujo on tarea.flujo_idflujo = flujo.idflujo " +
                    "inner join actividad on tarea.actividad_idactividad = actividad.idactividad " +
                    "inner join proceso on actividad.proceso_idproceso = proceso.idproceso " +
                    "where " +
                    "((CONVERT(tarea.idtarea,char) LIKE :buscar) or (actividad.nombre like :buscar) or (flujo.nombre like :buscar)) and " +
                    "tarea.usuario_idusuario = :idusuario and (tarea.estado > 0 and tarea.estado < 4) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idusuario", idusuario);
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Object[]> tareas = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return tareas;
    }
    
    public Long getTareaCountEjecutar(String buscar, int idusuario) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT " +
                    "count(*) " +
                    "FROM " +
                    "tarea " +
                    "inner join flujo on tarea.flujo_idflujo = flujo.idflujo " +
                    "inner join actividad on tarea.actividad_idactividad = actividad.idactividad " +
                    "inner join proceso on actividad.proceso_idproceso = proceso.idproceso " +
                    "where " +
                    "((CONVERT(tarea.idtarea,char) LIKE :buscar) or (actividad.nombre like :buscar) or (flujo.nombre like :buscar)) and " +
                    "tarea.usuario_idusuario = :idusuario and (tarea.estado > 0 and tarea.estado < 4)");
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idusuario", idusuario);
        long total = ((java.math.BigInteger)q.uniqueResult()).longValue();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Object[]> getTareaProcessEjecutarLibre(String buscar, int idusuario, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT " +
                    "tarea.idtarea as idtarea, " +
                    "proceso.nombre as pnombre," +
                    "actividad.nombre as anombre," +
                    "tarea.frecibido as frecibido, " +
                    "tarea.estado as estado " +
                    "FROM " +
                    "tarea " +
                    "inner join flujo on tarea.flujo_idflujo = flujo.idflujo " +
                    "inner join actividad on tarea.actividad_idactividad = actividad.idactividad " +
                    "inner join proceso on actividad.proceso_idproceso = proceso.idproceso " +
                    "inner join grupo on actividad.grupo_idgrupo = grupo.idgrupo  " +
                    "inner join asignacion on asignacion.grupo_idgrupo = grupo.idgrupo  " +
                    "where " +
                    "((CONVERT(tarea.idtarea,char) LIKE :buscar) or (actividad.nombre like :buscar) or (flujo.nombre like :buscar)) and " +
                    "asignacion.usuario_idusuario = :idusuario and (tarea.estado = 1) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idusuario", idusuario);
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Object[]> tareas = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return tareas;
    }
    
    public Long getTareaCountEjecutarLibre(String buscar, int idusuario) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT " +
                    "count(*) " +
                    "FROM " +
                    "tarea " +
                    "inner join flujo on tarea.flujo_idflujo = flujo.idflujo " +
                    "inner join actividad on tarea.actividad_idactividad = actividad.idactividad " +
                    "inner join proceso on actividad.proceso_idproceso = proceso.idproceso " +
                    "inner join grupo on actividad.grupo_idgrupo = grupo.idgrupo  " +
                    "inner join asignacion on asignacion.grupo_idgrupo = grupo.idgrupo  " +
                    "where " +
                    "((CONVERT(tarea.idtarea,char) LIKE :buscar) or (actividad.nombre like :buscar) or (flujo.nombre like :buscar)) and " +
                    "asignacion.usuario_idusuario = :idusuario and (tarea.estado = 1)");
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idusuario", idusuario);
        long total = ((java.math.BigInteger)q.uniqueResult()).longValue();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Object[]> getTareaProcessDelegar(String buscar, int idusuario, String colname, String dir, int index, int size) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT " +
            "tarea.`idtarea` AS idtarea, " +
            "proceso.`nombre` AS pnombre, " +
            "actividad.`nombre` AS anombre, " +
            "tarea.`frecibido` AS frecibido, " +
            "tarea.`fasignado` AS fasignado, " +
            "usuario_asignado.`nombre` AS unombre, " +
            "tarea.`estado` AS estado " +
            "FROM " +
            "`actividad` actividad INNER JOIN `tarea` tarea ON actividad.`idactividad` = tarea.`actividad_idactividad` " +
            "INNER JOIN `grupo` grupo ON actividad.`grupo_idgrupo` = grupo.`idgrupo` " +
            "INNER JOIN `proceso` proceso ON actividad.`proceso_idproceso` = proceso.`idproceso` " +
            "INNER JOIN `asignacion` asignacion ON grupo.`idgrupo` = asignacion.`grupo_idgrupo` " +
            "INNER JOIN `usuario` usuario ON asignacion.`usuario_idusuario` = usuario.`idusuario` " +
            "LEFT JOIN `usuario` usuario_asignado ON tarea.`usuario_idusuario` = usuario_asignado.`idusuario` " +
            "WHERE " +
            "usuario.`idusuario` = :idusuario and " +
            "((CONVERT(tarea.idtarea,char) LIKE :buscar) or (actividad.nombre like :buscar) or (proceso.nombre like :buscar)) and " +
            "(tarea.estado > 0 and tarea.estado < 4) order by "+colname+" "+dir);
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idusuario", idusuario);
        q.setFirstResult(index);
        q.setMaxResults(size);
        List<Object[]> tareas = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return tareas;
    }
    
    public Long getTareaCountDelegar(String buscar, int idusuario) {
        session.getTransaction().begin();
         SQLQuery q = session.createSQLQuery("SELECT " +
            "count(*) " +
            "FROM " +
            "`actividad` actividad INNER JOIN `tarea` tarea ON actividad.`idactividad` = tarea.`actividad_idactividad` " +
            "INNER JOIN `grupo` grupo ON actividad.`grupo_idgrupo` = grupo.`idgrupo` " +
            "INNER JOIN `proceso` proceso ON actividad.`proceso_idproceso` = proceso.`idproceso` " +
            "INNER JOIN `asignacion` asignacion ON grupo.`idgrupo` = asignacion.`grupo_idgrupo` " +
            "INNER JOIN `usuario` usuario ON asignacion.`usuario_idusuario` = usuario.`idusuario` " +
            "LEFT JOIN `usuario` usuario_asignado ON tarea.`usuario_idusuario` = usuario_asignado.`idusuario` " +
            "WHERE " +
            "usuario.`idusuario` = :idusuario and " +
            "((CONVERT(tarea.idtarea,char) LIKE :buscar) or (actividad.nombre like :buscar) or (proceso.nombre like :buscar)) and " +
            "(tarea.estado > 0 and tarea.estado < 4)");
        q.setString("buscar", "%"+buscar+"%");
        q.setInteger("idusuario", idusuario);
        long total = ((java.math.BigInteger)q.uniqueResult()).longValue();
        session.getTransaction().commit();
        return total;
    }
    
    public List<Object[]> getTareaByUsuarioOFGroup(int idusuario) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT " +
            "tarea.`idtarea` AS idtarea, " +
            "tarea.`frecibido` AS frecibido, " +
            "tarea.`fasignado` AS fasignado, " +
            "tarea.`fproceso` AS fproceso, " +
            "usuario_asignado.`nombre` AS unombre, " +
            "tarea.`estado` AS estado, " +
            "flujo.`frequerido` AS frequerido " +
            "FROM " +
            "`actividad` actividad INNER JOIN `tarea` tarea ON actividad.`idactividad` = tarea.`actividad_idactividad` " +
            "INNER JOIN `grupo` grupo ON actividad.`grupo_idgrupo` = grupo.`idgrupo` " +
            "INNER JOIN `asignacion` asignacion ON grupo.`idgrupo` = asignacion.`grupo_idgrupo` " +
            "INNER JOIN `usuario` usuario ON asignacion.`usuario_idusuario` = usuario.`idusuario` " +
            "INNER JOIN `flujo` flujo ON tarea.`flujo_idflujo` = flujo.`idflujo` " +
            "LEFT JOIN `usuario` usuario_asignado ON tarea.`usuario_idusuario` = usuario_asignado.`idusuario` " +
            "WHERE " +
            "usuario.`idusuario` = :idusuario and (tarea.estado > 0 and tarea.estado < 4)");
        q.setInteger("idusuario", idusuario);
        List<Object[]> tareas = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return tareas;
    }
    
    public List<Object[]> getTareaByUsuarioOFGroupLAST50(int idusuario) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT " +
            "tarea.`idtarea` AS idtarea, " +
            "tarea.`frecibido` AS frecibido, " +
            "tarea.`fasignado` AS fasignado, " +
            "tarea.`fproceso` AS fproceso, " +
            "tarea.`fliberado` AS fliberado, " +
            "usuario_asignado.`nombre` AS unombre, " +
            "tarea.`estado` AS estado " +
            "FROM " +
            "`actividad` actividad INNER JOIN `tarea` tarea ON actividad.`idactividad` = tarea.`actividad_idactividad` " +
            "INNER JOIN `grupo` grupo ON actividad.`grupo_idgrupo` = grupo.`idgrupo` " +
            "INNER JOIN `asignacion` asignacion ON grupo.`idgrupo` = asignacion.`grupo_idgrupo` " +
            "INNER JOIN `usuario` usuario ON asignacion.`usuario_idusuario` = usuario.`idusuario` " +
            "LEFT JOIN `usuario` usuario_asignado ON tarea.`usuario_idusuario` = usuario_asignado.`idusuario` " +
            "WHERE " +
            "usuario.`idusuario` = :idusuario and tarea.estado < 5 order by frecibido desc limit 50");
        q.setInteger("idusuario", idusuario);
        List<Object[]> tareas = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return tareas;
    }
    
    public List<Object[]> getTareaEjecutarBYUsuarioLIMIT_TIME(int idusuario) {
        session.getTransaction().begin();
        SQLQuery q = session.createSQLQuery("SELECT " +
                    "tarea.idtarea as idtarea, " +
                    "actividad.nombre as anombre," +
                    "tarea.fasignado as fasignado, " +
                    "tarea.fproceso as fproceso, " +
                    "CONCAT(FLOOR(HOUR(TIMEDIFF(fproceso,NOW()))/24),' dias ',MOD(HOUR(TIMEDIFF(fproceso,NOW())),24),' horas ',MINUTE(TIMEDIFF(fproceso,NOW())),' minutos') as restante, " +
                    "TIMESTAMPDIFF(MINUTE,NOW(),fproceso) as minutos " +
                    "FROM " +
                    "tarea " +
                    "inner join flujo on tarea.flujo_idflujo = flujo.idflujo " +
                    "inner join actividad on tarea.actividad_idactividad = actividad.idactividad " +
                    "where " +
                    "tarea.usuario_idusuario = :idusuario and (tarea.estado > 0 and tarea.estado < 4) order by minutos");
        q.setInteger("idusuario", idusuario);
        List<Object[]> tareas = (List<Object[]>) q.list();
        session.getTransaction().commit();
        return tareas;
    }
    
        
    public List<Tarea> getListTaskExpire1Hour(Date antes,Date ahora){
        session.getTransaction().begin();
        Query q = session.createQuery("FROM Tarea tarea WHERE fproceso BETWEEN :antes AND :ahora AND estado < 6");
        q.setDate("antes",antes);
        q.setDate("ahora",ahora);
        java.util.List<Tarea> tareas = (java.util.List<Tarea>) q.list();
        session.getTransaction().commit();
        return tareas;
    }

    public void close(){
        session.clear();
        session.close();
    }
    
    @Override
    public void finalize(){
        try {
            close();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) { }
        }
    }
    
}

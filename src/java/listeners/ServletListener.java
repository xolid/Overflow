/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listeners;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Web application lifecycle listener.
 *
 * @author czara
 */
public class ServletListener implements HttpSessionListener {

    private static int inActivas = 0;
    private static java.util.List<javax.servlet.http.HttpSession> sessions = new java.util.ArrayList();

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        sessions.add(se.getSession());
        inActivas++;
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        sessions.remove(se.getSession());
        if(inActivas>0)
            inActivas--;
    }

    public static int getActivas() {
        return inActivas;
    }

    public static java.util.List<javax.servlet.http.HttpSession> getSessions(){
        return sessions;
    }
    
    public static void closeSession(int idusuario){
        for(javax.servlet.http.HttpSession s:getSessions()){
            beans.Usuario u = (beans.Usuario)s.getAttribute("usuario");
            if(u!=null){
                if(u.getIdusuario()==idusuario){
                    s.invalidate();
                }
            }
        }
    }

}

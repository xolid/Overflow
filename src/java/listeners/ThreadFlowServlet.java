/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listeners;
import beans.Flujo;
import beans.Tarea;
import facades.FlujoFacade;
import facades.TareaFacade;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author czarate
 */
public class ThreadFlowServlet extends HttpServlet implements Runnable {
    
    private static final long serialVersionUID = 1L;

    @Override
    public void init() {
        try {
            Thread thread = new Thread(this);
            thread.start();
        } catch (Exception e) { }
    }

    @Override
    public void run() {
        while (true) {
            try {
                FlujoFacade facade = new FlujoFacade();
                Calendar fecha = Calendar.getInstance();
                fecha.add(Calendar.DAY_OF_MONTH,-1);
                for(Flujo flujo:facade.getListFlowExpire1Day(fecha.getTime(),new Date())){
                    String mensaje =  notifications.Template.notificationFlujo("La solicitud rebasara la fecha de requerido dentro de las proximas 24 horas.",flujo);
                    new notifications.EmailSender().sendEmail(flujo.getUsuario().getMail(),"Solicitud por expirar",mensaje);
                }
                facade.close();
                Thread.sleep(86400000);
            } catch (InterruptedException e) { }
        }
    }
    
}

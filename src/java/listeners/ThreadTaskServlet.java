/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listeners;
import beans.Tarea;
import facades.AsignacionFacade;
import facades.TareaFacade;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author czarate
 */
public class ThreadTaskServlet extends HttpServlet implements Runnable {
    
    private static final long serialVersionUID = 1L;

    @Override
    public void init() {
        try {
            Thread thread = new Thread(this);
            thread.start();
        } catch (Exception e) { }
    }

    @Override
    public void run() {
        while (true) {
            try {
                TareaFacade facade = new TareaFacade();
                AsignacionFacade asfacade = new AsignacionFacade();
                Calendar fecha = Calendar.getInstance();
                fecha.add(Calendar.HOUR_OF_DAY,-1);
                for(Tarea tarea:facade.getListTaskExpire1Hour(fecha.getTime(),new Date())){
                    String mensaje =  notifications.Template.notificationTarea("La tarea esta por expirar en 1 hora.",tarea);
                    if(tarea.getUsuario()!=null){
                        new notifications.EmailSender().sendEmail(tarea.getUsuario().getMail(),"Tarea por expirar",mensaje);
                    }else{
                        java.util.List<String> correos = asfacade.getAsignacionListMailByGrupo(tarea.getActividad().getGrupo().getIdgrupo());
                        new notifications.EmailSender().sendMultipleEmail(correos,"Tarea por expirar",mensaje);
                        
                    }
                }
                asfacade.close();
                facade.close();
                Thread.sleep(3600000);
            } catch (InterruptedException e) { }
        }
    }
    
}

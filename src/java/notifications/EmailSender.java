/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notifications;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.Session;

/**
 *
 * @author CarlosAlberto
 */
public class EmailSender {

    private final Session session;
    
    public EmailSender() {
        String smtpHostServer = "127.0.0.1";
        Properties props = System.getProperties();
        props.put("mail.smtp.host", smtpHostServer);
        session = Session.getInstance(props, null);
    }

    public void sendEmail(String toEmail, String subject, String body) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MimeMessage msg = new MimeMessage(session);
                    msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
                    msg.addHeader("format", "flowed");
                    msg.addHeader("Content-Transfer-Encoding", "8bit");
                    msg.setFrom(new InternetAddress("no_reply@overflow.com", "Notificaciones Overflow"));
                    msg.setReplyTo(InternetAddress.parse("no_reply@overflow.com", false));
                    msg.setSubject(subject, "UTF-8");
                    msg.setContent(body, "text/html; charset=utf-8");
                    msg.setSentDate(new Date());
                    msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
                    Transport.send(msg);
                } catch (UnsupportedEncodingException | MessagingException e) { }
            }
        }).start();
    }
    
    public void sendMultipleEmail(java.util.List<String> toEmail, String subject, String body) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(String mail:toEmail){
                    try {
                        MimeMessage msg = new MimeMessage(session);
                        msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
                        msg.addHeader("format", "flowed");
                        msg.addHeader("Content-Transfer-Encoding", "8bit");
                        msg.setFrom(new InternetAddress("no_reply@overflow.com", "Notificaciones Overflow"));
                        msg.setReplyTo(InternetAddress.parse("no_reply@overflow.com", false));
                        msg.setSubject(subject, "UTF-8");
                        msg.setContent(body, "text/html; charset=utf-8");
                        msg.setSentDate(new Date());
                        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail, false));
                        Transport.send(msg);
                    } catch (UnsupportedEncodingException | MessagingException e) { }
                }
            }
        }).start();
    }
    
}

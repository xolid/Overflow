/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

/**
 *
 * @author CarlosAlberto
 */
public class LogRegister {
    
    public LogRegister(){
        
    }
    
    public static void write(String dispara, String objetivo, String accion, beans.Flujo flujo){
        facades.RegistroFacade facade = new facades.RegistroFacade();
        beans.Registro registro = new beans.Registro();
        registro.setIdregistro(0);
        registro.setDispara(dispara);
        registro.setObjetivo(objetivo);
        registro.setAccion(accion);
        registro.setFecha(new java.util.Date());
        registro.setFlujo(flujo);
        facade.saveRegistro(registro);
        facade.close();
    }
    
}

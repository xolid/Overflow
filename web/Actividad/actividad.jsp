<%-- 
    Document   : actividad
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="actividad" class="beans.Actividad" scope="page"/>
<jsp:setProperty name="actividad" property="idactividad" />
<jsp:setProperty name="actividad" property="orden" />
<jsp:setProperty name="actividad" property="nombre" />
<jsp:setProperty name="actividad" property="descripcion" />
<jsp:setProperty name="actividad" property="consideraciones" />
<jsp:setProperty name="actividad" property="entregables" />
<jsp:setProperty name="actividad" property="duracion" />
<jsp:useBean id="facade" scope="page" class="facades.ActividadFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProcesoFacade"/>
<jsp:useBean id="rfacade" scope="page" class="facades.GrupoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("proceso_idproceso")).equals("")){
        actividad.setProceso(pfacade.getProcesoByID(Integer.parseInt(String.valueOf(request.getParameter("proceso_idproceso")))));
    }
    if(!String.valueOf(request.getParameter("grupo_idgrupo")).equals("")){
        actividad.setGrupo(rfacade.getGrupoByID(Integer.parseInt(String.valueOf(request.getParameter("grupo_idgrupo")))));
    }
    if(request.getParameter("accion").equals("1")){
        if(actividad.getIdactividad()==0){
            facade.saveActividad(actividad);
        }else{
            facade.updateActividad(actividad);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteActividad(actividad);        
    }
}
if(request.getParameter("id")!=null){
    int idactividad=Integer.parseInt((String)request.getParameter("id"));
    actividad=facade.getActividadByID(idactividad);
}else{
    beans.Proceso proceso = new beans.Proceso();
    if(request.getParameter("idproceso")!=null){
        int idproceso = Integer.parseInt(String.valueOf(request.getParameter("idproceso")));
        proceso = pfacade.getProcesoByID(idproceso);
    }
    actividad.setIdactividad(0);
    Integer orden = pfacade.getSiguienteOrdenByProceso(proceso.getIdproceso());
    if(orden ==  null){ 
        orden = 500; 
    }else{ 
        orden = orden+50;
    }
    actividad.setOrden(orden);
    actividad.setNombre("");
    actividad.setDescripcion("");
    actividad.setConsideraciones("");
    actividad.setEntregables("");
    actividad.setDuracion(0);
    actividad.setProceso(proceso);
    actividad.setGrupo(new beans.Grupo());
}%>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Catalogos Principales</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item"><a href="#" onclick="javascript:go2to('Proceso/procesoListado.jsp', 'page-wrapper');">Procesos</a></li>
                    <li class="breadcrumb-item"><a href="#" onclick="javascript:go2to('Actividad/actividadListado.jsp?idproceso=<%=actividad.getProceso().getIdproceso()%>', 'page-wrapper');">Actividades</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Actividad del Proceso: <%=actividad.getProceso().getNombre()%></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form actividade="form" id="form1" name="form1" action="actividad.jsp" method="post">
                            <input type="hidden" id="idactividad" name="idactividad" value="<%=actividad.getIdactividad()%>"/>
                            <input type="hidden" id="proceso_idproceso" name="proceso_idproceso" value="<%=actividad.getProceso().getIdproceso()%>"/>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="orden">Orden: </label>
                                        <input class="form-control input-sm" id="orden" name="orden" type="number" min="0" max="10000" step="1" value="<%=actividad.getOrden()%>" autocomplete="off" required autofocus/>
                                    </div>      
                                </div>
                                <div class="col-lg-10">
                                    <div class="form-group">
                                        <label for="nombre">Nombre: </label>
                                        <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=actividad.getNombre()%>" autocomplete="off" required/>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="descripcion">Descripcion de la Actividad: </label>
                                        <textarea id="descripcion" name="descripcion" class="form-control input-sm" rows="5" required><%=actividad.getDescripcion()%></textarea>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="consideraciones">Consideraciones en la Actividad: </label>
                                        <textarea id="consideraciones" name="consideraciones" class="form-control input-sm" rows="5"><%=actividad.getConsideraciones()==null?"":actividad.getConsideraciones()%></textarea>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="entregables">Entregables de la Actividad: </label>
                                        <textarea id="entregables" name="entregables" class="form-control input-sm" rows="5"><%=actividad.getEntregables()==null?"":actividad.getEntregables()%></textarea>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="duracion">Duracion en Horas: </label>
                                        <input class="form-control input-sm" id="duracion" name="duracion" type="number" min="0" max="1000" step="1" value="<%=actividad.getDuracion()%>" autocomplete="off" required/>
                                    </div>      
                                </div>
                                <div class="col-lg-10">
                                    <div class="form-group">
                                        <label for="grupo_idgrupo">Grupo Grupo: </label>
                                        <select id="grupo_idgrupo" name="grupo_idgrupo" class="form-control input-sm">
                                            <%for(beans.Grupo grupo:rfacade.getGrupoAll()){%>
                                                <option value="<%=grupo.getIdgrupo()%>" <%=grupo.getIdgrupo()==actividad.getGrupo().getIdgrupo()?"SELECTED":""%>><%=grupo.getNombre()%></option>
                                            <%}%>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>    
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:save('Actividad/actividad.jsp?idproceso=<%=actividad.getProceso().getIdproceso()%>','Actividad/actividadListado.jsp?idproceso=<%=actividad.getProceso().getIdproceso()%>','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                                <%if(actividad.getIdactividad()!=0){%>
                                    <button class="btn btn-outline-danger btn-sm" type="button" onclick="javascript:erase('Actividad/actividad.jsp?idproceso=<%=actividad.getProceso().getIdproceso()%>','Actividad/actividadListado.jsp?idproceso=<%=actividad.getProceso().getIdproceso()%>','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                                <%}%>
                                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="javascript:go2to('Actividad/actividadListado.jsp?idproceso=<%=actividad.getProceso().getIdproceso()%>','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<%
facade.close();
pfacade.close();
rfacade.close();
%>
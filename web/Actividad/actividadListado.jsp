<%-- 
    Document   : usuarioListado
    Created on : Jan 9, 2016, 4:50:18 PM
    Author     : czara
--%>

<jsp:useBean id="proceso" class="beans.Proceso" scope="page"/>
<jsp:useBean id="facade" scope="page" class="facades.ProcesoFacade"/>

<%if(request.getParameter("idproceso")!=null){
    int idproceso = Integer.parseInt((String)request.getParameter("idproceso"));
    proceso = facade.getProcesoByID(idproceso);
}%>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Catalogos Principales</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item"><a href="#" onclick="javascript:go2to('Proceso/procesoListado.jsp', 'page-wrapper');">Procesos</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Proceso: <small><%=proceso.getNombre()%></small></h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped table-bordered table-condensed table-hover" id="dataTablesActividad">
                                <thead>
                                    <th>Orden</th>
                                    <th>[B] Nombre</th>
                                    <th>Grupo</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:go2to('Actividad/actividad.jsp?idproceso=<%=proceso.getIdproceso()%>','page-wrapper');"><i class="fa fa-asterisk fa-fw"></i> Nuevo</button>
                                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="javascript:go2to('Proceso/procesoListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#dataTablesActividad').dataTable({
            "retrieve": true,
            "processing": true,  
            "serverSide": true,
            "ajax": "Actividad/processActividad.jsp?idproceso=<%=proceso.getIdproceso()%>",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "columns": [
                { "width": "10%" },
                { "width": "50%" },
                { "width": "40%" }
            ]
        });
    });
</script>

<%
facade.close();
%>
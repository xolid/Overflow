<%-- 
    Document   : process2
    Created on : Feb 7, 2016, 7:18:03 PM
    Author     : czara
--%>
 
<%@page import="java.util.*"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<jsp:useBean id="facade" scope="page" class="facades.ActividadFacade"/>

<%  
int idproceso = 0;
if(request.getParameter("idproceso")!=null){
    idproceso = Integer.parseInt((String)request.getParameter("idproceso"));
}    
    
String[] cols = { "orden","nombre","grupo_idgrupo" };
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();
int amount = 10;
int start = 0;
int echo = 0;
int col = 0;
String dir = "asc";
String sStart = request.getParameter("start");
String sAmount = request.getParameter("length");
String sEcho = request.getParameter("draw");
String sCol = request.getParameter("order[0][column]");
String sdir = request.getParameter("order[0][dir]");
String buscar = request.getParameter("search[value]");
if (sStart != null) {
    start = Integer.parseInt(sStart);
    if (start < 0)
        start = 0;
}
if (sAmount != null) {
    amount = Integer.parseInt(sAmount);
    if (amount < 10 || amount > 100)
        amount = 10;
}
if (sEcho != null) {
    echo = Integer.parseInt(sEcho);
}
if (sCol != null) {
    col = Integer.parseInt(sCol);
    if (col < 0 || col > 0)
        col = 0;
}
if (sdir != null) {
    if (!sdir.equals("asc"))
        dir = "desc";
}
String colName = cols[col];
long total = facade.getTotal();
List<beans.Actividad> actividades = facade.getActividadForProcessByProceso(buscar, idproceso, colName, dir, start, amount);
for(beans.Actividad p:actividades){
    JSONArray ja = new JSONArray();
    ja.add(p.getOrden());
    ja.add("<a href=\"#\" onclick=\"javascript:go2to('Actividad/actividad.jsp?id="+p.getIdactividad()+"','page-wrapper')\">"+p.getNombre()+"</a>");
    ja.add(p.getGrupo().getNombre());
    array.add(ja);
}
long totalAfterFilter = facade.getActividadForProcessCountByProceso(buscar, idproceso);
result.put("draw", sEcho);
result.put("recordsTotal", total);
result.put("recordsFiltered", totalAfterFilter);
result.put("data", array);
response.setContentType("application/json");
response.setHeader("Cache-Contactividad", "no-store");
out.print(result);

facade.close();    
%>


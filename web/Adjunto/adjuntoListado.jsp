<%-- 
    Document   : archivoListado
    Created on : 29-may-2018, 1:43:29
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.AdjuntoFacade"/>
<jsp:useBean id="ffacade" scope="page" class="facades.FlujoFacade"/>

<%int idflujo = 0;
if(request.getParameter("idflujo")!=null){
    idflujo = Integer.parseInt(String.valueOf(request.getParameter("idflujo")));
}
beans.Flujo flujo = ffacade.getFlujoByID(idflujo);%>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Adjuntos en el Flujo</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-sm table-striped table-bordered table-condensed table-hover" id="dataTablesAdjunto">
                            <thead>
                                <th>Fecha</th>
                                <th>Nombre</th>
                                <th>Subido</th>
                                <th>&nbsp;</th>
                            </thead>
                            <tbody>
                                <%for(beans.Adjunto adjunto:facade.getAdjuntoByFlujo(idflujo)){%>
                                    <tr>
                                        <td><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(adjunto.getFecha())%></td>
                                        <td><a href="/Overflow/Adjunto/descargar.jsp?idadjunto=<%=adjunto.getIdadjunto()%>" target="_blank"><%=adjunto.getNombre()%></a></td>
                                        <td><%=adjunto.getUsuario().getNombre()%></td>
                                        <%if(adjunto.getUsuario().getIdusuario()==((beans.Usuario)session.getAttribute("usuario")).getIdusuario()){%>
                                            <td><a href="#" onclick="javascript:borrar(<%=adjunto.getIdadjunto()%>)"><i class="fa fa-times fa-fw"></i> Borrar</a></td>
                                        <%}else{%>
                                            <td>&nbsp;</td>
                                        <%}%>
                                    </tr>
                                <%}%>
                            </tbody>
                        </table>     
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" data-toggle="modal" data-target="#myModalUpload"><i class="fa fa-upload fa-fw"></i> Subir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<div class="modal fade" id="myModalUpload">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Seleccionar Archivo</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/Adjunto/seleccionar.jsp?idflujo=<%=idflujo%>" width="100%" height="100">
                    <embed src="<%=request.getContextPath()%>/Adjunto/seleccionar.jsp?idflujo=<%=idflujo%>" width="100%" height="100"></embed>
                </object>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script lang="javascript">
        
    $(document).ready(function() {
        $('#dataTablesAdjunto').dataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "order": [[ 0,"desc"]],
            "lengthMenu": [[3, 5, 10, -1], [3, 5, 10, "All"]],
            "columns": [
                { "width": "15%" },
                { "width": "50%" },
                { "width": "25%" },
                { "width": "10%" }
            ]
        });
    });    
        
    function borrar(idadjunto){
        $.LoadingOverlay("show");  
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) { 
            if (xhr.readyState === 4 && xhr.status === 200) {
                var scs = xhr.responseText.extractScript();
                scs.evalScript();
                $.LoadingOverlay("hide");
            }
        };
        xhr.open("GET", "/Overflow/Adjunto/borrar.jsp?idadjunto="+idadjunto, true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    }
    
    window.closeModal = function(){
        $('#myModalUpload').modal('hide');
        setTimeout(function(){
            go2to('Adjunto/adjuntoListado.jsp?idflujo=<%=idflujo%>','adjuntoListado');
        },250);
    };
    
</script>

<%
facade.close();
ffacade.close();
%>
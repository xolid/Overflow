<%-- 
    Document   : borrar
    Created on : 29-may-2018, 1:58:09
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.AdjuntoFacade"/>

<%
int idadjunto = 0;
if(request.getParameter("idadjunto")!=null){
    idadjunto = Integer.parseInt(String.valueOf(request.getParameter("idadjunto")));
}
beans.Adjunto adjunto = facade.getAdjuntoByID(idadjunto);
facade.deleteAdjunto(adjunto);
tools.LogRegister.write(((beans.Usuario)session.getAttribute("usuario")).getNombre(),adjunto.getNombre(),"Elimina adjunto",adjunto.getFlujo());
%>

<script>
    go2to('Adjunto/adjuntoListado.jsp?idflujo=<%=adjunto.getFlujo().getIdflujo()%>','adjuntoListado');
</script>

<%
facade.close();
%>
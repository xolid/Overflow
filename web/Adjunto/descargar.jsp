<%-- 
    Document   : descargar
    Created on : 29-may-2018, 12:00:35
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.AdjuntoFacade"/>
 
<%
int idadjunto = 0;
if(request.getParameter("idadjunto")!=null){
    idadjunto = Integer.parseInt(String.valueOf(request.getParameter("idadjunto")));
}
beans.Adjunto adjunto = facade.getAdjuntoByID(idadjunto);

// gets file name and file blob data
String fileName = adjunto.getNombre();
java.sql.Blob blob = new javax.sql.rowset.serial.SerialBlob(adjunto.getContenido());
java.io.InputStream inputStream = blob.getBinaryStream();
int fileLength = inputStream.available();
ServletContext context = getServletContext();
// sets MIME type for the file download
String mimeType = context.getMimeType(fileName);
if (mimeType == null) {        
    mimeType = "application/octet-stream";
}              
// set content properties and header attributes for the response
response.setContentType(mimeType);
response.setContentLength(fileLength);
String headerKey = "Content-Disposition";
String headerValue = String.format("attachment; filename=\"%s\"", fileName);
response.setHeader(headerKey, headerValue);
// writes the file to the client
java.io.OutputStream outStream = response.getOutputStream();
byte[] buffer = new byte[4096];
int bytesRead = -1;
while ((bytesRead = inputStream.read(buffer)) != -1) {
    outStream.write(buffer, 0, bytesRead);
}
inputStream.close();
outStream.close();     

tools.LogRegister.write(((beans.Usuario)session.getAttribute("usuario")).getNombre(),adjunto.getNombre(),"Descarga adjunto",adjunto.getFlujo());
facade.close();
%>
<%-- 
    Document   : seleccionar
    Created on : 29-may-2018, 2:25:36
    Author     : solid
--%>

<%int idflujo = 0;
if(request.getParameter("idflujo")!=null){
    idflujo = Integer.parseInt(String.valueOf(request.getParameter("idflujo")));
}%>

<html>
    <head>
        <title>Herramientas de Busqueda</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/font-awesome/css/font-awesome.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adminlte.css"/>
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/img/favicon.ico"/>
    </head>
    <body>
        <form role="form" id="form1" name="form1" action="subir.jsp?idflujo=<%=idflujo%>" enctype="MULTIPART/FORM-DATA" method="post">
            <div class="form-group">
                <span class="btn btn-default btn-file">
                    Seleccionar <input type="file" name="file"> 
                </span>
                <input type="submit" value="Subir" class="btn btn-success"/>
            </div>
        </form>
        <!-- Core Scripts - Include with every page -->
        <!-- jQuery -->
        <script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<%=request.getContextPath()%>/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
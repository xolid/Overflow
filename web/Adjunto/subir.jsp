<%-- 
    Document   : subir
    Created on : 29-may-2018, 1:18:22
    Author     : solid
--%>

<%@ page import="java.util.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.*" %>
<%@ page import="java.io.*" %>

<jsp:useBean id="facade" scope="page" class="facades.AdjuntoFacade"/>
<jsp:useBean id="ffacade" scope="page" class="facades.FlujoFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
 
<%
int idflujo = 0;
if(request.getParameter("idflujo")!=null){
    idflujo = Integer.parseInt(String.valueOf(request.getParameter("idflujo")));
}
FileItemFactory file_factory = new DiskFileItemFactory();
ServletFileUpload servlet_up = new ServletFileUpload(file_factory);
servlet_up.setSizeMax(-1);
List items = servlet_up.parseRequest(request);
for(int i=0;i<items.size();i++){
    FileItem item = (FileItem) items.get(i);
    if (! item.isFormField()){
        beans.Adjunto adjunto = new beans.Adjunto();
        adjunto.setIdadjunto(0);
        adjunto.setNombre(item.getName());
        adjunto.setFecha(new java.util.Date());
        adjunto.setContenido(item.get());
        adjunto.setFlujo(ffacade.getFlujoByID(idflujo));
        adjunto.setUsuario(ufacade.getUsuarioByID(((beans.Usuario)session.getAttribute("usuario")).getIdusuario()));
        facade.saveAdjunto(adjunto);
        tools.LogRegister.write(((beans.Usuario)session.getAttribute("usuario")).getNombre(),adjunto.getNombre(),"Adjunta archivo",adjunto.getFlujo());
    }
}
ufacade.close();
ffacade.close();
facade.close();
%>

<script>
    window.parent.closeModal();
</script>
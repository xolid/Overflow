<%-- 
    Document   : asignacion
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="asignacion" class="beans.Asignacion" scope="page"/>
<jsp:setProperty name="asignacion" property="idasignacion" />
<jsp:useBean id="facade" scope="page" class="facades.AsignacionFacade"/>
<jsp:useBean id="rfacade" scope="page" class="facades.GrupoFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>

<%if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("grupo_idgrupo")).equals("")){
        asignacion.setGrupo(rfacade.getGrupoByID(Integer.parseInt(String.valueOf(request.getParameter("grupo_idgrupo")))));
    }
    if(!String.valueOf(request.getParameter("usuario_idusuario")).equals("")){
        asignacion.setUsuario(ufacade.getUsuarioByID(Integer.parseInt(String.valueOf(request.getParameter("usuario_idusuario")))));
    }
    if(!String.valueOf(request.getParameter("finicio")).equals("")){asignacion.setFinicio(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(request.getParameter("finicio"))));}
    if(!String.valueOf(request.getParameter("ftermino")).equals("")){asignacion.setFtermino(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(request.getParameter("ftermino"))));}
    if(request.getParameter("accion").equals("1")){
        if(asignacion.getIdasignacion()==0){
            facade.saveAsignacion(asignacion);
        }else{
            facade.updateAsignacion(asignacion);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteAsignacion(asignacion);        
    }
}
if(request.getParameter("id")!=null){
    int idasignacion=Integer.parseInt((String)request.getParameter("id"));
    asignacion=facade.getAsignacionByID(idasignacion);
}else{
    beans.Grupo grupo = new beans.Grupo();
    if(request.getParameter("idgrupo")!=null){
        int idgrupo = Integer.parseInt(String.valueOf(request.getParameter("idgrupo")));
        grupo = rfacade.getGrupoByID(idgrupo);
    }
    asignacion.setIdasignacion(0);
    asignacion.setFinicio(new java.util.Date());
    asignacion.setFtermino(null);
    asignacion.setGrupo(grupo);
    asignacion.setUsuario(new beans.Usuario());
}%>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Catalogos Principales</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item"><a href="#" onclick="javascript:go2to('Grupo/grupoListado.jsp', 'page-wrapper');">Grupos</a></li>
                    <li class="breadcrumb-item"><a href="#" onclick="javascript:go2to('Asignacion/asignacionListado.jsp?idgrupo=<%=asignacion.getGrupo().getIdgrupo()%>', 'page-wrapper');">Asignaciones</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Asignacion del Grupo: <%=asignacion.getGrupo().getNombre()%></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form asignacione="form" id="form1" name="form1" action="asignacion.jsp" method="post">
                            <input type="hidden" id="idasignacion" name="idasignacion" value="<%=asignacion.getIdasignacion()%>"/>
                            <input type="hidden" id="grupo_idgrupo" name="grupo_idgrupo" value="<%=asignacion.getGrupo().getIdgrupo()%>"/>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="finicio">Fecha de Inicio: </label>
                                        <input class="form-control input-sm" id="finicio" name="finicio" type="text" value="<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(asignacion.getFinicio())%>" autocomplete="off" required/>
                                    </div>      
                                </div>
                                <div class="col-lg-3 offset-lg-6">
                                    <div class="form-group">
                                        <label for="ftermino">Fecha de Termino: </label>
                                        <input class="form-control input-sm" id="ftermino" name="ftermino" type="text" value="<%=asignacion.getFtermino()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(asignacion.getFtermino())%>" autocomplete="off"/>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="usuario_idusuario">Usuario Asignado: </label>
                                        <select id="usuario_idusuario" name="usuario_idusuario" class="form-control input-sm">
                                            <%for(beans.Usuario usuario:ufacade.getUsuarioDisponible()){%>
                                                <option value="<%=usuario.getIdusuario()%>" <%=usuario.getIdusuario()==asignacion.getUsuario().getIdusuario()?"SELECTED":""%>><%=usuario.getNombre()%></option>
                                            <%}%>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>    
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:save('Asignacion/asignacion.jsp?idgrupo=<%=asignacion.getGrupo().getIdgrupo()%>','Asignacion/asignacionListado.jsp?idgrupo=<%=asignacion.getGrupo().getIdgrupo()%>','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                                <%if(asignacion.getIdasignacion()!=0){%>
                                    <button class="btn btn-outline-danger btn-sm" type="button" onclick="javascript:erase('Asignacion/asignacion.jsp?idgrupo=<%=asignacion.getGrupo().getIdgrupo()%>','Asignacion/asignacionListado.jsp?idgrupo=<%=asignacion.getGrupo().getIdgrupo()%>','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                                <%}%>
                                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="javascript:go2to('Asignacion/asignacionListado.jsp?idgrupo=<%=asignacion.getGrupo().getIdgrupo()%>','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    jQuery.datetimepicker.setLocale('es');
    jQuery(document).ready(function () {
        'use strict';
        jQuery('#finicio').datetimepicker({
            format:'Y-m-d H:i:s',
            lang:'es',
            maxDate:'0'
        });
        jQuery('#ftermino').datetimepicker({
            format:'Y-m-d H:i:s',
            lang:'es',
            minDate:'0'
        });
    });
</script>

<%
facade.close();
rfacade.close();
ufacade.close();
%>
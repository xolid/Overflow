<%-- 
    Document   : borrar
    Created on : 29-may-2018, 1:58:09
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.ComentarioFacade"/>
<jsp:useBean id="ffacade" scope="page" class="facades.FlujoFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>

<%
int idflujo = 0;
if(request.getParameter("idflujo")!=null){
    idflujo = Integer.parseInt(String.valueOf(request.getParameter("idflujo")));
}
String texto = "";
if(request.getParameter("texto")!=null){
    texto = String.valueOf(request.getParameter("texto"));
}
beans.Comentario comentario = new beans.Comentario();
comentario.setFecha(new java.util.Date());
comentario.setTexto(texto);
comentario.setFlujo(ffacade.getFlujoByID(idflujo));
comentario.setUsuario(ufacade.getUsuarioByID(((beans.Usuario)session.getAttribute("usuario")).getIdusuario()));
facade.saveComentario(comentario);
tools.LogRegister.write(((beans.Usuario)session.getAttribute("usuario")).getNombre(),comentario.getTexto(),"Sube comentario",comentario.getFlujo());
%>

<script>
    go2to('Comentario/comentarioListado.jsp?idflujo=<%=idflujo%>','comentarioListado');
</script>

<%
facade.close();
ffacade.close();
ufacade.close();
%>
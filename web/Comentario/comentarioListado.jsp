<%-- 
    Document   : archivoListado
    Created on : 29-may-2018, 1:43:29
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.ComentarioFacade"/>

<%int idflujo = 0;
if(request.getParameter("idflujo")!=null){
    idflujo = Integer.parseInt(String.valueOf(request.getParameter("idflujo")));
}%>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Comentarios en el Flujo</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-sm table-striped table-bordered table-condensed table-hover" id="dataTablesComentarios">
                            <thead>
                                <th>Fecha</th>
                                <th>Texto</th>
                                <th>Usuario</th>
                            </thead>
                            <tbody>
                                <%for(beans.Comentario comentario:facade.getComentarioByFlujo(idflujo)){%>
                                    <tr>
                                        <td><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(comentario.getFecha())%></td>
                                        <td><%=comentario.getTexto()%></td>
                                        <td><%=comentario.getUsuario().getNombre()%></td>
                                    </tr>
                                <%}%>
                            </tbody>
                        </table>   
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm btn-sm" type="button" data-toggle="modal" data-target="#myModalComentar"><i class="fa fa-comments-o fa-fw"></i> Comentar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<div class="modal fade" id="myModalComentar">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Ingrese el Comentario</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="comentario">Comentario: Maximo 2000 caracteres.</label>
                        <textarea id="comentario" name="comentario" class="form-control input-sm" rows="5" maxlength="2000"></textarea>
                    </div>      
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" onclick="javascript:comentar(document.getElementById('comentario').value);" data-dismiss="modal">Comentar</button>
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script lang="javascript">
    $(document).ready(function() {
        $('#dataTablesComentarios').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "order": [[ 0,"desc"]],
            "lengthMenu": [[3, 5, 10, -1], [3, 5, 10, "All"]],
            "columns": [
                { "width": "15%" },
                { "width": "60%" },
                { "width": "25%" }
            ]
        });
    });
    
    function comentar(texto){
        $.LoadingOverlay("show");  
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) { 
            if (xhr.readyState === 4 && xhr.status === 200) {
                var scs = xhr.responseText.extractScript();
                scs.evalScript();
                $.LoadingOverlay("hide");
            }
        };
        xhr.open("GET", "/Overflow/Comentario/comentar.jsp?idflujo=<%=idflujo%>&texto="+texto, true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    }
</script>

<%
facade.close();
%>
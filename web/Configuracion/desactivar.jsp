<%-- 
    Document   : cerrar
    Created on : Nov 4, 2014, 10:07:32 PM
    Author     : CarlosAlberto
--%>

<%String id = "";
if(request.getParameter("id")!=null){
    id = request.getParameter("id");
}
try{
    for(javax.servlet.http.HttpSession s:listeners.ServletListener.getSessions()){
        if(s.getId().equals(id)){
            int idusuario = ((beans.Usuario)s.getAttribute("usuario")).getIdusuario();
            facades.UsuarioFacade facade = new facades.UsuarioFacade();
            beans.Usuario usuario = facade.getUsuarioByID(idusuario);
            usuario.setFegreso(new java.util.Date());
            facade.updateUsuario(usuario);
            facade.close();
            s.invalidate();
        }
    }
}catch(Exception e){}
%>

<script>
    go2to('Configuracion/sesiones.jsp', 'page-wrapper');
</script>
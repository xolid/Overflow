<%-- 
    Document   : cerrar
    Created on : Nov 4, 2014, 10:07:32 PM
    Author     : CarlosAlberto
--%>

<%String id = "";
if(request.getParameter("id")!=null){
    id = request.getParameter("id");
}
try{
    for(javax.servlet.http.HttpSession s:listeners.ServletListener.getSessions()){
        if(s.getId().equals(id)){
            s.invalidate();
        }
    }
}catch(Exception e){}
%>

<script>
    go2to('Configuracion/sesiones.jsp', 'page-wrapper');
</script>
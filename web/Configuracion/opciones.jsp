<%-- 
    Document   : opciones
    Created on : Jan 10, 2016, 1:11:54 AM
    Author     : czara
--%>

<%properties.PropertiesManager propertiesManager = new properties.PropertiesManager();
if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(request.getParameter("ENTRADA")!=null)
            propertiesManager.setProperty("ENTRADA",request.getParameter("ENTRADA"));
        if(request.getParameter("SALIDA")!=null)
            propertiesManager.setProperty("SALIDA",request.getParameter("SALIDA"));
        if(request.getParameter("COMIDA_ENTRADA")!=null)
            propertiesManager.setProperty("COMIDA_ENTRADA",request.getParameter("COMIDA_ENTRADA"));
        if(request.getParameter("COMIDA_SALIDA")!=null)
            propertiesManager.setProperty("COMIDA_SALIDA",request.getParameter("COMIDA_SALIDA"));
        if(request.getParameter("DOMINGO")!=null)
            propertiesManager.setProperty("DOMINGO",request.getParameter("DOMINGO"));
        if(request.getParameter("LUNES")!=null)
            propertiesManager.setProperty("LUNES",request.getParameter("LUNES"));
        if(request.getParameter("MARTES")!=null)
            propertiesManager.setProperty("MARTES",request.getParameter("MARTES"));
        if(request.getParameter("MIERCOLES")!=null)
            propertiesManager.setProperty("MIERCOLES",request.getParameter("MIERCOLES"));
        if(request.getParameter("JUEVES")!=null)
            propertiesManager.setProperty("JUEVES",request.getParameter("JUEVES"));
        if(request.getParameter("VIERNES")!=null)
            propertiesManager.setProperty("VIERNES",request.getParameter("VIERNES"));
        if(request.getParameter("SABADO")!=null)
            propertiesManager.setProperty("SABADO",request.getParameter("SABADO"));
        if(request.getParameter("URL")!=null)
            propertiesManager.setProperty("URL",request.getParameter("URL"));
        propertiesManager.saveProperties();
    }
}%>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Configuración</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Horario de Operación</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form role="form" id="form1" name="form1" action="opciones.jsp" method="post">                        
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="ENTRADA">Horario de Entrada</label>
                                        <input class="form-control input-sm" id="ENTRADA" name="ENTRADA" type="text" step="3600" value="<%=propertiesManager.getProperty("ENTRADA")%>" autocomplete="off" required autofocus/>
                                    </div>      
                                </div>
                                <div class="col-lg-4 offset-lg-4">
                                    <div class="form-group">
                                        <label for="SALIDA">Horario de Salida</label>
                                        <input class="form-control input-sm" id="SALIDA" name="SALIDA" type="text" step="3600" value="<%=propertiesManager.getProperty("SALIDA")%>" autocomplete="off" required/>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="COMIDA_SALIDA">Horario de Salida a Descanzo</label>
                                        <input class="form-control input-sm" id="COMIDA_SALIDA" name="COMIDA_SALIDA" type="text" step="3600" value="<%=propertiesManager.getProperty("COMIDA_SALIDA")%>" autocomplete="off" required/>
                                    </div>      
                                </div>
                                <div class="col-lg-4 offset-lg-4">
                                    <div class="form-group">
                                        <label for="COMIDA_ENTRADA">Horario de Entrada a Descanzo</label>
                                        <input class="form-control input-sm" id="COMIDA_ENTRADA" name="COMIDA_ENTRADA" type="text" step="3600" value="<%=propertiesManager.getProperty("COMIDA_ENTRADA")%>" autocomplete="off" required/>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="LUNES">Lunes</label>
                                        <select class="form-control input-sm" id="LUNES" name="LUNES" required>
                                            <option value="0" <%=propertiesManager.getProperty("LUNES").equals("0")?"SELECTED":""%>>NO</option>
                                            <option value="1" <%=propertiesManager.getProperty("LUNES").equals("1")?"SELECTED":""%>>SI</option>
                                        </select>
                                    </div>      
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="MARTES">Martes</label>
                                        <select class="form-control input-sm" id="MARTES" name="MARTES" required>
                                            <option value="0" <%=propertiesManager.getProperty("MARTES").equals("0")?"SELECTED":""%>>NO</option>
                                            <option value="1" <%=propertiesManager.getProperty("MARTES").equals("1")?"SELECTED":""%>>SI</option>
                                        </select>
                                    </div>      
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="MIERCOLES">Miercoles</label>
                                        <select class="form-control input-sm" id="MIERCOLES" name="MIERCOLES" required>
                                            <option value="0" <%=propertiesManager.getProperty("MIERCOLES").equals("0")?"SELECTED":""%>>NO</option>
                                            <option value="1" <%=propertiesManager.getProperty("MIERCOLES").equals("1")?"SELECTED":""%>>SI</option>
                                        </select>
                                    </div>      
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="JUEVES">Jueves</label>
                                        <select class="form-control input-sm" id="JUEVES" name="JUEVES" required>
                                            <option value="0" <%=propertiesManager.getProperty("JUEVES").equals("0")?"SELECTED":""%>>NO</option>
                                            <option value="1" <%=propertiesManager.getProperty("JUEVES").equals("1")?"SELECTED":""%>>SI</option>
                                        </select>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">        
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="VIERNES">Viernes</label>
                                        <select class="form-control input-sm" id="VIERNES" name="VIERNES" required>
                                            <option value="0" <%=propertiesManager.getProperty("VIERNES").equals("0")?"SELECTED":""%>>NO</option>
                                            <option value="1" <%=propertiesManager.getProperty("VIERNES").equals("1")?"SELECTED":""%>>SI</option>
                                        </select>
                                    </div>      
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="SABADO">Sabado</label>
                                        <select class="form-control input-sm" id="SABADO" name="SABADO" required>
                                            <option value="0" <%=propertiesManager.getProperty("SABADO").equals("0")?"SELECTED":""%>>NO</option>
                                            <option value="1" <%=propertiesManager.getProperty("SABADO").equals("1")?"SELECTED":""%>>SI</option>
                                        </select>
                                    </div>      
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="DOMINGO">Domingo</label>
                                        <select class="form-control input-sm" id="DOMINGO" name="DOMINGO" required>
                                            <option value="0" <%=propertiesManager.getProperty("DOMINGO").equals("0")?"SELECTED":""%>>NO</option>
                                            <option value="1" <%=propertiesManager.getProperty("DOMINGO").equals("1")?"SELECTED":""%>>SI</option>
                                        </select>
                                    </div>      
                                </div>
                            </div>
                        </form>        
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary" type="button" onclick="javascript:save('Configuracion/opciones.jsp','Configuracion/opciones.jsp','form1','page-wrapper');">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
            <div class="col-md-6">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Configuracion de Overflow</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form role="form" id="form2" name="form2" action="opciones.jsp" method="post">                        
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="URL">URL Publica de Overflow</label>
                                        <input class="form-control input-sm" id="URL" name="URL" type="text" value="<%=propertiesManager.getProperty("URL")%>" autocomplete="off" required/>
                                    </div>      
                                </div>
                            </div>
                        </form>        
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary" type="button" onclick="javascript:save('Configuracion/opciones.jsp','Configuracion/opciones.jsp','form2','page-wrapper');">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    jQuery.datetimepicker.setLocale('es');
    jQuery(document).ready(function () {
        'use strict';
        jQuery('#ENTRADA,#SALIDA,#COMIDA_ENTRADA,#COMIDA_SALIDA').datetimepicker({
            format:'H:i',
            lang:'es',
            datepicker: false
        });
    });
</script>
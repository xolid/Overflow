<%-- 
    Document   : sesiones
    Created on : Feb 14, 2016, 10:45:12 PM
    Author     : czara
--%>

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Configuración del Sistema</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Conexiones actuales el Overflow</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered table-striped" id="dataTablesConexiones">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Rol</th>
                                        <th>Inicio de Sesion</th>
                                        <th>Ultimo Movimiento</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for(javax.servlet.http.HttpSession s:listeners.ServletListener.getSessions()){
                                        beans.Rol r = (beans.Rol)s.getAttribute("rol");
                                        beans.Usuario u = (beans.Usuario)s.getAttribute("usuario");
                                        if(u!=null){%>
                                            <tr>
                                                <td><%=u.getNombre()%></td>
                                                <td><%=r.getNombre()%></td>
                                                <td><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(s.getCreationTime()))%></td>
                                                <td><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(s.getLastAccessedTime()))%></td>
                                                <td class="center-col-lg-2">
                                                    <center>
                                                        <button class="btn btn-outline-danger btn-sm" type="button" onclick="javascript:desactivar('<%=s.getId()%>');" title="Bloquear Usuario"><i class="fa fa-lock"></i></button>
                                                        <button class="btn btn-outline-info btn-sm" type="button" onclick="javascript:invalidar('<%=s.getId()%>');" title="Cerrar Sesion"><i class="fa fa-close"></i></button>
                                                    </center>
                                                </td>
                                            </tr>
                                        <%}else{%>
                                            <tr>
                                                <td>Sesion no logeada</td>
                                                <td>Sesion no logeada</td>
                                                <td><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(s.getCreationTime()))%></td>
                                                <td><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(s.getLastAccessedTime()))%></td>
                                                <td class="center-col-lg-2">
                                                    <center>
                                                        <button class="btn btn-outline-warning btn-sm" type="button" onclick="javascript:invalidar('<%=s.getId()%>');"><i class="fa fa-close"></i></button>
                                                    </center>
                                                </td>
                                            </tr>
                                        <%}
                                    }%>
                                </tbody>
                            </table>   
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary" type="button" onclick="javascript:go2to('Configuracion/sesiones.jsp','page-wrapper');">Refrescar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    
    $(document).ready(function() {
        $('#dataTablesConexiones').dataTable({
            "retrieve": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "columns": [
                { "width": "30%" },
                { "width": "20%" },
                { "width": "20%" },
                { "width": "20%" },
                { "width": "10%" }
            ]
        });
    });
    
    function desactivar(id){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "¿Confirma desea desactivar la cuenta del usuario?",
            type: 'type-danger',
            closable: false,
            btnOKClass: 'btn-danger',
            callback: function(result) {
                if(result) {
                    $.LoadingOverlay("show");  
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function (e) { 
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            var scs = xhr.responseText.extractScript();
                            scs.evalScript();
                            $.LoadingOverlay("hide");
                        }
                    };
                    xhr.open("GET", "/Overflow/Configuracion/desactivar.jsp?id="+id, true);
                    xhr.setRequestHeader('Content-type', 'text/html');
                    xhr.send();
                }
            }
        });
    }
    
    function invalidar(id){
        $.LoadingOverlay("show");  
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) { 
            if (xhr.readyState === 4 && xhr.status === 200) {
                var scs = xhr.responseText.extractScript();
                scs.evalScript();
                $.LoadingOverlay("hide");
            }
        };
        xhr.open("GET", "/Overflow/Configuracion/invalidar.jsp?id="+id, true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    }
    
</script>
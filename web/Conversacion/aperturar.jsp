<%-- 
    Document   : borrar
    Created on : 29-may-2018, 1:58:09
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.ConverzacionFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>

<%
int prioridad = -1;
if(request.getParameter("prioridad")!=null){
    prioridad = Integer.parseInt(String.valueOf(request.getParameter("prioridad")));
}
String motivo = "";
if(request.getParameter("motivo")!=null){
    motivo = String.valueOf(request.getParameter("motivo"));
}
beans.Converzacion converzacion = new beans.Converzacion();
converzacion.setIdconverzacion(0);
converzacion.setFcreacion(new java.util.Date());
converzacion.setPrioridad(prioridad);
converzacion.setMotivo(motivo);
converzacion.setUsuario(ufacade.getUsuarioByID(((beans.Usuario)session.getAttribute("usuario")).getIdusuario()));
facade.saveConverzacion(converzacion);
%>

<script>
    go2to('Conversacion/conversaciones.jsp?prioridad=<%=prioridad%>','page-wrapper');
</script>

<%
facade.close();
ufacade.close();
%>
<%-- 
    Document   : conversaciones
    Created on : Jul 24, 2018, 5:43:58 PM
    Author     : czarate
--%>

<jsp:useBean id="facade" scope="page" class="facades.ConverzacionFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>

<%
int prioridad = -1;
if(request.getParameter("prioridad")!=null){
    prioridad = Integer.parseInt(String.valueOf(request.getParameter("prioridad")));
}
%>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Conversaciones</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5">
                <!-- TO DO List -->
                <div class="card card-<%=(prioridad==0?"danger":(prioridad==1?"warning":"info"))%>">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="ion ion-clipboard mr-1"></i>
                            Prioridad <%=new catalogs.Prioridad().getNombre(prioridad)%>
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-toggle="modal" data-target="#myModalAperturar"><i class="fa fa-comments"></i> Nueva</button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <ul class="todo-list">
                            <%for(Object[] converzacion:facade.getConverzacionByPrioridadANDUsuario(prioridad,((beans.Usuario)session.getAttribute("usuario")).getIdusuario())){
                                beans.Usuario usuario = ufacade.getUsuarioByID(Integer.parseInt(String.valueOf(converzacion[3])));%>
                                <li>
                                    <small class="badge badge-<%=(usuario.getIdusuario()==((beans.Usuario)session.getAttribute("usuario")).getIdusuario())?"success":"info"%>"><%=(usuario.getIdusuario()==((beans.Usuario)session.getAttribute("usuario")).getIdusuario())?"ADM":"USR"%></small>
                                    <a href="#" onclick="javascript:go2to('Mensaje/mensajes.jsp?idconversacion=<%=converzacion[0]%>','mensajes');"><span class="text" title="<%=String.valueOf(converzacion[2])%>"><%=String.valueOf(converzacion[2]).length()>50?String.valueOf(converzacion[2]).substring(0,50):String.valueOf(converzacion[2])%></span></a>
                                    <%if(usuario.getIdusuario()==((beans.Usuario)session.getAttribute("usuario")).getIdusuario()){%>
                                        <div class="tools">
                                            <i class="fa fa-close" onclick="javascript:terminar(<%=converzacion[0]%>);"></i>
                                        </div>
                                    <%}%>
                                </li>
                            <%}%>
                        </ul>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <div class="col-lg-7" id="mensajes"></div>
        </div>
    </div>
</section>
                        
<div class="modal fade" id="myModalAperturar">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Ingrese el titulo</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="motivo">Titulo: Maximo 200 caracteres.</label>
                        <input type="text" id="motivo" name="motivo" class="form-control input-sm" maxlength="200"/>
                    </div>      
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" onclick="javascript:aperturar(document.getElementById('motivo').value);" data-dismiss="modal">Aperturar</button>
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
                        
<script>
    
    function aperturar(motivo){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "Confirma la apertura de la conversacion?",
            type: 'type-info',
            closable: false,
            btnOKClass: 'btn-info',
            callback: function(result) {
                if(result) {
                    $.LoadingOverlay("show");  
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function (e) { 
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            var scs = xhr.responseText.extractScript();
                            scs.evalScript();
                            $.LoadingOverlay("hide");
                        }
                    };
                    xhr.open("GET", "/Overflow/Conversacion/aperturar.jsp?prioridad=<%=prioridad%>&motivo="+motivo, true);
                    xhr.setRequestHeader('Content-type', 'text/html');
                    xhr.send();
                }
            }
        });
    }
    
    function terminar(idconversacion){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "Confirma terminar la conversacion?",
            type: 'type-warning',
            closable: false,
            btnOKClass: 'btn-warning',
            callback: function(result) {
                if(result) {
                    $.LoadingOverlay("show");  
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function (e) { 
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            var scs = xhr.responseText.extractScript();
                            scs.evalScript();
                            $.LoadingOverlay("hide");
                        }
                    };
                    xhr.open("GET", "/Overflow/Conversacion/terminar.jsp?idconversacion="+idconversacion, true);
                    xhr.setRequestHeader('Content-type', 'text/html');
                    xhr.send();
                }
            }
        });
    }
    
    $('#manuales').removeClass("active");
    $('#documentos').removeClass("active");
    $('#aplicaciones').removeClass("active");
    $('#dashboard').removeClass("active");
    $('#historico').removeClass("active");
    $('#redactar').removeClass("active");
    $('#delegar').removeClass("active");
    $('#ejecutar').removeClass("active");
    $('#grupos').removeClass("active");
    $('#procesos').removeClass("active");
    $('#roles').removeClass("active");
    $('#usuarios').removeClass("active");
    $('#conversacion_alta').removeClass('active');
    $('#conversacion_media').removeClass('active');
    $('#conversacion_baja').removeClass('active');
    $('#conversacion_<%=prioridad==0?"alta":(prioridad==1?"media":"baja")%>').addClass('active');
    
</script>
                            
<%
facade.close();
ufacade.close();
%>
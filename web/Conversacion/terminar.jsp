<%-- 
    Document   : borrar
    Created on : 29-may-2018, 1:58:09
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.ConverzacionFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>

<%int idconverzacion = -1;
if(request.getParameter("idconversacion")!=null){
    idconverzacion = Integer.parseInt(String.valueOf(request.getParameter("idconversacion")));
}
beans.Converzacion converzacion = facade.getConverzacionByID(idconverzacion);
if(converzacion!=null){
    converzacion.setFtermino(new java.util.Date());
    facade.saveConverzacion(converzacion);
}%>

<script>
    go2to('Conversacion/conversaciones.jsp?prioridad=<%=converzacion.getPrioridad()%>','page-wrapper');
</script>

<%
facade.close();
ufacade.close();
%>
<%-- 
    Document   : dashboard
    Created on : 5/02/2018, 01:12:07 AM
    Author     : czara
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script>
    
    var intervalONE;
    var intervalTWO;
    var intervalTHREE;
    var intervalFOUR;
    var intervalFIVE;
    var intervalSIX;
    var intervalSEVEN;
    var intervalEIGHT;
    var intervalNINE;
    var intervalTEN;
    var intervalELEVEN;
    
    window.clearInterval(intervalONE);
    window.clearInterval(intervalTWO);
    window.clearInterval(intervalTHREE);
    window.clearInterval(intervalFOUR);
    window.clearInterval(intervalFIVE);
    window.clearInterval(intervalSIX);
    window.clearInterval(intervalSEVEN);
    window.clearInterval(intervalEIGHT);
    window.clearInterval(intervalNINE);
    window.clearInterval(intervalTEN);
    window.clearInterval(intervalELEVEN);

    function cargarPanel(div,url){
        $('#'+div).LoadingOverlay("show");  
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) { 
            if (xhr.readyState === 4 && xhr.status === 200) {
                $('#'+div).LoadingOverlay("hide");
                xhr.responseText.extractScript().evalScript();
                document.getElementById(div).innerHTML = xhr.responseText;
            }
        };
        xhr.open("GET", url, true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    }
    
    function updateObject(idobject,idembed,url){ 
        document.getElementById(idobject).setAttribute('data',url);
        document.getElementById(idembed).setAttribute('src',url);
    }
    
    function updateInterval(interval,divLetter,divText,value){
        document.getElementById(divLetter).innerHTML = divText;
        switch(interval){
            case 1:
                window.clearInterval(intervalONE);
                cargarPanel('divONE','/Overflow/Dashboard/divONE.jsp');
                intervalONE = window.setInterval(function() { cargarPanel('divONE','/Overflow/Dashboard/divONE.jsp'); }, value);
                break;
            case 2:
                window.clearInterval(intervalTWO);
                updateObject('objectTWO','embedTWO','<%=request.getContextPath()%>/Dashboard/divTWO.jsp');
                intervalTWO = window.setInterval(function() { updateObject('objectTWO','embedTWO','<%=request.getContextPath()%>/Dashboard/divTWO.jsp'); }, value);
                break;
            case 3:
                window.clearInterval(intervalTHREE);
                updateObject('objectTHREE','embedTHREE','<%=request.getContextPath()%>/Dashboard/divTHREE.jsp');
                intervalTHREE = window.setInterval(function() { updateObject('objectTHREE','embedTHREE','<%=request.getContextPath()%>/Dashboard/divTHREE.jsp'); }, value);
                break;
            case 4:
                window.clearInterval(intervalFOUR);
                cargarPanel('divFOUR','/Overflow/Dashboard/divFOUR.jsp');
                intervalFOUR = window.setInterval(function() { cargarPanel('divFOUR','/Overflow/Dashboard/divFOUR.jsp'); }, value);
                break;
            case 5:
                window.clearInterval(intervalFIVE);
                updateObject('objectFIVE','embedFIVE','<%=request.getContextPath()%>/Dashboard/divFIVE.jsp');
                intervalFIVE = window.setInterval(function() { updateObject('objectFIVE','embedFIVE','<%=request.getContextPath()%>/Dashboard/divFIVE.jsp'); }, value);
                break;
            case 6:
                window.clearInterval(intervalSIX);
                cargarPanel('divSIX','/Overflow/Dashboard/divSIX.jsp');
                intervalSIX = window.setInterval(function() { cargarPanel('divSIX','/Overflow/Dashboard/divSIX.jsp'); }, value);
                break;
            case 7:
                window.clearInterval(intervalSEVEN);
                cargarPanel('divSEVEN','/Overflow/Dashboard/divSEVEN.jsp');
                intervalSEVEN = window.setInterval(function() { cargarPanel('divSEVEN','/Overflow/Dashboard/divSEVEN.jsp'); }, value);
                break;
            case 8:
                window.clearInterval(intervalEIGHT);
                cargarPanel('divEIGHT','/Overflow/Dashboard/divEIGHT.jsp');
                intervalEIGHT = window.setInterval(function() { cargarPanel('divEIGHT','/Overflow/Dashboard/divEIGHT.jsp'); }, value);
                break;
            case 9:
                window.clearInterval(intervalNINE);
                updateObject('objectNINE','embedNINE','<%=request.getContextPath()%>/Dashboard/divNINE.jsp');
                intervalNINE = window.setInterval(function() { updateObject('objectNINE','embedNINE','<%=request.getContextPath()%>/Dashboard/divNINE.jsp'); }, value);
                break;
            case 10:
                window.clearInterval(intervalTEN);
                updateObject('objectTEN','embedTEN','<%=request.getContextPath()%>/Dashboard/divTEN.jsp');
                intervalTEN = window.setInterval(function() { updateObject('objectTEN','embedTEN','<%=request.getContextPath()%>/Dashboard/divTEN.jsp'); }, value);
                break;
            case 11:
                window.clearInterval(intervalELEVEN);
                updateObject('objectELEVEN','embedELEVEN','<%=request.getContextPath()%>/Dashboard/divELEVEN.jsp');
                intervalELEVEN = window.setInterval(function() { updateObject('objectELEVEN','embedELEVEN','<%=request.getContextPath()%>/Dashboard/divELEVEN.jsp'); }, value);
                break;
        }
    }
    <%
    if(((beans.Rol)session.getAttribute("rol")).getEjecuta()==1){
        out.print("cargarPanel('divONE','/Overflow/Dashboard/divONE.jsp');");
        out.print("cargarPanel('divSIX','/Overflow/Dashboard/divSIX.jsp');");
    }
    if(((beans.Rol)session.getAttribute("rol")).getDelega()==1){
        out.print("cargarPanel('divFOUR','/Overflow/Dashboard/divFOUR.jsp');");
    }
    if(((beans.Rol)session.getAttribute("rol")).getAdministra()==1){
        out.print("cargarPanel('divSEVEN','/Overflow/Dashboard/divSEVEN.jsp');");
        out.print("cargarPanel('divEIGHT','/Overflow/Dashboard/divEIGHT.jsp');");
    }
    %>  
                                                    
    $('#manuales').removeClass("active");
    $('#documentos').removeClass("active");
    $('#aplicaciones').removeClass("active");
    $('#dashboard').removeClass("active");
    $('#historico').removeClass("active");
    $('#redactar').removeClass("active");
    $('#delegar').removeClass("active");
    $('#ejecutar').removeClass("active");
    $('#grupos').removeClass("active");
    $('#procesos').removeClass("active");
    $('#roles').removeClass("active");
    $('#usuarios').removeClass("active");
    $('#conversacion_alta').removeClass('active');
    $('#conversacion_media').removeClass('active');
    $('#conversacion_baja').removeClass('active');
    $('#dashboard').addClass('active');        
</script>

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dashboard - <small><%=((beans.Usuario)session.getAttribute("usuario")).getNombre()%></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<%if(((beans.Rol)session.getAttribute("rol")).getEjecuta()==1){%>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divONELetter">Mis tareas y las de mi grupo</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divONE"></div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(1,'divONELetter','Mis tareas y las de mi grupo [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(1,'divONELetter','Mis tareas y las de mi grupo [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(1,'divONELetter','Mis tareas y las de mi grupo [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:cargarPanel('divONE','/Overflow/Dashboard/divONE.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>  
    <section class="content">
        <div class="container-fluid">
            <div class="row">                
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divSIXLetter">Tiempo restante a tareas activas</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divSIX"></div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(6,'divSIXLetter','Tiempo restante a tareas activas [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(6,'divSIXLetter','Tiempo restante a tareas activas [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(6,'divSIXLetter','Tiempo restante a tareas activas [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:cargarPanel('divSIX','/Overflow/Dashboard/divSIX.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<%}if(((beans.Rol)session.getAttribute("rol")).getRedacta()==1){%>
    <section class="content">
        <div class="container-fluid">
            <div class="row">                
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divTWOLetter">Estado de los flujos sin cierre</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divTWO">
                                <object id="objectTWO" data="<%=request.getContextPath()%>/Dashboard/divTWO.jsp" width="100%" height="400">
                                    <embed id="embedTWO" src="<%=request.getContextPath()%>/Dashboard/divTWO.jsp" width="100%" height="400"></embed>
                                </object>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(2,'divTWOLetter','Estado de los flujos sin cierre [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(2,'divTWOLetter','Estado de los flujos sin cierre [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(2,'divTWOLetter','Estado de los flujos sin cierre [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:updateObject('objectTWO','embedTWO','<%=request.getContextPath()%>/Dashboard/divTWO.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divTHREELetter">Porcentaje de avance en los flujos</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divTHREE">
                                <object id="objectTHREE" data="<%=request.getContextPath()%>/Dashboard/divTHREE.jsp" width="100%" height="400">
                                    <embed id="embedTHREE" src="<%=request.getContextPath()%>/Dashboard/divTHREE.jsp" width="100%" height="400"></embed>
                                </object>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(3,'divTHREELetter','Porcentaje de avance en los flujos [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(3,'divTHREELetter','Porcentaje de avance en los flujos [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(3,'divTHREELetter','Porcentaje de avance en los flujos [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:updateObject('objectTHREE','embedTHREE','<%=request.getContextPath()%>/Dashboard/divTHREE.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<%}if(((beans.Rol)session.getAttribute("rol")).getDelega()==1){%>
    <section class="content">
        <div class="container-fluid">
            <div class="row">                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divFOURLetter">Indicadores en mis grupos</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divFOUR"></div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(4,'divFOURLetter','Indicadores en mis grupos [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(4,'divFOURLetter','Indicadores en mis grupos [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(4,'divFOURLetter','Indicadores en mis grupos [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:cargarPanel('divFOUR','/Overload/Dashboard/divFOUR.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row"> 
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divFIVELetter">Tiempo de asignacion las ultimas tareas</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divFIVE">
                                <object id="objectFIVE" data="<%=request.getContextPath()%>/Dashboard/divFIVE.jsp" width="100%" height="400">
                                    <embed id="embedFIVE" src="<%=request.getContextPath()%>/Dashboard/divFIVE.jsp" width="100%" height="400"></embed>
                                </object>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(5,'divFOURLetter','Tiempo de asignacion las ultimas tareas [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(5,'divFOURLetter','Tiempo de asignacion las ultimas tareas [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(5,'divFOURLetter','Tiempo de asignacion las ultimas tareas [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:updateObject('objectFIVE','embedFIVE','<%=request.getContextPath()%>/Dashboard/divFIVE.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<%}if(((beans.Rol)session.getAttribute("rol")).getAdministra()==1){%>
    <section class="content">
        <div class="container-fluid">
            <div class="row">                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divSEVENLetter">Indicadores del servidor</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divSEVEN"></div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(7,'divSEVENLetter','Indicadores del servidor [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(7,'divSEVENLetter','Indicadores del servidor [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(7,'divSEVENLetter','Indicadores del servidor [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:cargarPanel('divSEVEN','/Overflow/Dashboard/divSEVEN.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row"> 
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divEIGHTLetter">Indicadores de sesiones</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divEIGHT"></div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(8,'divEIGHTLetter','Indicadores de sesiones [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(8,'divEIGHTLetter','Indicadores de sesiones [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(8,'divEIGHTLetter','Indicadores de sesiones [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:cargarPanel('divEIGHT','/Overflow/Dashboard/divEIGHT.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<%}if(((beans.Rol)session.getAttribute("rol")).getConsulta()==1){%>
    <section class="content">
        <div class="container-fluid">
            <div class="row">                
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divNINELetter">Flujos completados</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divNINE">
                                <object id="objectNINE" data="<%=request.getContextPath()%>/Dashboard/divNINE.jsp" width="100%" height="400">
                                    <embed id="embedNINE" src="<%=request.getContextPath()%>/Dashboard/divNINE.jsp" width="100%" height="400"></embed>
                                </object>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(9,'divNINELetter','Flujos completados [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(9,'divNINELetter','Flujos completados [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(9,'divNINELetter','Flujos completados [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:updateObject('objectNINE','embedNINE','<%=request.getContextPath()%>/Dashboard/divNINE.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divTENLetter">Flujos cancelados</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divTEN">
                                <object id="objectTEN" data="<%=request.getContextPath()%>/Dashboard/divTEN.jsp" width="100%" height="400">
                                    <embed id="embedTEN" src="<%=request.getContextPath()%>/Dashboard/divTEN.jsp" width="100%" height="400"></embed>
                                </object>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(10,'divTENLetter','Flujos cancelados [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(10,'divTENLetter','Flujos cancelados [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(10,'divTENLetter','Flujos cancelados [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:updateObject('objectTEN','embedTEN','<%=request.getContextPath()%>/Dashboard/divTEN.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="divELEVENLetter">Flujos en proceso</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="divELEVEN">
                                <object id="objectELEVEN" data="<%=request.getContextPath()%>/Dashboard/divELEVEN.jsp" width="100%" height="400">
                                    <embed id="embedELEVEN" src="<%=request.getContextPath()%>/Dashboard/divELEVEN.jsp" width="100%" height="400"></embed>
                                </object>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="btn-group">
                                        <a class="btn btn-default" onclick="javascript:updateInterval(10,'divELEVENLetter','Flujos cancelados [Refrescar 5 Min]',(5*60*1000));">5 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(10,'divELEVENLetter','Flujos cancelados [Refrescar 1 Min]',(60*1000));">1 Min</a>
                                        <a class="btn btn-default" onclick="javascript:updateInterval(10,'divELEVENLetter','Flujos cancelados [Refrescar 30 Seg]',(30*1000));">30 Seg</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <div class="btn-group">
                                        <button class="btn btn-default" type="button" onclick="javascript:updateObject('objectELEVEN','embedELEVEN','<%=request.getContextPath()%>/Dashboard/divELEVEN.jsp');"><span class="fa fa-refresh fa-fw"></span></button>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<%}%>
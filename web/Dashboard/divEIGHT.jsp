<%-- 
    Document   : divSEVEN
    Created on : 15/07/2018, 02:56:52 AM
    Author     : CarlosAlberto
--%>

<%
int logedSession = 0;
int ghostSession = 0;
for (javax.servlet.http.HttpSession s : listeners.ServletListener.getSessions()) {
    if ((beans.Usuario) s.getAttribute("usuario") != null) {
        logedSession++;
    } else {
        ghostSession++;
    }
}
%>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fa fa-user"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Sesiones logeadas</span>
                        <span class="info-box-number">
                            <%=logedSession%>
                            <small>Sesiones</small>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-lock"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Sessiones no logueadas</span>
                        <span class="info-box-number">
                            <%=ghostSession%>
                            <small>Sesiones</small>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
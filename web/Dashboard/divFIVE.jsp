<%-- 
    Document   : divTWO
    Created on : 6/07/2018, 07:33:42 PM
    Author     : CarlosAlberto
--%>

<jsp:useBean id="facade" scope="page" class="facades.TareaFacade"/>

<%
java.util.List<String> names = new java.util.ArrayList();
java.util.List<String> assigment_minutes = new java.util.ArrayList();
for (Object[] tarea : facade.getTareaByUsuarioOFGroupLAST50(((beans.Usuario) session.getAttribute("usuario")).getIdusuario())){
    if(tarea[1]!=null&&tarea[2]!=null){
        names.add("'Folio: "+String.valueOf(tarea[0])+"'");
        java.util.Date recepcion = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(tarea[1]));
        java.util.Date asignacion = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(tarea[2]));
        assigment_minutes.add(String.valueOf(new tools.Hora().diffMinutes(recepcion,asignacion)));
    }
}
%>

<html>
    <head>
        <title>Stacked Bar Chart</title>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/Chart.bundle.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/utils.js"></script>
    </head>
    <body>
        <div>
            <div id="canvas-holder" style="width:100%; height: 100%;">
                <canvas id="canvas" style="width:100%; height: 100%;"></canvas>
            </div>
        </div>
        <script>
            var lineChartData = {
                labels: [<%=String.join(",",names)%>],
                datasets: [{
                    label: 'Asignacion en Minutos',
                    backgroundColor: window.chartColors.red,
                    data: [<%=String.join(",",assigment_minutes)%>]
                    }
                ]
            };
            window.onload = function () {
                var ctx = document.getElementById('canvas').getContext('2d');
                window.myBar = new Chart(ctx, {
                    type: 'line',
                    data: lineChartData,
                    options: {
                        tooltips: {
                            mode: 'index',
                            intersect: false
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        }
                    }
                });
            };
        </script>
    </body>
</html>

<%
facade.close();
%>
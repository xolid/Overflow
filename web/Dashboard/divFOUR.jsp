<%-- 
    Document   : divTWO
    Created on : 6/07/2018, 07:33:42 PM
    Author     : CarlosAlberto
--%>

<%-- 
    Document   : dashboard
    Created on : 5/02/2018, 01:12:07 AM
    Author     : czara
--%>

<jsp:useBean id="facade" scope="page" class="facades.TareaFacade"/>

<%
int engrupos = 0;
int noasignados = 0;
int vencidos_requerido = 0;
for(Object[] tarea:facade.getTareaByUsuarioOFGroup(((beans.Usuario)session.getAttribute("usuario")).getIdusuario())){
    if(tarea[4]==null){
        noasignados++;
    }else{
        engrupos++;
    }
    if(tarea[6]!=null){ // Requeridos
        java.util.Date frequerido = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(tarea[6]));
        if(new java.util.Date().after(frequerido)){ // si se ha sobrepasado la fecha de requerido
            vencidos_requerido++;
        }
    }
}
%>

<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-4">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3><%=noasignados%> Tarea(s)</h3>
                        <p>Pendiente(s) de asignacion en mis grupos</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-asterisk"></i>
                    </div>
                    <a href="#" onclick="javascript:go2to('Delega/tareaListado.jsp', 'page-wrapper');" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3><%=engrupos%> Tarea(s)</h3>
                        <p>En progreso en mis grupos de asignacion</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-gears"></i>
                    </div>
                    <a href="#" onclick="javascript:go2to('Delega/tareaListado.jsp', 'page-wrapper');" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><%=vencidos_requerido%> Tarea(s)</h3>
                        <p>En mis grupos y con fecha de requerido vencida</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-warning"></i>
                    </div>
                    <a href="#" onclick="javascript:go2to('Delega/tareaListado.jsp', 'page-wrapper');" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
</section>

<%
facade.close();
%>
<%-- 
    Document   : divNINE
    Created on : 15/07/2018, 02:56:52 AM
    Author     : CarlosAlberto
--%>

<jsp:useBean id="facade" scope="page" class="facades.FlujoFacade"/>

<%
String[] colores = {"red","orange","yellow","green","blue","gray"};
java.util.List<String> names = new java.util.ArrayList();
java.util.List<String> values = new java.util.ArrayList();
java.util.List<String> colors = new java.util.ArrayList();
for (Object[] flujo : facade.getFlujoByEstadoCountANDGroupBy(6) ) {
    names.add("'"+String.valueOf(flujo[0])+"'");
    values.add(String.valueOf(flujo[1]));
    colors.add("color(window.chartColors."+colores[(int)Math.floor(Math.random()*6)]+").alpha(0.5).rgbString()");
}
%>

<html>
    <head>
        <title>Polar Chart</title>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/Chart.bundle.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/utils.js"></script>
    </head>
    <body>
        <div id="canvas-holder" style="width:100%; height: 100%;">
            <canvas id="chart-area" style="width:100%; height: 100%;"></canvas>
        </div>
        <script>
            var chartColors = window.chartColors;
            var color = Chart.helpers.color;
            var config = {
                data: {
                    datasets: [{
                        data: [<%=String.join(",",values)%>],
                        backgroundColor: [
                            <%=String.join(",",colors)%>
                        ],
                        label: 'My dataset'
                    }],
                    labels: [<%=String.join(",",names)%>]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'right',
                    },
                    scale: {
                        ticks: {
                            beginAtZero: true,
                            min: 0
                        },
                        reverse: false
                    },
                    animation: {
                        animateRotate: false,
                        animateScale: true
                    }
                }
            };

            window.onload = function () {
                var ctx = document.getElementById('chart-area');
                window.myPolarArea = Chart.PolarArea(ctx, config);
            };
            
            var colorNames = Object.keys(window.chartColors);
            
        </script>
    </body>
</html>


<%
facade.close();
%>
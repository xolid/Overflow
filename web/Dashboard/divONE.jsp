<%-- 
    Document   : divONE
    Created on : 13/07/2018, 10:52:33 PM
    Author     : CarlosAlberto
--%>

<jsp:useBean id="facade" scope="page" class="facades.TareaFacade"/>

<%
java.util.Calendar calendarINI = java.util.Calendar.getInstance();
calendarINI.set(java.util.Calendar.HOUR_OF_DAY,0);
calendarINI.set(java.util.Calendar.MINUTE,0);
calendarINI.set(java.util.Calendar.SECOND,0);
calendarINI.set(java.util.Calendar.MILLISECOND,0);
java.util.Date fechaINI = calendarINI.getTime();

java.util.Calendar calendarFIN = java.util.Calendar.getInstance();
calendarFIN.set(java.util.Calendar.HOUR_OF_DAY,23);
calendarFIN.set(java.util.Calendar.MINUTE,59);
calendarFIN.set(java.util.Calendar.SECOND,59);
calendarFIN.set(java.util.Calendar.MILLISECOND,999);
java.util.Date fechaFIN = calendarFIN.getTime();

java.util.Date ahora = new java.util.Date();
java.util.Calendar calendar = java.util.Calendar.getInstance();
calendar.setTime(ahora);
calendar.add(java.util.Calendar.HOUR_OF_DAY,2);
java.util.Date inmediato = calendar.getTime();
    
int asignados_ami = 0;
int noasignados = 0;
int vencen_hoy = 0;
int vencen_inmediato = 0;
for(Object[] tarea:facade.getTareaByUsuarioOFGroup(((beans.Usuario)session.getAttribute("usuario")).getIdusuario())){
    if(tarea[4]!=null){ // Asignados
        if(String.valueOf(tarea[4]).equals(((beans.Usuario)session.getAttribute("usuario")).getNombre())){ // Asignados a mi
            asignados_ami++;
            if(tarea[3]!=null){ // con fecha procesal
                java.util.Date fproceso = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(tarea[3]));
                if(fproceso.after(fechaINI)&&fproceso.before(fechaFIN)){ // si fecha procesal es hoy
                    vencen_hoy++;
                }
                if(fproceso.after(ahora)&&fproceso.before(inmediato)){ // si fecha procesal es en dos horas
                    vencen_inmediato++;
                }
            }
        }
    }else{
        noasignados++;
    }
}
%>

<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3><%=asignados_ami%> Tarea(s)</h3>
                        <p>Asignadas a mi en bandeja de entrada</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-inbox"></i>
                    </div>
                    <a href="#" onclick="javascript:go2to('Ejecuta/tareaListado.jsp', 'page-wrapper');" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3><%=noasignados%> Tarea(s)</h3>
                        <p>Asignadas a mi grupo sin ocupar</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <a href="#" onclick="javascript:go2to('Ejecuta/tareaListado.jsp', 'page-wrapper');" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3><%=vencen_hoy%> Tarea(s)</h3>
                        <p>Asignados a mi que vencen hoy</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <a href="#" onclick="javascript:go2to('Ejecuta/tareaListado.jsp', 'page-wrapper');" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><%=vencen_inmediato%> Tarea(s)</h3>
                        <p>Asignados a mi que vencen en 2 Hrs</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-fire"></i>
                    </div>
                    <a href="#" onclick="javascript:go2to('Ejecuta/tareaListado.jsp', 'page-wrapper');" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
    </div>
</section>
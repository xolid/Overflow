<%-- 
    Document   : divSEVEN
    Created on : 15/07/2018, 02:56:52 AM
    Author     : CarlosAlberto
--%>

<%
//procesador
long manyProcessors = Runtime.getRuntime().availableProcessors();
com.sun.management.OperatingSystemMXBean bean = (com.sun.management.OperatingSystemMXBean)java.lang.management.ManagementFactory.getOperatingSystemMXBean();
double porcentageProcessor = bean.getSystemCpuLoad()*100;
String colorProcessor = "success";
if(porcentageProcessor>75){
    colorProcessor = "warning";
}
if(porcentageProcessor>95){
    colorProcessor = "danger";
}

//memoria
long freeMemory = Runtime.getRuntime().freeMemory();
long totalMemory = Runtime.getRuntime().totalMemory();
long usedMemory = totalMemory - freeMemory;
long maxMemory = Runtime.getRuntime().maxMemory();
double porcentageMemory = (new Double(usedMemory).doubleValue()/new Double(totalMemory).doubleValue())*100;
String colorMemory = "success";
if(porcentageMemory>75){
    colorMemory = "warning";
}
if(porcentageMemory>95){
    colorMemory = "danger";
}

//discos
java.util.List<Object[]> discos = new java.util.ArrayList();
for (java.io.File root:java.io.File.listRoots()) {
    long totalSpace = root.getTotalSpace();
    long freeSpace = root.getFreeSpace();
    long usedSpace = totalSpace - freeSpace;
    double porcentageDisk = (new Double(usedSpace).doubleValue()/new Double(totalSpace).doubleValue())*100;
    String colorDisk = "success";
    if(porcentageDisk>75){
        colorDisk = "warning";
    }
    if(porcentageDisk>95){
        colorDisk = "danger";
    }
    Object[] disco = {root.getAbsolutePath(),usedSpace,totalSpace,porcentageDisk,colorDisk};
    discos.add(disco);
}
%>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
                <div class="info-box bg-<%=colorProcessor%>">
                    <span class="info-box-icon"><i class="fa fa-gears"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text"><%=manyProcessors%> Cores</span>
                        <span class="info-box-number"><%=new java.text.DecimalFormat("##.##").format(porcentageProcessor)%> %</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <%=porcentageProcessor%>%"></div>
                        </div>
                        <span class="progress-description">
                            Promedio de procesamiento
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <div class="info-box bg-<%=colorMemory%>">
                    <span class="info-box-icon"><i class="fa fa-area-chart"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Memoria de JVM Mbytes</span>
                        <span class="info-box-number"><%=(usedMemory/1048576)%> / <%=(totalMemory/1048576)%></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <%=porcentageMemory%>%"></div>
                        </div>
                        <span class="progress-description">
                            Memoria maxima <%=(maxMemory/1048576)%> Mbytes
                        </span>
                    </div>
                </div>
            </div>
            <%for(Object[] disk:discos){%>                    
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-<%=disk[4]%>">
                        <span class="info-box-icon"><i class="fa fa-database"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Unidad <%=disk[0]%></span>
                            <span class="info-box-number"><%=((long)disk[1]/1048576)%> / <%=((long)disk[2]/1048576)%></span>
                            <div class="progress">
                                <div class="progress-bar" style="width: <%=disk[3]%>%"></div>
                            </div>
                            <span class="progress-description">
                                Uso de disco en MBytes
                            </span>
                        </div>
                    </div>
                </div>
            <%}%>
        </div>
    </div>
</section>
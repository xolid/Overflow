<%-- 
    Document   : divLista
    Created on : 11/02/2018, 11:54:37 AM
    Author     : czara
--%>

<jsp:useBean id="facade" scope="page" class="facades.TareaFacade"/>

<%java.util.List results = facade.getTareaEjecutarBYUsuarioLIMIT_TIME(((beans.Usuario)session.getAttribute("usuario")).getIdusuario());
for(int i=0;i<results.size();i++){
    Object[] objects = (Object[]) results.get(i);
    String color = "list-group-item-success";
    int minutes = Integer.valueOf(String.valueOf(objects[5]));
    if(minutes<1440){
        color = "list-group-item-warning";
    }
    if(minutes<120){
        color = "list-group-item-danger";
    }
    String estatus = "Restan";
    if(((java.util.Date)objects[3]).before(new java.util.Date())){
        estatus = "Exedido";
    }
    %>
    <a href="#" class="list-group-item <%=color%>" onclick="javascript:go2to('Ejecuta/tarea.jsp?id=<%=objects[0]%>', 'page-wrapper');" title="Inicio: [<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(objects[2])%>] Termina: [<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(objects[3])%>]">
        <i class="fa fa-warning fa-fw"></i> Tarea "<%=objects[1]%>"
        <span class="pull-right text-muted small"><em> <%=estatus+" "+objects[4]%></em></span>
    </a>
<%}%>

<%
facade.close();
%>
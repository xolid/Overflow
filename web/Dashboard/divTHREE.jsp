<%-- 
    Document   : divTWO
    Created on : 6/07/2018, 07:33:42 PM
    Author     : CarlosAlberto
--%>

<jsp:useBean id="facade" scope="page" class="facades.FlujoFacade"/>
<jsp:useBean id="tfacade" scope="page" class="facades.TareaFacade"/>

<%
String[] colores = {"red","orange","yellow","green","blue","gray"};
java.util.List<String> names = new java.util.ArrayList();
java.util.List<String> values = new java.util.ArrayList();
java.util.List<String> colors = new java.util.ArrayList();
for (beans.Flujo flujo : facade.getFlujoByUsuario(((beans.Usuario) session.getAttribute("usuario")).getIdusuario())) {
    int total_tareas = 0;
    int total_terminadas = 0;
    for(beans.Tarea tarea:tfacade.getTareaByFlujo(flujo.getIdflujo())){
        total_tareas++;
        if(tarea.getEstado()==4&&tarea.getFliberado()!=null)
            total_terminadas++;
    }
    int porcentaje = 0;
    if(total_tareas>0){
        porcentaje = ((100*total_terminadas)/total_tareas);
    }
    names.add("'Folio: "+flujo.getIdflujo()+"'");
    values.add(String.valueOf(porcentaje));
    colors.add("color(window.chartColors."+colores[(int)Math.floor(Math.random()*6)]+").alpha(0.5).rgbString()");
}
%>

<html>
    <head>
        <title>Polar Chart</title>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/Chart.bundle.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/utils.js"></script>
    </head>
    <body>
        <div id="canvas-holder" style="width:100%; height: 100%;">
            <canvas id="chart-area" style="width:100%; height: 100%;"></canvas>
        </div>
        <script>
            var chartColors = window.chartColors;
            var color = Chart.helpers.color;
            var config = {
                data: {
                    datasets: [{
                        data: [<%=String.join(",",values)%>],
                        backgroundColor: [
                            <%=String.join(",",colors)%>
                        ],
                        label: 'My dataset'
                    }],
                    labels: [<%=String.join(",",names)%>]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'right',
                    },
                    scale: {
                        ticks: {
                            beginAtZero: true,
                            min: 0,
                            max: 100
                        },
                        reverse: false
                    },
                    animation: {
                        animateRotate: false,
                        animateScale: true
                    }
                }
            };

            window.onload = function () {
                var ctx = document.getElementById('chart-area');
                window.myPolarArea = Chart.PolarArea(ctx, config);
            };
            
            var colorNames = Object.keys(window.chartColors);
            
        </script>
    </body>
</html>

<%
facade.close();
tfacade.close();
%>
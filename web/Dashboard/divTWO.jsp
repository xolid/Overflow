<%-- 
    Document   : divTWO
    Created on : 6/07/2018, 07:33:42 PM
    Author     : CarlosAlberto
--%>
<jsp:useBean id="facade" scope="page" class="facades.FlujoFacade"/>

<%
int pendientes = 0;
int recibidos = 0;
int rechazados = 0;
int progreso = 0;
int completados = 0;
int declinados = 0;
for(beans.Flujo flujo:facade.getFlujoByUsuario(((beans.Usuario)session.getAttribute("usuario")).getIdusuario())){
    switch(flujo.getEstado()){
        case 0:
            pendientes++;
            break;
        case 1:
            recibidos++;
            break;
        case 2:
            rechazados++;
            break;
        case 3:
            progreso++;
            break;
        case 4:
            completados++;
            break;
        case 5:
            declinados++;
            break;
    }
}
%>

<html>
    <head>
        <title>Pie Chart</title>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/Chart.bundle.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/utils.js"></script>
    </head>
    <body>
        <div id="canvas-holder" style="width:100%; height: 100%;">
            <canvas id="chart-area" style="width:100%; height: 100%;"></canvas>
        </div>
        <script>
            var config = {
                type: 'pie',
                data: {
                    datasets: [{
                            data: [
                                <%=pendientes%>,
                                <%=recibidos%>,
                                <%=rechazados%>,
                                <%=progreso%>,
                                <%=completados%>,
                                <%=declinados%>
                            ],
                            backgroundColor: [
                                window.chartColors.red,
                                window.chartColors.orange,
                                window.chartColors.yellow,
                                window.chartColors.green,
                                window.chartColors.blue,
                                window.chartColors.gray
                            ],
                            label: 'Dataset 1'
                        }],
                    labels: [
                        'Pendientes',
                        'Recibidos',
                        'Rechazados',
                        'Progreso',
                        'Completados',
                        'Declinados'
                    ]
                },
                options: {
                    responsive: true
                }
            };
            window.onload = function () {
                var ctx = document.getElementById('chart-area').getContext('2d');
                window.myPie = new Chart(ctx, config);
            };
            var colorNames = Object.keys(window.chartColors);
        </script>
    </body>
</html>

<%
facade.close();
%>
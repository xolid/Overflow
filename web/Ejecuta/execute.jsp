<%-- 
    Document   : execute
    Created on : Aug 14, 2018, 1:09:17 PM
    Author     : czarate
--%>

<%
if(request.getParameter("idtarea")!=null && request.getParameter("operacion")!=null){ 
    int idtarea = Integer.parseInt(String.valueOf(request.getParameter("idtarea")));
    int idusuario = ((beans.Usuario)session.getAttribute("usuario")).getIdusuario();
    String operacion = String.valueOf(request.getParameter("operacion"));
    switch(operacion){
        case "completeTask":
            new navigation.Flow().completeTask(idusuario,idtarea);
            break;
        case "progressTask":
            new navigation.Flow().progressTask(idusuario,idtarea);
            break;
        case "refuseTask":
            new navigation.Flow().refuseTask(idusuario,idtarea);
            break;
    }
}
%>

<script>
    go2to('Ejecuta/tareaListado.jsp','page-wrapper');
</script>
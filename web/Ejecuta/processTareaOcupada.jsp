<%-- 
    Document   : process2
    Created on : Feb 7, 2016, 7:18:03 PM
    Author     : czara
--%>
 
<%@page import="java.util.*"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<jsp:useBean id="facade" scope="page" class="facades.TareaFacade"/>

<%  
String[] cols = { "idtarea","pnombre","anombre","fasignado","fproceso","estado"};
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();
int amount = 10;
int start = 0;
int echo = 0;
int col = 0;
String dir = "asc";
String sStart = request.getParameter("start");
String sAmount = request.getParameter("length");
String sEcho = request.getParameter("draw");
String sCol = request.getParameter("order[0][column]");
String sdir = request.getParameter("order[0][dir]");
String buscar = request.getParameter("search[value]");
if (sStart != null) {
    start = Integer.parseInt(sStart);
    if (start < 0)
        start = 0;
}
if (sAmount != null) {
    amount = Integer.parseInt(sAmount);
    if (amount < 10 || amount > 100)
        amount = 10;
}
if (sEcho != null) {
    echo = Integer.parseInt(sEcho);
}
if (sCol != null) {
    col = Integer.parseInt(sCol);
    if (col < 0 || col > 5)
        col = 0;
}
if (sdir != null) {
    if (!sdir.equals("asc"))
        dir = "desc";
}
String colName = cols[col];
long total = facade.getTotal();
int idautor = ((beans.Usuario)session.getAttribute("usuario")).getIdusuario();
List<Object[]> tareas = facade.getTareaProcessEjecutar(buscar, idautor, colName, dir, start, amount);
for(Object[] p:tareas){
    JSONArray ja = new JSONArray();
    ja.add("<a href=\"#\" onclick=\"javascript:go2to('Ejecuta/tarea.jsp?id="+p[0]+"','page-wrapper')\">"+p[0]+"</a>");
    ja.add(p[1]);
    ja.add(p[2]);
    ja.add(p[3]==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(p[3]));
    ja.add(p[4]==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(p[4]));
    String color2 = "info";
    switch(Integer.valueOf(String.valueOf(p[5]))){
        case 0:
            color2 = "warning";
            break;
        case 1:
            color2 = "info";
            break;
        case 2:
            color2 = "danger";
            break;
        case 3:
            color2 = "primary";
            break;
        case 4:
            color2 = "success";
            break;
        case 5:
            color2 = "danger";
            break;
        case 6:
            color2 = "success";
            break;
        case 7:
            color2 = "danger";
            break;
            
    }
    ja.add("<small class=\"badge badge-"+color2+"\"> "+new catalogs.Estado().getNombre(Integer.valueOf(String.valueOf(p[5])))+"</small>");
    array.add(ja);
}
long totalAfterFilter = facade.getTareaCountEjecutar(buscar, idautor);
result.put("draw", sEcho);
result.put("recordsTotal", total);
result.put("recordsFiltered", totalAfterFilter);
result.put("data", array);
response.setContentType("application/json");
response.setHeader("Cache-Control", "no-store");
out.print(result);

facade.close();    
%>


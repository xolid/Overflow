<%-- 
    Document   : proceso
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="tarea" class="beans.Tarea" scope="page"/>
<jsp:useBean id="facade" scope="page" class="facades.TareaFacade"/>

<%
if(request.getParameter("id")!=null){
    int idtarea=Integer.parseInt((String)request.getParameter("id"));
    tarea=facade.getTareaByID(idtarea);
}%>


<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Bandeja de Entrada - <small>Detalle de la Tarea</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegaci�n</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3 class="card-title">Detalle del Flujo</h3>
                            </div>
                            <div class="col-lg-6 text-right">
                                <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#myModalLog"><i class="fa fa-file-text fa-fw"></i> Registro</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="idflujo">Folio de Solicitud: </label>
                                    <input class="form-control input-sm" id="idflujo" name="idflujo" type="text" value="<%=tarea.getFlujo().getIdflujo()%>" readonly/>
                                </div>      
                            </div>
                            <div class="col-lg-2 offset-lg-8">
                                <div class="form-group">
                                    <label for="fcreado">Fecha de Creacion: </label>
                                    <input class="form-control input-sm" id="fcreado" name="fcreado" type="text" value="<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFlujo().getFcreado())%>" readonly/>
                                </div>      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label for="nombre">Nombre de la Solicitud: </label>
                                    <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=tarea.getFlujo().getNombre()%>" readonly/>
                                </div>      
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="usuario_nombre">Solicitado por: </label>
                                    <input class="form-control input-sm" id="usuario_nombre" name="usuario_nombre" type="text" value="<%=tarea.getFlujo().getUsuario().getNombre()%>" readonly/>
                                </div>      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="descripcion">Descripcion de la Solicitud: </label>
                                    <textarea id="descripcion" name="descripcion" class="form-control input-sm" rows="5" readonly><%=tarea.getFlujo().getDescripcion()%></textarea>
                                </div>      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="proceso_idproceso">Proceso para la Solicitud: </label>
                                    <select id="proceso_idproceso" name="proceso_idproceso" class="form-control input-sm" disabled>
                                        <option value="<%=tarea.getFlujo().getProceso().getIdproceso()%>"><%=tarea.getFlujo().getProceso().getNombre()%></option>
                                    </select>
                                </div>      
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label for="myModalProceso">&nbsp;</label>
                                    <button class="btn btn-info btn-block" type="button" data-toggle="modal" data-target="#myModalProceso"><i class="fa fa-eye fa-fw"></i></button>
                                </div>      
                            </div>
                            <div class="col-lg-2 offset-lg-2">
                                <div class="form-group">
                                    <label for="fsolicitado">Fecha de Solicitud: </label>
                                    <input class="form-control input-sm" id="fsolicitado" name="fsolicitado" type="text" value="<%=tarea.getFlujo().getFsolicitado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFlujo().getFsolicitado())%>" readonly/>
                                </div>      
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="fasignacion">Fecha Limite para Entrega: </label>
                                    <input class="form-control input-sm" id="frequerido" name="frequerido" type="text" value="<%=tarea.getFlujo().getFrequerido()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFlujo().getFrequerido())%>" readonly/>
                                </div>      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="estado">Estado de la Solicitud: </label>
                                    <select id="estado" name="estado" class="form-control input-sm" disabled>
                                        <option value="<%=tarea.getFlujo().getEstado()%>"><%=new catalogs.Estado().getNombre(tarea.getFlujo().getEstado())%></option>
                                    </select>
                                </div>      
                            </div>
                            <div class="col-lg-2 offset-lg-6">
                                <div class="form-group">
                                    <label for="fentregado">Fecha de Entrega: </label>
                                    <input class="form-control input-sm" id="fentregado" name="fentregado" type="text" value="<%=tarea.getFlujo().getFentregado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFlujo().getFentregado())%>" readonly/>
                                </div>      
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="fterminado">Fecha de Termino: </label>
                                    <input class="form-control input-sm" id="fterminado" name="fterminado" type="text" value="<%=tarea.getFlujo().getFterminado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFlujo().getFterminado())%>" readonly/>
                                </div>      
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 
<div class="content-header" id="adjuntoListado"></div>
<div class="content-header" id="comentarioListado"></div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Detalle de la Tarea</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="idtarea">Folio de Tarea: </label>
                                    <input class="form-control input-sm" id="idtarea" name="idtarea" type="text" value="<%=tarea.getIdtarea()%>" readonly/>
                                </div>      
                            </div>
                            <div class="col-lg-2 offset-lg-6">
                                <div class="form-group">
                                    <label for="estado">Estado de la Tarea: </label>
                                    <select id="estado" name="estado" class="form-control input-sm" disabled>
                                        <option value="<%=tarea.getEstado()%>"><%=new catalogs.Estado().getNombre(tarea.getEstado())%></option>
                                    </select>
                                </div>      
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="frecibido">Fecha de Recepcion: </label>
                                    <input class="form-control input-sm" id="frecibido" name="frecibido" type="text" value="<%=tarea.getFrecibido()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFrecibido())%>" readonly/>
                                </div>      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="anombre">Nombre de la Actividad: </label>
                                    <input class="form-control input-sm" id="anombre" name="anombre" type="text" maxlength="75" value="<%=tarea.getActividad().getNombre()%>" readonly/>
                                </div>      
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <label for="myModalActividad">&nbsp;</label>
                                    <button class="btn btn-info btn-block" data-toggle="modal" data-target="#myModalActividad"><i class="fa fa-eye fa-fw"></i></button>
                                </div>      
                            </div>
                            <div class="col-lg-2 offset-lg-4">
                                <div class="form-group">
                                    <label for="fasignacion">Fecha de Asignacion: </label>
                                    <input class="form-control input-sm" id="fasignacion" name="fasignacion" type="text" value="<%=tarea.getFasignado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFasignado())%>" readonly/>
                                </div>      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="usuario_nombre">Usuario Asignado: </label>
                                    <input class="form-control input-sm" id="usuario_nombre" name="usuario_nombre" type="text" value="<%=tarea.getUsuario()==null?"":tarea.getUsuario().getNombre()%>" readonly/>
                                </div>      
                            </div>
                            <div class="col-lg-2 offset-lg-2">
                                <div class="form-group">
                                    <label for="fproceso">Fecha Limite por Proceso: </label>
                                    <input class="form-control input-sm" id="fproceso" name="fproceso" type="text" value="<%=tarea.getFproceso()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFproceso())%>" readonly/>
                                </div>      
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="fliberado">Fecha Liberado: </label>
                                    <input class="form-control input-sm" id="fliberado" name="fliberado" type="text" value="<%=tarea.getFliberado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFliberado())%>" readonly/>
                                </div>      
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="javascript:go2to('Ejecuta/tareaListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                                <%if(tarea.getEstado()==1){%>
                                    <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:progreso();"><i class="fa fa-hand-o-up fa-fw"></i> Ocupar</button>
                                <%}%>
                                <%if(tarea.getEstado()>1&&tarea.getEstado()<4){%>
                                    <button class="btn btn-outline-danger btn-sm" type="button" onclick="javascript:rechazar();"><i class="fa fa-times-circle-o fa-fw"></i> Rechazar</button>
                                    <button class="btn btn-outline-success btn-sm" type="button" onclick="javascript:completar();"><i class="fa fa-check-circle-o fa-fw"></i> Completar</button>
                                <%}%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="myModalLog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Registro de Eventos</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/registro.jsp?idflujo=<%=tarea.getFlujo().getIdflujo()%>" width="100%" height="400">
                    <embed src="<%=request.getContextPath()%>/registro.jsp?idflujo=<%=tarea.getFlujo().getIdflujo()%>" width="100%" height="400"></embed>
                </object>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>  
<div class="modal fade" id="myModalProceso">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Datos del Proceso</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="nombre">Nombre del Proceso: </label>
                            <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=tarea.getFlujo().getProceso().getNombre()%>" readonly/>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="descripcion">Descripcion del Proceso: </label>
                            <textarea id="descripcion" name="descripcion" class="form-control input-sm" rows="5" readonly><%=tarea.getFlujo().getProceso().getDescripcion()%></textarea>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <object data="<%=request.getContextPath()%>/mapeo.jsp?idproceso=<%=tarea.getFlujo().getProceso().getIdproceso()%>" width="100%" height="400">
                            <embed src="<%=request.getContextPath()%>/mapeo.jsp?idproceso=<%=tarea.getFlujo().getProceso().getIdproceso()%>" width="100%" height="400"></embed>
                        </object>
                    </div>
                </div>  
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 
<div class="modal fade" id="myModalActividad">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Datos de la Actividad</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="nombre">Nombre: </label>
                            <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=tarea.getActividad().getNombre()%>" readonly/>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="descripcion">Descripcion de la Actividad: </label>
                            <textarea id="descripcion" name="descripcion" class="form-control input-sm" rows="5" readonly><%=tarea.getActividad().getDescripcion()%></textarea>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="consideraciones">Consideraciones en la Actividad: </label>
                            <textarea id="consideraciones" name="consideraciones" class="form-control input-sm" rows="5" readonly><%=tarea.getActividad().getConsideraciones()==null?"":tarea.getActividad().getConsideraciones()%></textarea>
                        </div>      
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="entregables">Entregables en la Actividad: </label>
                            <textarea id="entregables" name="entregables" class="form-control input-sm" rows="5" readonly><%=tarea.getActividad().getEntregables()==null?"":tarea.getActividad().getEntregables()%></textarea>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="duracion">Duracion (Hrs): </label>
                            <input class="form-control input-sm" id="duracion" name="duracion" type="number" value="<%=tarea.getActividad().getDuracion()%>" readonly/>
                        </div>      
                    </div>
                    <div class="col-lg-10">
                        <div class="form-group">
                            <label for="grupo_idgrupo">Grupo de Asignacion: </label>
                            <select id="grupo_idgrupo" name="grupo_idgrupo" class="form-control input-sm" disabled>
                                <option value="<%=tarea.getActividad().getGrupo().getIdgrupo()%>"><%=tarea.getActividad().getGrupo().getNombre()%></option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>                         
<div class="modal fade" id="myModalActividad">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Datos de la Actividad</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="nombre">Nombre: </label>
                            <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=tarea.getActividad().getNombre()%>" readonly/>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="descripcion">Descripcion de la Actividad: </label>
                            <textarea id="descripcion" name="descripcion" class="form-control input-sm" rows="5" readonly><%=tarea.getActividad().getDescripcion()%></textarea>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="consideraciones">Consideraciones en la Actividad: </label>
                            <textarea id="consideraciones" name="consideraciones" class="form-control input-sm" rows="5" readonly><%=tarea.getActividad().getConsideraciones()==null?"":tarea.getActividad().getConsideraciones()%></textarea>
                        </div>      
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="entregables">Entregables en la Actividad: </label>
                            <textarea id="entregables" name="entregables" class="form-control input-sm" rows="5" readonly><%=tarea.getActividad().getEntregables()==null?"":tarea.getActividad().getEntregables()%></textarea>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="duracion">Duracion (Hrs): </label>
                            <input class="form-control input-sm" id="duracion" name="duracion" type="number" value="<%=tarea.getActividad().getDuracion()%>" readonly/>
                        </div>      
                    </div>
                    <div class="col-lg-10">
                        <div class="form-group">
                            <label for="grupo_idgrupo">Grupo de Asignacion: </label>
                            <select id="grupo_idgrupo" name="grupo_idgrupo" class="form-control input-sm" disabled>
                                <option value="<%=tarea.getActividad().getGrupo().getIdgrupo()%>"><%=tarea.getActividad().getGrupo().getNombre()%></option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 

<script lang="javascript">
    
    function progreso(){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "�Confirma desea marcar la tarea como EN PROGRESO?<br/><br/>Esta accion retroalimenta al usuario solicitante y es requisito para completar la tarea.",
            type: 'type-primary',
            closable: false,
            btnOKClass: 'btn-primary',
            callback: function(result) {
                if(result) {
                    $.LoadingOverlay("show");  
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function (e) { 
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            var scs = xhr.responseText.extractScript();
                            scs.evalScript();
                            $.LoadingOverlay("hide");
                        }
                    };
                    xhr.open("GET", "/Overflow/Ejecuta/execute.jsp?operacion=progressTask&idtarea=<%=tarea.getIdtarea()%>", true);
                    xhr.setRequestHeader('Content-type', 'text/html');
                    xhr.send();
                }
            }
        });
    }
    
    function completar(){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "�Confirma desea marcar la tarea como COMPLETADA?<br/><br/>Esta accion completara esta tarea y disparara la siguiente.",
            type: 'type-success',
            closable: false,
            btnOKClass: 'btn-success',
            callback: function(result) {
                if(result) {
                    $.LoadingOverlay("show");  
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function (e) { 
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            var scs = xhr.responseText.extractScript();
                            scs.evalScript();
                            $.LoadingOverlay("hide");
                        }
                    };
                    xhr.open("GET", "/Overflow/Ejecuta/execute.jsp?operacion=completeTask&idtarea=<%=tarea.getIdtarea()%>", true);
                    xhr.setRequestHeader('Content-type', 'text/html');
                    xhr.send();
                }
            }
        });
    }
    
    function rechazar(){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "�Confirma desea marcar la tarea como RECHAZADA?<br/><br/>Esta accion reiniciaria esta tarea y reactivaria la anterior.",
            type: 'type-danger',
            closable: false,
            btnOKClass: 'btn-danger',
            callback: function(result) {
                if(result) {
                    $.LoadingOverlay("show");  
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function (e) { 
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            var scs = xhr.responseText.extractScript();
                            scs.evalScript();
                            $.LoadingOverlay("hide");
                        }
                    };
                    xhr.open("GET", "/Overflow/Ejecuta/execute.jsp?operacion=refuseTask&idtarea=<%=tarea.getIdtarea()%>", true);
                    xhr.setRequestHeader('Content-type', 'text/html');
                    xhr.send();
                }
            }
        });
    }
    
    function refreshArchivos(){
        xmlhttpPost2EXEC('Adjunto/adjuntoListado.jsp?idflujo=<%=tarea.getFlujo().getIdflujo()%>','adjuntoListado', null, 'xmlhttpPost2(\'Comentario/comentarioListado.jsp?idflujo=<%=tarea.getFlujo().getIdflujo()%>\',\'comentarioListado\',null)');
    }
    
    refreshArchivos();
</script>



<%
facade.close();
%>
<%-- 
    Document   : tareaListado
    Created on : Jan 28, 2016, 4:50:18 PM
    Author     : czara
--%>


<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Bandeja de Entrada - <small>Tareas</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Listado de Tareas sin Asignacion</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped table-bordered table-condensed table-hover" id="dataTablesTareasLibres">
                                <thead>
                                    <th>[B] Folio</th>
                                    <th>[B] Proceso</th>
                                    <th>[B] Nombre de la Actividad</th>
                                    <th>Fecha Recibido</th>
                                    <th>Estado</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Listado de Tareas Asignadas a <%=((beans.Usuario)session.getAttribute("usuario")).getNombre()%></h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped table-bordered table-condensed table-hover" id="dataTablesTareasOcupadas">
                                <thead>
                                    <th>[B] Folio</th>
                                    <th>[B] Proceso</th>
                                    <th>[B] Nombre de la Actividad</th>
                                    <th>Fecha Asignado</th>
                                    <th>Entrega por Proceso</th>
                                    <th>Estado</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#dataTablesTareasOcupadas').dataTable({
            "retrieve": true,
            "processing": true,  
            "serverSide": true,
            "ajax": "Ejecuta/processTareaOcupada.jsp",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "columns": [
                { "width": "10%" },
                { "width": "20%" },
                { "width": "30%" },
                { "width": "15%" },
                { "width": "15%" },
                { "width": "10%" }
            ],
            columnDefs: [{
                targets: 1,
                render: function ( data, type, row ) {
                    return data.length > 50 ? data.substr( 0, 50 ) + '...' : data;
                }
            },
            {
                targets: 2,
                render: function ( data, type, row ) {
                    return data.length > 100 ? data.substr( 0, 100 ) + '...' : data;
                }
            }]
        });

        $('#dataTablesTareasLibres').dataTable({
            "retrieve": true,
            "processing": true,  
            "serverSide": true,
            "ajax": "Ejecuta/processTareaLibre.jsp",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "columns": [
                { "width": "10%" },
                { "width": "25%" },
                { "width": "40%" },
                { "width": "15%" },
                { "width": "10%" }
            ],
            columnDefs: [{
                targets: 1,
                render: function ( data, type, row ) {
                    return data.length > 50 ? data.substr( 0, 50 ) + '...' : data;
                }
            },
            {
                targets: 2,
                render: function ( data, type, row ) {
                    return data.length > 100 ? data.substr( 0, 100 ) + '...' : data;
                }
            }]
        });
        
        $('#manuales').removeClass("active");
        $('#documentos').removeClass("active");
        $('#aplicaciones').removeClass("active");
        $('#dashboard').removeClass("active");
        $('#historico').removeClass("active");
        $('#redactar').removeClass("active");
        $('#delegar').removeClass("active");
        $('#ejecutar').removeClass("active");
        $('#grupos').removeClass("active");
        $('#procesos').removeClass("active");
        $('#roles').removeClass("active");
        $('#usuarios').removeClass("active");
        $('#conversacion_alta').removeClass('active');
        $('#conversacion_media').removeClass('active');
        $('#conversacion_baja').removeClass('active');
        $('#ejecutar').addClass('active');
    });
</script>
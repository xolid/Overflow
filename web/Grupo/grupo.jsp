<%-- 
    Document   : grupo
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="grupo" class="beans.Grupo" scope="page"/>
<jsp:setProperty name="grupo" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.GrupoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(grupo.getIdgrupo()==0){
            facade.saveGrupo(grupo);
        }else{
            facade.updateGrupo(grupo);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteGrupo(grupo);        
    }
}
if(request.getParameter("id")!=null){
    int idgrupo=Integer.parseInt((String)request.getParameter("id"));
    grupo=facade.getGrupoByID(idgrupo);
}else{
    grupo.setIdgrupo(0);
    grupo.setNombre("");
}%>



<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Catalogos Principales</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Definicion del Grupo</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form grupoe="form" id="form1" name="form1" action="grupo.jsp" method="post">
                            <input type="hidden" id="idgrupo" name="idgrupo" value="<%=grupo.getIdgrupo()%>"/>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="nombre">Nombre: </label>
                                        <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=grupo.getNombre()%>" autocomplete="off" required autofocus/>
                                    </div>      
                                </div>
                            </div>
                        </form>      
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:save('Grupo/grupo.jsp','Grupo/grupoListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                                <%if(grupo.getIdgrupo()!=0){%>
                                    <button class="btn btn-outline-danger btn-sm" type="button" onclick="javascript:erase('Grupo/grupo.jsp','Grupo/grupoListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                                <%}%>
                                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="javascript:go2to('Grupo/grupoListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<%
facade.close();
%>
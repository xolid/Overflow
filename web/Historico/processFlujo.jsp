<%-- 
    Document   : process2
    Created on : Feb 7, 2016, 7:18:03 PM
    Author     : czara
--%>
 
<%@page import="java.util.*"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<jsp:useBean id="facade" scope="page" class="facades.FlujoFacade"/>
<jsp:useBean id="tfacade" scope="page" class="facades.TareaFacade"/>

<%  
String[] cols = { "idflujo","nombre","descripcion","fsolicitado","fentregado","estado"};
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();
int amount = 10;
int start = 0;
int echo = 0;
int col = 0;
String dir = "asc";
String sStart = request.getParameter("start");
String sAmount = request.getParameter("length");
String sEcho = request.getParameter("draw");
String sCol = request.getParameter("order[0][column]");
String sdir = request.getParameter("order[0][dir]");
String buscar = request.getParameter("search[value]");
if (sStart != null) {
    start = Integer.parseInt(sStart);
    if (start < 0)
        start = 0;
}
if (sAmount != null) {
    amount = Integer.parseInt(sAmount);
    if (amount < 10 || amount > 100)
        amount = 10;
}
if (sEcho != null) {
    echo = Integer.parseInt(sEcho);
}
if (sCol != null) {
    col = Integer.parseInt(sCol);
    if (col < 0 || col > 5)
        col = 0;
}
if (sdir != null) {
    if (!sdir.equals("asc"))
        dir = "desc";
}
String colName = cols[col];
long total = facade.getTotal();
int idautor = ((beans.Usuario)session.getAttribute("usuario")).getIdusuario();
List<beans.Flujo> flujos = new java.util.ArrayList();
if(((beans.Rol)session.getAttribute("rol")).getConsulta()==1){
    flujos = facade.getFlujoForProcessFINISHED(buscar, colName, dir, start, amount);
}else{
    flujos = facade.getFlujoForProcessByUsuarioFINISHED(buscar, idautor, colName, dir, start, amount);
}
for(beans.Flujo p:flujos){
    JSONArray ja = new JSONArray();
    ja.add("<a href=\"#\" onclick=\"javascript:go2to('Historico/visualizar.jsp?id="+p.getIdflujo()+"','page-wrapper')\">"+p.getIdflujo()+"</a>");
    ja.add(p.getNombre());
    ja.add(p.getDescripcion());
    ja.add(p.getFsolicitado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(p.getFsolicitado()));
    ja.add(p.getFentregado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(p.getFentregado()));
    String color2 = "info";
    switch(p.getEstado()){
        case 0:
            color2 = "secundary";
            break;
        case 1:
            color2 = "info";
            break;
        case 2:
            color2 = "danger";
            break;
        case 3:
            color2 = "primary";
            break;
        case 4:
            color2 = "success";
            break;
        case 5:
            color2 = "danger";
            break;
        case 6:
            color2 = "success";
            break;
        case 7:
            color2 = "danger";
            break;
            
    }
    ja.add("<small class=\"badge badge-"+color2+"\"> "+new catalogs.Estado().getNombre(p.getEstado())+"</small>");
    array.add(ja);
}
long totalAfterFilter = 0;
if(((beans.Rol)session.getAttribute("rol")).getConsulta()==1){
    totalAfterFilter = facade.getFlujoForProcessCountFINISHED(buscar);
}else{
    totalAfterFilter = facade.getFlujoForProcessCountByUsuarioFINISHED(buscar, idautor);
}
result.put("draw", sEcho);
result.put("recordsTotal", total);
result.put("recordsFiltered", totalAfterFilter);
result.put("data", array);
response.setContentType("application/json");
response.setHeader("Cache-Control", "no-store");
out.print(result);

facade.close();    
tfacade.close();
%>


<%-- 
    Document   : flujo
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="flujo" class="beans.Flujo" scope="page"/>
<jsp:useBean id="facade" scope="page" class="facades.FlujoFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProcesoFacade"/>
<jsp:useBean id="tfacade" scope="page" class="facades.TareaFacade"/>

<%if(request.getParameter("id")!=null){
    flujo=facade.getFlujoByID(Integer.parseInt((String)request.getParameter("id")));
}%>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Historico</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3 class="card-title">Definicion del Flujo</h3>
                            </div>
                            <div class="col-lg-6 text-right">
                                <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#myModalAvance"><i class="fa fa-clock-o fa-fw"></i> Avance</button>
                                <button class="btn btn-secundary btn-sm" type="button" data-toggle="modal" data-target="#myModalLog"><i class="fa fa-file-text fa-fw"></i> Registro</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form role="form" id="form1" name="form1" action="flujo.jsp" method="post">
                            <input type="hidden" id="idflujo" name="idflujo" value="<%=flujo.getIdflujo()%>"/>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="idflujo">Folio de Solicitud: </label>
                                        <input class="form-control input-sm" id="idflujo" name="idflujo" type="text" value="<%=flujo.getIdflujo()%>" readonly/>
                                    </div>      
                                </div>
                                <div class="col-lg-2 offset-lg-8">
                                    <div class="form-group">
                                        <label for="fcreado">Fecha de Creacion: </label>
                                        <input class="form-control input-sm" id="fcreado" name="fcreado" type="text" value="<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(flujo.getFcreado())%>" readonly/>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="nombre">Nombre de la Solicitud: </label>
                                        <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=flujo.getNombre()%>" readonly/>
                                    </div>      
                                </div>
                                <div class="col-lg-4 offset-lg-2">
                                    <div class="form-group">
                                        <label for="usuario_nombre">Solicitado por: </label>
                                        <input class="form-control input-sm" id="usuario_nombre" name="usuario_nombre" type="text" value="<%=flujo.getUsuario().getNombre()%>" readonly/>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="descripcion">Descripcion de la Solicitud: </label>
                                        <textarea id="descripcion" name="descripcion" class="form-control input-sm" rows="5" readonly><%=flujo.getDescripcion()%></textarea>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="proceso_idproceso">Proceso para la Solicitud: </label>
                                        <select id="proceso_idproceso" name="proceso_idproceso" class="form-control input-sm" disabled>
                                            <%for(beans.Proceso proceso:pfacade.getProcesoAll()){%>
                                                <option value="<%=proceso.getIdproceso()%>" <%=proceso.getIdproceso()==flujo.getProceso().getIdproceso()?"SELECTED":""%>><%=proceso.getNombre()%></option>
                                            <%}%>
                                        </select>
                                    </div>      
                                </div>
                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="myModalProceso">&nbsp;</label>
                                        <button class="btn btn-info btn-block" type="button" data-toggle="modal" data-target="#myModalProceso"><i class="fa fa-eye fa-fw"></i></button>
                                    </div>      
                                </div>
                                <div class="col-lg-2 offset-lg-2">
                                    <div class="form-group">
                                        <label for="fsolicitado">Fecha de Solicitud: </label>
                                        <input class="form-control input-sm" id="fsolicitado" name="fsolicitado" type="text" value="<%=flujo.getFsolicitado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(flujo.getFsolicitado())%>" readonly/>
                                    </div>      
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="fasignacion">Fecha Limite para Entrega: </label>
                                        <input class="form-control input-sm" id="frequerido" name="frequerido" type="text" value="<%=flujo.getFrequerido()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(flujo.getFrequerido())%>" readonly/>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="estado">Estado de la Solicitud: </label>
                                        <select id="estado" name="estado" class="form-control input-sm" disabled>
                                            <option value="<%=flujo.getEstado()%>"><%=new catalogs.Estado().getNombre(flujo.getEstado())%></option>
                                        </select>
                                    </div>      
                                </div>
                                <div class="col-lg-2 offset-lg-6">
                                    <div class="form-group">
                                        <label for="fentregado">Fecha de Entrega: </label>
                                        <input class="form-control input-sm" id="fentregado" name="fentregado" type="text" value="<%=flujo.getFentregado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(flujo.getFentregado())%>" readonly/>
                                    </div>      
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="fterminado">Fecha de Termino: </label>
                                        <input class="form-control input-sm" id="fterminado" name="fterminado" type="text" value="<%=flujo.getFterminado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(flujo.getFterminado())%>" readonly/>
                                    </div>      
                                </div>
                            </div>
                        </form>     
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-6">
                                <button class="btn btn-outline-secundary btn-sm" type="button" onclick="javascript:go2to('Historico/flujoListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<div class="content-header" id="adjuntoListado"></div>
<div class="content-header" id="comentarioListado"></div>

<%
java.util.List<beans.Tarea> tareas = tfacade.getTareaByFlujo(flujo.getIdflujo());
if(tareas!=null){%>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detalle de Tareas Realizadas</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <%for(beans.Tarea tarea:tareas){%>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!-- general form elements disabled -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title"><%=tarea.getActividad().getNombre()%></h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="idtarea">Folio de Tarea: </label>
                                            <input class="form-control input-sm" id="idtarea" name="idtarea" type="text" value="<%=tarea.getIdtarea()%>" readonly/>
                                        </div>      
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="estado">Estado de la Tarea: </label>
                                            <select id="estado" name="estado" class="form-control input-sm" disabled>
                                                <option value="<%=tarea.getEstado()%>"><%=new catalogs.Estado().getNombre(tarea.getEstado())%></option>
                                            </select>
                                        </div>      
                                    </div>
                                    <div class="col-lg-2 offset-lg-4">
                                        <div class="form-group">
                                            <label for="frecibido">Fecha de Recepcion: </label>
                                            <input class="form-control input-sm" id="frecibido" name="frecibido" type="text" value="<%=tarea.getFrecibido()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFrecibido())%>" readonly/>
                                        </div>      
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="fasignacion">Fecha de Asignacion: </label>
                                            <input class="form-control input-sm" id="fasignacion" name="fasignacion" type="text" value="<%=tarea.getFasignado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFasignado())%>" readonly/>
                                        </div>      
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="usuario_nombre">Usuario Asignado: </label>
                                            <input class="form-control input-sm" id="usuario_nombre" name="usuario_nombre" type="text" value="<%=tarea.getUsuario()==null?"":tarea.getUsuario().getNombre()%>" readonly/>
                                        </div>      
                                    </div>
                                    <div class="col-lg-2 offset-lg-4">
                                        <div class="form-group">
                                            <label for="fproceso">Fecha Limite por Proceso: </label>
                                            <input class="form-control input-sm" id="fproceso" name="fproceso" type="text" value="<%=tarea.getFproceso()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFproceso())%>" readonly/>
                                        </div>      
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="fliberado">Fecha Liberado: </label>
                                            <input class="form-control input-sm" id="fliberado" name="fliberado" type="text" value="<%=tarea.getFliberado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFliberado())%>" readonly/>
                                        </div>      
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    <%}
}%>
<div class="modal fade" id="myModalLog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Registro de Eventos</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/registro.jsp?idflujo=<%=flujo.getIdflujo()%>" width="100%" height="400">
                    <embed src="<%=request.getContextPath()%>/registro.jsp?idflujo=<%=flujo.getIdflujo()%>" width="100%" height="400"></embed>
                </object>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalAvance">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Avance en Tareas</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/avance.jsp?idflujo=<%=flujo.getIdflujo()%>" width="100%" height="400">
                    <embed src="<%=request.getContextPath()%>/avance.jsp?idflujo=<%=flujo.getIdflujo()%>" width="100%" height="400"></embed>
                </object>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalProceso">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Datos del Proceso</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="nombre">Nombre del Proceso: </label>
                            <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=flujo.getProceso().getNombre()%>" readonly/>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="descripcion">Descripcion del Proceso: </label>
                            <textarea id="descripcion" name="descripcion" class="form-control input-sm" rows="5" readonly><%=flujo.getProceso().getDescripcion()%></textarea>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <object data="<%=request.getContextPath()%>/mapeo.jsp?idproceso=<%=flujo.getProceso().getIdproceso()%>" width="100%" height="400">
                            <embed src="<%=request.getContextPath()%>/mapeo.jsp?idproceso=<%=flujo.getProceso().getIdproceso()%>" width="100%" height="400"></embed>
                        </object>
                    </div>
                </div> 
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script lang="javascript">

    function refreshArchivos(){
        xmlhttpPost2EXEC('Adjunto/adjuntoListado.jsp?idflujo=<%=flujo.getIdflujo()%>','adjuntoListado', null, 'xmlhttpPost2(\'Comentario/comentarioListado.jsp?idflujo=<%=flujo.getIdflujo()%>\',\'comentarioListado\',null)');
    }

    <%=flujo.getIdflujo()==0?"":"refreshArchivos();"%>
</script>

<%
facade.close();
ufacade.close();
pfacade.close();
tfacade.close();
%>
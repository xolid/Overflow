<%-- 
    Document   : borrar
    Created on : 29-may-2018, 1:58:09
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.ConverzacionFacade"/>
<jsp:useBean id="mfacade" scope="page" class="facades.MensajeFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="ifacade" scope="page" class="facades.InvitadoFacade"/>

<%
int idconverzacion = 0;
if(request.getParameter("idconversacion")!=null){
    idconverzacion = Integer.parseInt(String.valueOf(request.getParameter("idconversacion")));
}
int idusuario = 0;
if(request.getParameter("idusuario")!=null){
    idusuario = Integer.parseInt(String.valueOf(request.getParameter("idusuario")));
}
beans.Converzacion converzacion = facade.getConverzacionByID(idconverzacion);
beans.Usuario usuario = ufacade.getUsuarioByID(idusuario);
beans.Invitado invitado = new beans.Invitado();
invitado.setConverzacion(converzacion);
invitado.setUsuario(usuario);
invitado.setFingreso(new java.util.Date());
ifacade.saveInvitado(invitado);
%>

<script>
    go2to('Mensaje/mensajes.jsp?idconversacion=<%=idconverzacion%>','mensajes');
</script>

<%
facade.close();
ufacade.close();
mfacade.close();
ifacade.close();
%>
<%-- 
    Document   : borrar
    Created on : 29-may-2018, 1:58:09
    Author     : solid
--%>

<jsp:useBean id="ifacade" scope="page" class="facades.InvitadoFacade"/>

<%
int idinvitado = 0;
if(request.getParameter("idinvitado")!=null){
    idinvitado = Integer.parseInt(String.valueOf(request.getParameter("idinvitado")));
}
beans.Invitado invitado = ifacade.getInvitadoByID(idinvitado);
invitado.setFegreso(new java.util.Date());
ifacade.updateInvitado(invitado);
%>

<script>
    go2to('Mensaje/mensajes.jsp?idconversacion=<%=invitado.getConverzacion().getIdconverzacion()%>','mensajes');
</script>

<%
ifacade.close();
%>
<%-- 
    Document   : borrar
    Created on : 29-may-2018, 1:58:09
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.ConverzacionFacade"/>
<jsp:useBean id="mfacade" scope="page" class="facades.MensajeFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>

<%
int idconverzacion = 0;
if(request.getParameter("idconversacion")!=null){
    idconverzacion = Integer.parseInt(String.valueOf(request.getParameter("idconversacion")));
}
String message = "";
if(request.getParameter("message")!=null){
    message = String.valueOf(request.getParameter("message"));
}
beans.Converzacion converzacion = facade.getConverzacionByID(idconverzacion);
beans.Mensaje mensaje = new beans.Mensaje();
mensaje.setIdmensaje(0);
mensaje.setConverzacion(converzacion);
mensaje.setUsuario(ufacade.getUsuarioByID(((beans.Usuario)session.getAttribute("usuario")).getIdusuario()));
mensaje.setFecha(new java.util.Date());
mensaje.setMensaje(message);
mfacade.saveMensaje(mensaje);
%>

<script>
    go2to('Mensaje/mensajes.jsp?idconversacion=<%=idconverzacion%>','mensajes');
</script>

<%
facade.close();
ufacade.close();
mfacade.close();
%>
<%-- 
    Document   : mensajes
    Created on : Jul 24, 2018, 6:46:19 PM
    Author     : czarate
--%>

<jsp:useBean id="facade" scope="page" class="facades.ConverzacionFacade"/>
<jsp:useBean id="mfacade" scope="page" class="facades.MensajeFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="ifacade" scope="page" class="facades.InvitadoFacade"/>

<%
int idconverzacion = 0;
if(request.getParameter("idconversacion")!=null){
    idconverzacion = Integer.parseInt(String.valueOf(request.getParameter("idconversacion")));
}
beans.Converzacion converzacion = facade.getConverzacionByID(idconverzacion);
%>

<div class="card card-<%=(converzacion.getPrioridad()==0?"danger":(converzacion.getPrioridad()==1?"warning":"info"))%> direct-chat direct-chat-<%=(converzacion.getPrioridad()==0?"danger":(converzacion.getPrioridad()==1?"warning":"info"))%>">
    <div class="card-header">
        <h3 class="card-title">Mensajes</h3>
        <div class="card-tools">
            <span class="badge badge-light"><%=converzacion.getMotivo()%></span>
            <button type="button" class="btn btn-tool" onclick="javascript:go2to('Mensaje/mensajes.jsp?idconversacion=<%=idconverzacion%>','mensajes');"><i class="fa fa-refresh"></i></button>
            <%if(converzacion.getUsuario().getIdusuario()==((beans.Usuario)session.getAttribute("usuario")).getIdusuario()){%>
                <button type="button" class="btn btn-tool" data-toggle="modal" data-target="#myModalAgregar"><i class="fa fa-user-plus"></i></button>
            <%}%>
            <button type="button" class="btn btn-tool"  data-toggle="modal" data-target="#myModalMostrar"><i class="fa fa-users"></i></button>
            <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="card-body">
        <div class="direct-chat-messages">
            <%for(beans.Mensaje mensaje:mfacade.getMensajeByConverzacion(converzacion.getIdconverzacion())){%>
                <div class="direct-chat-msg <%=(mensaje.getUsuario().getIdusuario()==((beans.Usuario)session.getAttribute("usuario")).getIdusuario())?"right":""%>">
                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name float-left"><%=mensaje.getUsuario().getNombre()%></span>
                        <span class="direct-chat-timestamp float-right"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(mensaje.getFecha())%></span>
                    </div>
                    <%if(mensaje.getUsuario().getFoto()==null){%>
                        <img class="direct-chat-img" src="<%=request.getContextPath()%>/img/default-avatar.jpg" alt="message user image">
                    <%}else{%>
                        <img class="direct-chat-img" src="<%=request.getContextPath()%>/Perfil/descargar.jsp?idusuario=<%=mensaje.getUsuario().getIdusuario()%>" alt="message user image">
                    <%}%>
                    <div class="direct-chat-text">
                        <%=mensaje.getMensaje()%>
                    </div>
                </div>
            <%}%>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <form action="#" method="post">
            <div class="input-group">
                <input type="text" id="message" name="message" placeholder="Escriba un mensaje ..." class="form-control input-sm" autocomplete="off" >
                <span class="input-group-append">
                    <button type="button" class="btn btn-sm btn-<%=(converzacion.getPrioridad()==0?"danger":(converzacion.getPrioridad()==1?"warning":"info"))%>" onclick="javascript:enviar();">Enviar</button>
                </span>
            </div>
        </form>
    </div>
    <!-- /.card-footer-->
</div>
<!--/.direct-chat -->

<div class="modal fade" id="myModalAgregar">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Agregar a la conversacion</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="usuario_idusuario">Seleccione el usuario:</label>
                        <select id="usuario_idusuario" name="usuario_idusuario" class="form-control input-sm">
                            <%for(beans.Usuario usuario:ufacade.getUsuarioDisponible()){
                                String agregado = "";
                                if(ifacade.getInvitadoByConverzacionANDUsuario(idconverzacion,usuario.getIdusuario())!=null){
                                    agregado = "Usuario agregado";
                                }
                                if(usuario.getIdusuario()==((beans.Usuario)session.getAttribute("usuario")).getIdusuario()){
                                    agregado = "Soy yo";
                                }
                                %>
                                <option value="<%=usuario.getIdusuario()%>" <%=agregado.equals("")?"":"DISABLED"%>><%=usuario.getNombre()%> <%=agregado.equals("")?"":"- "+agregado%></option>
                            <%}%>
                        </select>
                    </div>      
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" onclick="javascript:agregar();" data-dismiss="modal">Agregar</button>
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
                        
<div class="modal fade" id="myModalMostrar">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Usuarios en la conversacion</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <h6>Administrador</h6>
                <ul class="todo-list">
                    <li>
                        <small class="badge badge-info"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(converzacion.getFcreacion())%></small>
                        <span class="text"><%=converzacion.getUsuario().getNombre()%></span>
                    </li>
                </ul>
                <br/>
                <h6>Invitados</h6>
                <ul class="todo-list">
                    <%for(beans.Invitado invitado:ifacade.getInvitadoByConverzacionALL(idconverzacion)){%>
                        <li>
                            <small class="badge badge-info"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(invitado.getFingreso())%></small>
                            <span class="text"><%=invitado.getUsuario().getNombre()%></span>
                            <%if(invitado.getFegreso()!=null){%>
                                <small class="badge badge-warning"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(invitado.getFegreso())%></small>
                            <%}%>
                            <%if(converzacion.getUsuario().getIdusuario()==((beans.Usuario)session.getAttribute("usuario")).getIdusuario()){
                                if(invitado.getUsuario().getIdusuario()!=((beans.Usuario)session.getAttribute("usuario")).getIdusuario()){%>
                                    <div class="tools">
                                        <%if(invitado.getFegreso()!=null){%>
                                            <i class="fa fa-unlock" onclick="javascript:desbloquear(<%=invitado.getIdinvitado()%>);" data-dismiss="modal"></i>
                                        <%}else{%>
                                            <i class="fa fa-lock" onclick="javascript:bloquear(<%=invitado.getIdinvitado()%>);" data-dismiss="modal"></i>
                                        <%}%>
                                    </div>
                                <%}%>
                            <%}%>
                        </li>
                    <%}%>
                </ul>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    function enviar(){
        var message = document.getElementById("message").value;
        $('#mensajes').LoadingOverlay("show");  
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) { 
            if (xhr.readyState === 4 && xhr.status === 200) {
                var scs = xhr.responseText.extractScript();
                scs.evalScript();
                $('#mensajes').LoadingOverlay("hide");
            }
        };
        xhr.open("GET", "/Overflow/Mensaje/enviar.jsp?idconversacion=<%=idconverzacion%>&message="+message, true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    }
    
    function agregar(){
        var idusuario = document.getElementById('usuario_idusuario').options[document.getElementById('usuario_idusuario').selectedIndex].value;
        $('#mensajes').LoadingOverlay("show");  
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) { 
            if (xhr.readyState === 4 && xhr.status === 200) {
                var scs = xhr.responseText.extractScript();
                scs.evalScript();
                $('#mensajes').LoadingOverlay("hide");
            }
        };
        xhr.open("GET", "/Overflow/Mensaje/agregar.jsp?idconversacion=<%=idconverzacion%>&idusuario="+idusuario, true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    }
    
    function bloquear(idinvitado){
        $('#mensajes').LoadingOverlay("show");  
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) { 
            if (xhr.readyState === 4 && xhr.status === 200) {
                var scs = xhr.responseText.extractScript();
                scs.evalScript();
                $('#mensajes').LoadingOverlay("hide");
            }
        };
        xhr.open("GET", "/Overflow/Mensaje/bloquear.jsp?idinvitado="+idinvitado, true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    }
    
    function desbloquear(idinvitado){
        $('#mensajes').LoadingOverlay("show");  
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) { 
            if (xhr.readyState === 4 && xhr.status === 200) {
                var scs = xhr.responseText.extractScript();
                scs.evalScript();
                $('#mensajes').LoadingOverlay("hide");
            }
        };
        xhr.open("GET", "/Overflow/Mensaje/desbloquear.jsp?idinvitado="+idinvitado, true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    }
</script>

<%
facade.close();
mfacade.close();
ufacade.close();
ifacade.close();
%>
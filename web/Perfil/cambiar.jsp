<%-- 
    Document   : cambiar
    Created on : Jan 16, 2016, 11:49:25 AM
    Author     : czara
--%>

<jsp:useBean id="usuario" class="beans.Usuario" scope="page"/>
<jsp:setProperty name="usuario" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.AsignacionFacade"/>

<%
int result = -1;
if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(request.getParameter("pass0")!=null&&request.getParameter("pass1")!=null&&request.getParameter("pass2")!=null){
            String pass0=tools.MD5.getMD5(request.getParameter("pass0"));
            String pass1=tools.MD5.getMD5(request.getParameter("pass1"));
            String pass2=tools.MD5.getMD5(request.getParameter("pass2"));
            if(!pass0.equals("null")&&!pass1.equals("null")&&!pass2.equals("null")){
                if(usuario.getPass().equals(pass0)){
                    if(pass1.equals(pass2)){
                        beans.Usuario u = facade.getUsuarioByID(usuario.getIdusuario());
                        u.setPass(pass2);
                        facade.saveUsuario(u);
                        result=0;
                    }else{
                        result=1;
                    }
                }else{
                    result=2;
                }
            }
        }
    }
}
if(session.getAttribute("usuario")!=null){
    beans.Usuario u = (beans.Usuario)session.getAttribute("usuario");
    usuario = facade.getUsuarioByID(u.getIdusuario());
}%>

<%if(result!=-1){
    String msg = "";
    String type = "";
    switch(result){
        case 0:
        msg = "El password ha cambiado satisfactoriamente...";
        type = "success";
        break;
    case 1:
        msg = "El password nuevo no coincide...";
        type = "warning";
        break;
    case 2:
        msg = "El password anterior no coincide...";
        type = "danger";
        break;
    }%>
    <script>
        BootstrapDialog.alert({
            title: "Mensaje del Sistema",
            message: "<strong><%=msg%></strong><br/><br/>",
            type: 'type-<%=type%>',
            closable: false,
            btnOKClass: 'btn-<%=type%>'
        });
    </script>
<%}%>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Perfil de Usuario</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <%if(usuario.getFoto()==null){%>
                                <img class="profile-user-img img-fluid img-circle" src="<%=request.getContextPath()%>/img/default-avatar.jpg" alt="User profile picture">
                            <%}else{%>
                                <img class="profile-user-img img-fluid img-circle" src="<%=request.getContextPath()%>/Perfil/descargar.jsp?idusuario=<%=usuario.getIdusuario()%>" alt="User profile picture">
                            <%}%>
                        </div>
                        <h3 class="profile-username text-center"><%=usuario.getNombre()%></h3>
                        <p class="text-muted text-center"><%=usuario.getPuesto()%></p>
                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Correo</b> <a class="float-right"><%=usuario.getMail()%></a>
                            </li>
                            <li class="list-group-item">
                                <b>Fecha Ingreso</b> <a class="float-right"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH.mm:ss").format(usuario.getFingreso())%></a>
                            </li>
                            <li class="list-group-item">
                                <b>Rol</b> <a class="float-right"><%=usuario.getRol().getNombre()%></a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-6">
                                <a href="#" class="btn btn-outline-warning btn-sm" data-toggle="modal" data-target="#myModalUploadProfile"><i class="fa fa-user-circle-o fa-fw"></i> <%=usuario.getFoto()==null?"Subir":"Cambiar"%> Foto</a>
                            </div>
                            <div class="col-lg-6 text-right">
                                <%if(usuario.getFoto()!=null){%>
                                    <a href="#" class="btn btn-outline-danger btn-sm" onclick="javascript:remove();"><i class="fa fa-remove fa-fw"></i> Remover Foto</a>
                                <%}%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Grupos de Asignacion</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="todo-list">
                                    <%for(beans.Asignacion asignacion:afacade.getAsignacionByUsuario(((beans.Usuario)session.getAttribute("usuario")).getIdusuario())){%>
                                        <li>
                                            <small class="badge badge-info"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(asignacion.getFinicio())%></small>
                                            <span class="text"><%=asignacion.getGrupo().getNombre()%></span>
                                            <%if(asignacion.getFtermino()!=null){%>
                                                <small class="badge badge-warning"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(asignacion.getFtermino())%></small>
                                            <%}%>
                                        </li>
                                    <%}%>
                                </ul>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
            <div class="col-md-4">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Cambiar password</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form role="form" id="form2" name="form2" action="cambiar.jsp" method="post">
                            <input type="hidden" id="idusuario" name="idusuario" value="<%=usuario.getIdusuario()%>"/>
                            <input type="hidden" id="pass" name="pass" value="<%=usuario.getPass()%>"/>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Password Actual: </label>
                                        <input class="form-control input-sm" id="pass0" name="pass0" type="password" autocomplete="off" required autofocus/>
                                    </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Password Nuevo: </label>
                                        <input class="form-control input-sm" id="pass1" name="pass1" type="password" autocomplete="off" required/>
                                    </div>  
                                </div>
                            </div>
                            <div class="row">        
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Reescribir Password: </label>
                                        <input class="form-control input-sm" id="pass2" name="pass2" type="password" autocomplete="off" required/>     
                                    </div>  
                                </div>
                            </div>
                        </form>    
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-warning btn-sm" type="button" onclick="javascript:saveNORedirect('Perfil/cambiar.jsp','form2','page-wrapper');"><i class="fa fa-key fa-fw"></i> Cambiar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<div class="modal fade" id="myModalUploadProfile">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Seleccionar Archivo</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/Perfil/seleccionar.jsp?idusuario=<%=usuario.getIdusuario()%>" width="100%" height="100">
                    <embed src="<%=request.getContextPath()%>/Perfil/seleccionar.jsp?idusuario=<%=usuario.getIdusuario()%>" width="100%" height="100"></embed>
                </object>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    window.closeModal = function(){
        $('#myModalUploadProfile').modal('hide');
        setTimeout(function(){
            go2to('Perfil/cambiar.jsp','page-wrapper');
        },250);
    };
    
    function remove(){
        $('#page-wrapper').LoadingOverlay("show");  
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) { 
            if (xhr.readyState === 4 && xhr.status === 200) {
                var scs = xhr.responseText.extractScript();
                scs.evalScript();
                $('#page-wrapper').LoadingOverlay("hide");
            }
        };
        xhr.open("GET", "/Overflow/Perfil/remove.jsp?idusuario=<%=usuario.getIdusuario()%>", true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    }
    
</script>
                    
<%
facade.close();
afacade.close();
%>
<%-- 
    Document   : descargar
    Created on : 29-may-2018, 12:00:35
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>
 
<%
int idusuario = 0;
if(request.getParameter("idusuario")!=null){
    idusuario = Integer.parseInt(String.valueOf(request.getParameter("idusuario")));
}

beans.Usuario usuario = facade.getUsuarioByID(idusuario);

String fileName = usuario.getNombre();
java.sql.Blob blob = new javax.sql.rowset.serial.SerialBlob(usuario.getFoto());
java.io.InputStream inputStream = blob.getBinaryStream();
int fileLength = inputStream.available();
ServletContext context = getServletContext();
String mimeType = context.getMimeType(fileName);
if (mimeType == null) {        
    mimeType = "application/octet-stream";
}              
response.setContentType(mimeType);
response.setContentLength(fileLength);
String headerKey = "Content-Disposition";
String headerValue = String.format("attachment; filename=\"%s\"", fileName);
response.setHeader(headerKey, headerValue);
java.io.OutputStream outStream = response.getOutputStream();
byte[] buffer = new byte[4096];
int bytesRead = -1;
while ((bytesRead = inputStream.read(buffer)) != -1) {
    outStream.write(buffer, 0, bytesRead);
}
inputStream.close();
outStream.close();     
facade.close();
%>
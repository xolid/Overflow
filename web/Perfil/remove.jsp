<%-- 
    Document   : subir
    Created on : 29-may-2018, 1:18:22
    Author     : solid
--%>

<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
 
<%
int idusuario = 0;
if(request.getParameter("idusuario")!=null){
    idusuario = Integer.parseInt(String.valueOf(request.getParameter("idusuario")));
}

beans.Usuario usuario = ufacade.getUsuarioByID(idusuario);
usuario.setFoto(null);
ufacade.updateUsuario(usuario);

ufacade.close();
%>

<script>
    go2to('Perfil/cambiar.jsp','page-wrapper');
</script>
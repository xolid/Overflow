<%-- 
    Document   : subir
    Created on : 29-may-2018, 1:18:22
    Author     : solid
--%>

<%@ page import="java.util.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.*" %>
<%@ page import="java.io.*" %>

<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
 
<%
int idusuario = 0;
if(request.getParameter("idusuario")!=null){
    idusuario = Integer.parseInt(String.valueOf(request.getParameter("idusuario")));
}

beans.Usuario usuario = ufacade.getUsuarioByID(idusuario);
    
/*FileItemFactory es una interfaz para crear FileItem*/
FileItemFactory file_factory = new DiskFileItemFactory();
/*ServletFileUpload esta clase convierte los input file a FileItem*/
ServletFileUpload servlet_up = new ServletFileUpload(file_factory);
servlet_up.setSizeMax(-1);
/*sacando los FileItem del ServletFileUpload en una lista */
List items = servlet_up.parseRequest(request);
for(int i=0;i<items.size();i++){
    /*FileItem representa un archivo en memoria que puede ser pasado al disco duro*/
    FileItem item = (FileItem) items.get(i);
    /*item.isFormField() false=input file; true=text field*/
    if (! item.isFormField()){
        /*guardado en el bean adjunto para despues preservarlo en base de datos*/
        usuario.setFoto(item.get());
        ufacade.updateUsuario(usuario);
    }
}

ufacade.close();
%>

<script>
    window.parent.closeModal();
</script>
<%-- 
    Document   : proceso
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="proceso" class="beans.Proceso" scope="page"/>
<jsp:setProperty name="proceso" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.ProcesoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(proceso.getIdproceso()==0){
            facade.saveProceso(proceso);
        }else{
            facade.updateProceso(proceso);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteProceso(proceso);        
    }
}
if(request.getParameter("id")!=null){
    int idproceso=Integer.parseInt((String)request.getParameter("id"));
    proceso=facade.getProcesoByID(idproceso);
}else{
    proceso.setIdproceso(0);
    proceso.setNombre("");
    proceso.setDescripcion("");
}%>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Catalogos Principales</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Definicion del Proceso</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form procesoe="form" id="form1" name="form1" action="proceso.jsp" method="post">
                            <input type="hidden" id="idproceso" name="idproceso" value="<%=proceso.getIdproceso()%>"/>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="nombre">Nombre: </label>
                                        <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=proceso.getNombre()%>" autocomplete="off" required autofocus/>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="descripcion">Descripcion de la Solicitud: </label>
                                        <textarea id="descripcion" name="descripcion" class="form-control input-sm" rows="5" required><%=proceso.getDescripcion()%></textarea>
                                    </div>      
                                </div>
                            </div>
                        </form>    
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:save('Proceso/proceso.jsp','Proceso/procesoListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                                <%if(proceso.getIdproceso()!=0){%>
                                    <button class="btn btn-outline-danger btn-sm" type="button" onclick="javascript:erase('Proceso/proceso.jsp','Proceso/procesoListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                                <%}%>
                                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="javascript:go2to('Proceso/procesoListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<%
facade.close();
%>
<%-- 
    Document   : borrar
    Created on : 29-may-2018, 1:58:09
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.RecursoFacade"/>

<%
int idrecurso = 0;
if(request.getParameter("idrecurso")!=null){
    idrecurso = Integer.parseInt(String.valueOf(request.getParameter("idrecurso")));
}
beans.Recurso recurso = facade.getRecursoByID(idrecurso);
facade.deleteRecurso(recurso);
%>

<script>
    go2to('Recurso/recursos.jsp?tipo=<%=recurso.getTipo()%>','page-wrapper');
</script>

<%
facade.close();
%>
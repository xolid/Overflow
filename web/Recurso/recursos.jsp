<%-- 
    Document   : conversaciones
    Created on : Jul 24, 2018, 5:43:58 PM
    Author     : czarate
--%>

<jsp:useBean id="facade" scope="page" class="facades.RecursoFacade"/>
<jsp:useBean id="rfacade" scope="page" class="facades.RolFacade"/>

<%
int tipo = -1;
if(request.getParameter("tipo")!=null){
    tipo = Integer.parseInt(String.valueOf(request.getParameter("tipo")));
}
%>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Recursos del Sistema</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Recursos del tipo <%=new catalogs.Tipo().getNombre(tipo)%></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-sm table-striped table-bordered table-condensed table-hover" id="dataTablesRecursos">
                            <thead>
                                <th>Fecha</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Rol</th>
                                <th>&nbsp;</th>
                            </thead>
                            <tbody>
                                <%for(beans.Recurso recurso:facade.getRecursoByTipo(tipo)){
                                    boolean showResource = true;
                                    if(((beans.Rol)session.getAttribute("rol")).getAdministra()==0){
                                        if(recurso.getRol()!=null){
                                            if(recurso.getRol().getIdrol()!=((beans.Rol)session.getAttribute("rol")).getIdrol()){
                                                showResource = false;
                                            }
                                        }
                                    }
                                    if(showResource){%>
                                        <tr>
                                            <td><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(recurso.getFecha())%></td>
                                            <td><a href="/Overflow/Recurso/descargar.jsp?idrecurso=<%=recurso.getIdrecurso()%>" target="_blank"><%=recurso.getNombre()%></a></td>
                                            <td><%=recurso.getDescripcion()%></td>
                                            <td><%=recurso.getRol()==null?"CUALQUIERA":recurso.getRol().getNombre()%></td>
                                            <td>
                                                <%if(((beans.Rol)session.getAttribute("rol")).getAdministra()==1){%>
                                                    <a href="#" onclick="javascript:borrar(<%=recurso.getIdrecurso()%>)"><i class="fa fa-times fa-fw"></i> Borrar</a>
                                                <%}%>
                                            </td>
                                        </tr>
                                    <%}%>
                                <%}%>
                            </tbody>
                        </table>   
                    </div>
                    <!-- /.card-body -->
                    <%if(((beans.Rol)session.getAttribute("rol")).getAdministra()==1){ %>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button class="btn btn-outline-primary btn-sm btn-sm" type="button" data-toggle="modal" data-target="#myModalUpload"><i class="fa fa-asterisk fa-fw"></i> Nuevo</button>
                                </div>
                            </div>
                        </div>
                    <%}%>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
           
<div class="modal fade" id="myModalUpload">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Seleccionar Archivo</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/Recurso/seleccionar.jsp?tipo=<%=tipo%>" width="100%" height="350">
                    <embed src="<%=request.getContextPath()%>/Recurso/seleccionar.jsp?tipo=<%=tipo%>" width="100%" height="350"></embed>
                </object>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
                
<script>
    $(document).ready(function() {
        $('#dataTablesRecursos').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "order": [[ 0,"desc"]],
            "columns": [
                { "width": "15%" },
                { "width": "20%" },
                { "width": "35%" },
                { "width": "20%" },
                { "width": "10%" }
            ]
        });
    });
    
    function borrar(idrecurso){
        $.LoadingOverlay("show");  
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) { 
            if (xhr.readyState === 4 && xhr.status === 200) {
                var scs = xhr.responseText.extractScript();
                scs.evalScript();
                $.LoadingOverlay("hide");
            }
        };
        xhr.open("GET", "/Overflow/Recurso/borrar.jsp?idrecurso="+idrecurso, true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    }
    
    window.closeModal = function(){
        $('#myModalUpload').modal('hide');
        setTimeout(function(){
            go2to('/Overflow/Recurso/recursos.jsp?tipo=<%=tipo%>','page-wrapper');
        },250);
    };
    
    $('#manuales').removeClass("active");
    $('#documentos').removeClass("active");
    $('#aplicaciones').removeClass("active");
    $('#dashboard').removeClass("active");
    $('#historico').removeClass("active");
    $('#redactar').removeClass("active");
    $('#delegar').removeClass("active");
    $('#ejecutar').removeClass("active");
    $('#grupos').removeClass("active");
    $('#procesos').removeClass("active");
    $('#roles').removeClass("active");
    $('#usuarios').removeClass("active");
    $('#conversacion_alta').removeClass('active');
    $('#conversacion_media').removeClass('active');
    $('#conversacion_baja').removeClass('active');
    $('#<%=tipo==0?"manuales":(tipo==1?"documentos":"aplicaciones")%>').addClass('active');
    
</script>
                            
<%
facade.close();
rfacade.close();
%>
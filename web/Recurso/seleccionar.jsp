<%-- 
    Document   : seleccionar
    Created on : 29-may-2018, 2:25:36
    Author     : solid
--%>

<jsp:useBean id="rfacade" scope="page" class="facades.RolFacade"/>

<%int tipo = 0;
if(request.getParameter("tipo")!=null){
    tipo = Integer.parseInt(String.valueOf(request.getParameter("tipo")));
}%>

<html>
    <head>
        <title>Herramienta de upload</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/font-awesome/css/font-awesome.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adminlte.css"/>
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/img/favicon.ico"/>
    </head>
    <body>
        <form role="form" id="form1" name="form1" action="subir.jsp" enctype="MULTIPART/FORM-DATA" method="post">
            <input name="tipo" id="tipo" type="hidden" value="<%=tipo%>"/>
            <div class="form-group">
                <label for="rol_idrol">Rol de usuario:</label>
                <select id="rol_idrol" name="rol_idrol" class="form-control input-sm">
                    <option value="0">CUALQUIERA</option>
                    <%for(beans.Rol rol:rfacade.getRolAll()){%>
                        <option value="<%=rol.getIdrol()%>"><%=rol.getNombre()%></option>
                    <%}%>
                </select>
            </div>
            <div class="form-group">
                <label for="descripcion">Descripcion:</label>
                <textarea id="descripcion" name="descripcion" class="form-control input-sm" rows="5" maxlength="2000"></textarea>
            </div>
            <div class="form-group">
                <span class="btn btn-default btn-file">
                    Seleccionar <input type="file" name="file"> 
                </span>
                <input type="submit" value="Subir" class="btn btn-success"/>
            </div>
        </form>
        <!-- Core Scripts - Include with every page -->
        <!-- jQuery -->
        <script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<%=request.getContextPath()%>/js/bootstrap.bundle.min.js"></script>
    </body>
</html>

<%
rfacade.close();
%>
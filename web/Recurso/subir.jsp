<%-- 
    Document   : subir
    Created on : 29-may-2018, 1:18:22
    Author     : solid
--%>

<%@ page import="java.util.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.*" %>
<%@ page import="java.io.*" %>

<jsp:useBean id="facade" scope="page" class="facades.RecursoFacade"/>
<jsp:useBean id="rfacade" scope="page" class="facades.RolFacade"/>
 
<%
int tipo = 0;
int rol_idrol = 0;
String descripcion = "";
/*FileItemFactory es una interfaz para crear FileItem*/
FileItemFactory file_factory = new DiskFileItemFactory();
/*ServletFileUpload esta clase convierte los input file a FileItem*/
ServletFileUpload servlet_up = new ServletFileUpload(file_factory);
servlet_up.setSizeMax(-1);
byte[] contenido = new byte[0];
String nombre = "";
List items = servlet_up.parseRequest(request);
for(int i=0;i<items.size();i++){
    FileItem item = (FileItem) items.get(i);
    if (!item.isFormField()){
         contenido = item.get();
         nombre = item.getName();
    }else{
        switch(item.getFieldName()){
            case "tipo":
                tipo = Integer.parseInt(item.getString());
                break;
            case "rol_idrol":
                rol_idrol = Integer.parseInt(item.getString());
                break;
            case "descripcion":
                descripcion = item.getString();
                break;
        }
    }
}

beans.Recurso recurso = new beans.Recurso();
recurso.setIdrecurso(0);
recurso.setNombre(nombre);
recurso.setDescripcion(descripcion);
recurso.setFecha(new java.util.Date());
recurso.setTipo(tipo);
recurso.setContenido(contenido);
recurso.setRol(rol_idrol==0?null:rfacade.getRolByID(rol_idrol));
facade.saveRecurso(recurso);       

rfacade.close();
facade.close();
%>

<script>
    window.parent.closeModal();
</script>
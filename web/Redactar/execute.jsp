<%-- 
    Document   : execute
    Created on : Aug 14, 2018, 1:09:17 PM
    Author     : czarate
--%>

<%
if(request.getParameter("idflujo")!=null && request.getParameter("operacion")!=null){ 
    int idflujo = Integer.parseInt(String.valueOf(request.getParameter("idflujo")));
    int idusuario = ((beans.Usuario)session.getAttribute("usuario")).getIdusuario();
    String operacion = String.valueOf(request.getParameter("operacion"));
    switch(operacion){
        case "cancelFlow":
            new navigation.Flow().cancelFlow(idusuario,idflujo);
            break;
        case "aproveFlow":
            new navigation.Flow().aproveFlow(idusuario,idflujo);
            break;
        case "completeFlow":
            new navigation.Flow().completeFlow(idusuario,idflujo);
            break;
        case "declineFlow":
            new navigation.Flow().declineFlow(idusuario,idflujo);
            break;
        case "sendFlow":
            new navigation.Flow().sendFlow(idusuario,idflujo);
            break;
    }
}
%>

<script>
    go2to('Redactar/flujoListado.jsp','page-wrapper');
</script>
<%-- 
    Document   : flujo
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="flujo" class="beans.Flujo" scope="page"/>
<jsp:setProperty name="flujo" property="idflujo" />
<jsp:setProperty name="flujo" property="nombre" />
<jsp:setProperty name="flujo" property="descripcion"/>
<jsp:setProperty name="flujo" property="estado"/>
<jsp:useBean id="facade" scope="page" class="facades.FlujoFacade"/>
<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.ProcesoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("fcreado")).equals("")){flujo.setFcreado(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(request.getParameter("fcreado"))));}
    if(!String.valueOf(request.getParameter("frequerido")).equals("")){flujo.setFrequerido(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").parse(String.valueOf(request.getParameter("frequerido"))));}
    if(!String.valueOf(request.getParameter("proceso_idproceso")).equals("")){flujo.setProceso(pfacade.getProcesoByID(Integer.parseInt(String.valueOf(request.getParameter("proceso_idproceso")))));}
    flujo.setUsuario((beans.Usuario)session.getAttribute("usuario"));
    flujo.setEstado(0);
    if(request.getParameter("accion").equals("1")){
        if(flujo.getIdflujo()==0){
            new navigation.Flow().createFlow(((beans.Usuario)session.getAttribute("usuario")), flujo);
        }else{
            new navigation.Flow().updateFlow(((beans.Usuario)session.getAttribute("usuario")), flujo);
        }
    }
}
if(request.getParameter("id")!=null){
    flujo=facade.getFlujoByID(Integer.parseInt((String)request.getParameter("id")));
}else{
    flujo.setIdflujo(0);
    flujo.setNombre("");
    flujo.setDescripcion("");
    flujo.setFcreado(new java.util.Date());
    flujo.setFrequerido(null);
    flujo.setProceso(new beans.Proceso());
    flujo.setUsuario((beans.Usuario)session.getAttribute("usuario"));
}%>


<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Solicitudes - <small>Redactar Solicitud</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegaci�n</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3 class="card-title">Definicion en el Flujo</h3>
                            </div>
                            <div class="col-lg-6 text-right">
                                <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#myModalLog"><i class="fa fa-file-text fa-fw"></i> Registro</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form role="form" id="form1" name="form1" action="flujo.jsp" method="post">
                            <input type="hidden" id="idflujo" name="idflujo" value="<%=flujo.getIdflujo()%>"/>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="usuario_nombre">Solicitud del Usuario: </label>
                                        <input class="form-control input-sm" id="usuario_nombre" name="usuario_nombre" type="text" value="<%=flujo.getUsuario()==null?"":flujo.getUsuario().getNombre()%>" readonly/>
                                    </div>      
                                </div>
                                <div class="col-lg-2 offset-lg-6">
                                    <div class="form-group">
                                        <label for="fcreado">Fecha de Creacion: </label>
                                        <input class="form-control input-sm" id="fcreado" name="fcreado" type="text" value="<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(flujo.getFcreado())%>" autocomplete="off" readonly required/>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="nombre">Nombre de la Solicitud: </label>
                                        <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=flujo.getNombre()%>" autocomplete="off" required/>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="descripcion">Descripcion de la Solicitud: </label>
                                        <textarea id="descripcion" name="descripcion" class="form-control input-sm" rows="5" autocomplete="off" required><%=flujo.getDescripcion()%></textarea>
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="proceso_idproceso">Proceso para la Solicitud: </label>
                                        <select id="proceso_idproceso" name="proceso_idproceso" class="form-control input-sm" required>
                                            <%for(beans.Proceso proceso:pfacade.getProcesoAll()){%>
                                                <option value="<%=proceso.getIdproceso()%>" <%=proceso.getIdproceso()==flujo.getProceso().getIdproceso()?"SELECTED":""%>><%=proceso.getNombre()%></option>
                                            <%}%>
                                        </select>
                                    </div>      
                                </div>
                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="myModalProceso">&nbsp;</label>
                                        <button class="btn btn-info btn-block" type="button" onclick="javascript:lanzador();"><i class="fa fa-eye fa-fw"></i></button>
                                    </div>      
                                </div>
                                <div class="col-lg-2 offset-lg-4">
                                    <div class="form-group">
                                        <label for="frequerido">Fecha Limite para Entrega: </label>
                                        <input class="form-control input-sm" id="frequerido" name="frequerido" type="text" value="<%=flujo.getFrequerido()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(flujo.getFrequerido())%>" autocomplete="off" required/>
                                    </div>      
                                </div>
                            </div>
                        </form>     
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:save('Redactar/flujo.jsp','Redactar/flujoListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="javascript:go2to('Redactar/flujoListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                                <%if(flujo.getIdflujo()!=0){%>
                                    <button class="btn btn-danger btn-sm" type="button" onclick="javascript:cancelar();"><i class="fa fa-times-circle-o fa-fw"></i> Cancelar</button>
                                <%}if(flujo.getIdflujo()!=0 && flujo.getEstado()==0){%>
                                    <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:enviar();"><i class="fa fa-mail-forward fa-fw"></i> Enviar</button>
                                <%}%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<div class="content-header" id="adjuntoListado"></div>
<div class="content-header" id="comentarioListado"></div>

<div class="modal fade" id="myModalLog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Registro de Eventos</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <object data="<%=request.getContextPath()%>/registro.jsp?idflujo=<%=flujo.getIdflujo()%>" width="100%" height="400">
                    <embed src="<%=request.getContextPath()%>/registro.jsp?idflujo=<%=flujo.getIdflujo()%>" width="100%" height="400"></embed>
                </object>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalProceso">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Datos del Proceso</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="nombre">Nombre del Proceso: </label>
                            <input class="form-control input-sm" id="nombre_proceso" name="nombre_proceso" type="text" maxlength="75" value="<%=flujo.getProceso().getNombre()%>" readonly/>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <object id="objectProcess" data="<%=request.getContextPath()%>/mapeo.jsp?idproceso=<%=flujo.getProceso().getIdproceso()%>" width="100%" height="400">
                            <embed id="embedProcess" src="<%=request.getContextPath()%>/mapeo.jsp?idproceso=<%=flujo.getProceso().getIdproceso()%>" width="100%" height="400"></embed>
                        </object>
                    </div>
                </div> 
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script lang="javascript">
    
    jQuery.datetimepicker.setLocale('es');
    jQuery(document).ready(function () {
        'use strict';
        jQuery('#frequerido').datetimepicker({
            format:'Y-m-d H:i',
            lang:'es',
            minDate:'0',
            allowTimes:[<%=new tools.Hora(new java.util.Date()).getEnableHours()%>],
            disabledWeekDays:[<%=new tools.Semana().getDisabledWeekDays()%>]
        });
    });
    
    function enviar(){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "�Confirma desea enviar solicitud?<br/><br/>Una vez enviado no podra editar la solicitud o subir mas informacion.",
            type: 'type-warning',
            closable: false,
            btnOKClass: 'btn-warning',
            callback: function(result) {
                if(result) {
                    $.LoadingOverlay("show");  
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function (e) { 
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            var scs = xhr.responseText.extractScript();
                            scs.evalScript();
                            $.LoadingOverlay("hide");
                        }
                    };
                    xhr.open("GET", "/Overflow/Redactar/execute.jsp?operacion=sendFlow&idflujo=<%=flujo.getIdflujo()%>", true);
                    xhr.setRequestHeader('Content-type', 'text/html');
                    xhr.send();
                }
            }
        });
    }
    
    function cancelar(){
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "�Confirma desea cancelar la solicitud?<br/><br/>Esta accion es imposible de deshacer.",
            type: 'type-danger',
            closable: false,
            btnOKClass: 'btn-danger',
            callback: function(result) {
                if(result) {
                    $.LoadingOverlay("show");  
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function (e) { 
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            var scs = xhr.responseText.extractScript();
                            scs.evalScript();
                            $.LoadingOverlay("hide");
                        }
                    };
                    xhr.open("GET", "/Overflow/Redactar/execute.jsp?operacion=cancelFlow&idflujo=<%=flujo.getIdflujo()%>", true);
                    xhr.setRequestHeader('Content-type', 'text/html');
                    xhr.send();
                }
            }
        });
    }
    
    function lanzador(){
        var ipproceso = document.getElementById("proceso_idproceso").options[document.getElementById("proceso_idproceso").selectedIndex].value;
        var nombre_proceso = document.getElementById("proceso_idproceso").options[document.getElementById("proceso_idproceso").selectedIndex].innerHTML;
        var url="/Overflow/mapeo.jsp?idproceso="+ipproceso;
        document.getElementById("objectProcess").setAttribute('data',url);
        document.getElementById("embedProcess").setAttribute('src',url);
        document.getElementById("nombre_proceso").value = nombre_proceso;
        $('#myModalProceso').modal('show');
    }
    
    function refreshArchivos(){
        xmlhttpPost2EXEC('Adjunto/adjuntoListado.jsp?idflujo=<%=flujo.getIdflujo()%>','adjuntoListado', null, 'xmlhttpPost2(\'Comentario/comentarioListado.jsp?idflujo=<%=flujo.getIdflujo()%>\',\'comentarioListado\',null)');
    }

    <%=flujo.getIdflujo()==0?"":"refreshArchivos();"%>
</script>

<%
facade.close();
ufacade.close();
pfacade.close();
%>
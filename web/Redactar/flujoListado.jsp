<%-- 
    Document   : fujoListado
    Created on : Jan 9, 2016, 4:50:18 PM
    Author     : czara
--%>


<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Solicitudes - <small>En Curso</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Listado de Flujos Activos</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped table-bordered table-condensed table-hover" id="dataTablesFlujo">
                                <thead>
                                    <th>[B] Folio</th>
                                    <th>[B] Nombre</th>
                                    <th>[B] Descripcion</th>
                                    <th>[B] Fecha Solicitado</th>
                                    <th>Fecha Requerido</th>
                                    <th>Estado</th>
                                    <th>Avance</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:go2to('Redactar/flujo.jsp','page-wrapper');"><i class="fa fa-asterisk fa-fw"></i> Nuevo</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-footer -->
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#dataTablesFlujo').dataTable({
            "retrieve": true,
            "processing": true,  
            "serverSide": true,
            "ajax": "Redactar/processFlujo.jsp",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "columns": [
                { "width": "10%" },
                { "width": "20%" },
                { "width": "23%" },
                { "width": "10%" },
                { "width": "10%" },
                { "width": "7%" },
                { "width": "20%" }
            ],
            columnDefs: [{
                targets: 1,
                render: function ( data, type, row ) {
                    return data.length > 50 ? data.substr( 0, 50 ) + '...' : data;
                }
            },
            {
                targets: 2,
                render: function ( data, type, row ) {
                    return data.length > 100 ? data.substr( 0, 100 ) + '...' : data;
                }
            }]
        });
        
        $('#manuales').removeClass("active");
        $('#documentos').removeClass("active");
        $('#aplicaciones').removeClass("active");
        $('#dashboard').removeClass("active");
        $('#historico').removeClass("active");
        $('#redactar').removeClass("active");
        $('#delegar').removeClass("active");
        $('#ejecutar').removeClass("active");
        $('#grupos').removeClass("active");
        $('#procesos').removeClass("active");
        $('#roles').removeClass("active");
        $('#usuarios').removeClass("active");
        $('#conversacion_alta').removeClass('active');
        $('#conversacion_media').removeClass('active');
        $('#conversacion_baja').removeClass('active');
        $('#redactar').addClass('active');
    });
</script>
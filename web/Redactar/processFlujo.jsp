<%-- 
    Document   : process2
    Created on : Feb 7, 2016, 7:18:03 PM
    Author     : czara
--%>
 
<%@page import="java.util.*"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>

<jsp:useBean id="facade" scope="page" class="facades.FlujoFacade"/>
<jsp:useBean id="tfacade" scope="page" class="facades.TareaFacade"/>

<%  
String[] cols = { "idflujo","nombre","descripcion","fsolicitado","frequerido","estado"};
JSONObject result = new JSONObject();
JSONArray array = new JSONArray();
int amount = 10;
int start = 0;
int echo = 0;
int col = 0;
String dir = "asc";
String sStart = request.getParameter("start");
String sAmount = request.getParameter("length");
String sEcho = request.getParameter("draw");
String sCol = request.getParameter("order[0][column]");
String sdir = request.getParameter("order[0][dir]");
String buscar = request.getParameter("search[value]");
if (sStart != null) {
    start = Integer.parseInt(sStart);
    if (start < 0)
        start = 0;
}
if (sAmount != null) {
    amount = Integer.parseInt(sAmount);
    if (amount < 10 || amount > 100)
        amount = 10;
}
if (sEcho != null) {
    echo = Integer.parseInt(sEcho);
}
if (sCol != null) {
    col = Integer.parseInt(sCol);
    if (col < 0 || col > 5)
        col = 0;
}
if (sdir != null) {
    if (!sdir.equals("asc"))
        dir = "desc";
}
String colName = cols[col];
long total = facade.getTotal();
int idautor = ((beans.Usuario)session.getAttribute("usuario")).getIdusuario();
List<beans.Flujo> flujos = facade.getFlujoForProcessByUsuario(buscar, idautor, colName, dir, start, amount);
for(beans.Flujo p:flujos){
    JSONArray ja = new JSONArray();
    String target = "visualizar";
    if(p.getEstado()<1){
        target = "flujo";
    }
    ja.add("<a href=\"#\" onclick=\"javascript:go2to('Redactar/"+target+".jsp?id="+p.getIdflujo()+"','page-wrapper')\">"+p.getIdflujo()+"</a>");
    ja.add(p.getNombre());
    ja.add(p.getDescripcion());
    ja.add(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(p.getFrequerido()));
    ja.add(p.getFsolicitado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(p.getFsolicitado()));
    String color2 = "info";
    switch(p.getEstado()){
        case 0:
            color2 = "warning";
            break;
        case 1:
            color2 = "info";
            break;
        case 2:
            color2 = "danger";
            break;
        case 3:
            color2 = "primary";
            break;
        case 4:
            color2 = "success";
            break;
        case 5:
            color2 = "danger";
            break;
        case 6:
            color2 = "success";
            break;
        case 7:
            color2 = "danger";
            break;
            
    }
    ja.add("<small class=\"badge badge-"+color2+"\"> "+new catalogs.Estado().getNombre(p.getEstado())+"</small>");
    double total_tareas = 0;
    double total_terminadas = 0;
    for(beans.Tarea tarea:tfacade.getTareaByFlujo(p.getIdflujo())){
        total_tareas++;
        if(tarea.getEstado()==4&&tarea.getFliberado()!=null)
            total_terminadas++;
    }
    double porcentaje = 0;
    if(total_tareas>0){
        porcentaje = (total_terminadas/total_tareas)*100.0;
    }
    String color = "secundary";
    if(porcentaje==100){
        color = "success";
    }
    if(porcentaje>66.66&&porcentaje<100){
        color = "warning";
    }
    if(porcentaje<66.66){
        color = "danger";
    }
    ja.add("<div class=\"progress-group\">Avance de Tareas<span class=\"float-right\"><b>"+new Double(total_terminadas).intValue()+"</b>/"+new Double(total_tareas).intValue()+"</span><div class=\"progress progress-sm\"><div class=\"progress-bar bg-"+color+"\" style=\"width: "+porcentaje+"%\"></div></div></div>");
    array.add(ja);
}
long totalAfterFilter = facade.getFlujoForProcessCountByUsuario(buscar, idautor);
result.put("draw", sEcho);
result.put("recordsTotal", total);
result.put("recordsFiltered", totalAfterFilter);
result.put("data", array);
response.setContentType("application/json");
response.setHeader("Cache-Control", "no-store");
out.print(result);

facade.close();    
tfacade.close();
%>


<%-- 
    Document   : rol
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="rol" class="beans.Rol" scope="page"/>
<jsp:setProperty name="rol" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.RolFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(rol.getIdrol()==0){
            facade.saveRol(rol);
        }else{
            facade.updateRol(rol);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteRol(rol);        
    }
}
if(request.getParameter("id")!=null){
    int idrol=Integer.parseInt((String)request.getParameter("id"));
    rol=facade.getRolByID(idrol);
}else{
    rol.setIdrol(0);
    rol.setNombre("");
    rol.setConsulta(0);
    rol.setEjecuta(0);
    rol.setDelega(0);
    rol.setRedacta(0);
    rol.setAdministra(0);
}%>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Catalogos del Sistema</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Definicion del Rol</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form role="form" id="form1" name="form1" action="rol.jsp" method="post">
                            <input type="hidden" id="idrol" name="idrol" value="<%=rol.getIdrol()%>"/>
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="form-group">
                                        <label for="nombre">Nombre: </label>
                                        <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=rol.getNombre()%>" autocomplete="off" required autofocus/>
                                    </div>      
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="administra">Administra: </label>
                                        <select id="administra" name="administra" class="form-control input-sm">
                                            <option value="0" <%=rol.getAdministra()==0?"SELECTED":""%>>REVOCADO</option>
                                            <option value="1" <%=rol.getAdministra()==1?"SELECTED":""%>>ASIGNADO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="consulta">Consulta: </label>
                                        <select id="consulta" name="consulta" class="form-control input-sm">
                                            <option value="0" <%=rol.getConsulta()==0?"SELECTED":""%>>REVOCADO</option>
                                            <option value="1" <%=rol.getConsulta()==1?"SELECTED":""%>>ASIGNADO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="ejecuta">Ejecuta: </label>
                                        <select id="ejecuta" name="ejecuta" class="form-control input-sm">
                                            <option value="0" <%=rol.getEjecuta()==0?"SELECTED":""%>>REVOCADO</option>
                                            <option value="1" <%=rol.getEjecuta()==1?"SELECTED":""%>>ASIGNADO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="delega">Delega: </label>
                                        <select id="delega" name="delega" class="form-control input-sm">
                                            <option value="0" <%=rol.getDelega()==0?"SELECTED":""%>>REVOCADO</option>
                                            <option value="1" <%=rol.getDelega()==1?"SELECTED":""%>>ASIGNADO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="redacta">Redacta: </label>
                                        <select id="redacta" name="redacta" class="form-control input-sm">
                                            <option value="0" <%=rol.getRedacta()==0?"SELECTED":""%>>REVOCADO</option>
                                            <option value="1" <%=rol.getRedacta()==1?"SELECTED":""%>>ASIGNADO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:save('Rol/rol.jsp','Rol/rolListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                                <%if(rol.getIdrol()!=0){%>
                                    <button class="btn btn-outline-danger btn-sm" type="button" onclick="javascript:erase('Rol/rol.jsp','Rol/rolListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                                <%}%>
                                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="javascript:go2to('Rol/rolListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<%
facade.close();
%>
<%-- 
    Document   : usuario
    Created on : Jan 9, 2016, 7:42:35 PM
    Author     : czara
--%>

<jsp:useBean id="usuario" class="beans.Usuario" scope="page"/>
<jsp:setProperty name="usuario" property="idusuario" />
<jsp:setProperty name="usuario" property="nombre" />
<jsp:setProperty name="usuario" property="puesto" />
<jsp:setProperty name="usuario" property="mail" />
<jsp:setProperty name="usuario" property="pass" />
<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="rfacade" scope="page" class="facades.RolFacade"/>

<%if(request.getParameter("accion")!=null){
    if(!String.valueOf(request.getParameter("pass")).equals("")){usuario.setPass(tools.MD5.getMD5(usuario.getPass()));}
    if(!String.valueOf(request.getParameter("fingreso")).equals("")){usuario.setFingreso(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(request.getParameter("fingreso"))));}
    if(!String.valueOf(request.getParameter("fegreso")).equals("")){usuario.setFegreso(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(request.getParameter("fegreso"))));}
    if(!String.valueOf(request.getParameter("rol")).equals("")){usuario.setRol(rfacade.getRolByID(Integer.parseInt(String.valueOf(request.getParameter("rol")))));}
    if(request.getParameter("accion").equals("1")){
        if(usuario.getIdusuario()==0){
            facade.saveUsuario(usuario);
        }else{
            facade.updateUsuario(usuario);
        }
    }
    if(request.getParameter("accion").equals("2")){
        facade.deleteUsuario(usuario);        
    }
}
if(request.getParameter("id")!=null){
    usuario=facade.getUsuarioByID(Integer.parseInt((String)request.getParameter("id")));
    usuario.setPass("");
}else{
    usuario.setIdusuario(0);
    usuario.setNombre("");
    usuario.setPuesto("");
    usuario.setMail("");
    usuario.setPass("");
    usuario.setFingreso(new java.util.Date());
    usuario.setFegreso(null);
    usuario.setRol(new beans.Rol());
}%>


<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Catalogos del Sistema</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements disabled -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Definicion del Usuario</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form role="form" id="form1" name="form1" action="usuario.jsp" method="post">
                            <input type="hidden" id="idusuario" name="idusuario" value="<%=usuario.getIdusuario()%>"/>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="nombre">Nombre: </label>
                                        <input class="form-control input-sm" id="nombre" name="nombre" type="text" maxlength="75" value="<%=usuario.getNombre()%>" autocomplete="off" required autofocus/>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="puesto">Puesto: </label>
                                        <input class="form-control input-sm" id="puesto" name="puesto" type="text" maxlength="45" value="<%=usuario.getPuesto()%>" autocomplete="off" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="mail">Mail: </label>
                                        <input class="form-control input-sm" id="mail" name="mail" type="email" maxlength="45" value="<%=usuario.getMail()%>" autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="password">Password: </label>
                                        <input class="form-control input-sm" id="pass" name="pass" type="password" maxlength="45" value="<%=usuario.getPass()%>" autocomplete="off" required/>
                                    </div>        
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>Fecha Ingreso</label>
                                        <input class="form-control input-sm" id="fingreso" name="fingreso" type="text" value="<%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(usuario.getFingreso())%>" autocomplete="off" required/>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>Fecha Egreso</label>
                                        <input class="form-control input-sm" id="fegreso" name="fegreso" type="text" value="<%=usuario.getFegreso()!=null?new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(usuario.getFegreso()):""%>" autocomplete="off"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="rol">Rol</label>
                                        <select id="rol" name="rol" class="form-control input-sm">
                                            <%for(beans.Rol rol:rfacade.getRolAll()){%>
                                                <option value="<%=rol.getIdrol()%>" <%=rol.getIdrol()==usuario.getRol().getIdrol()?"SELECTED":""%>><%=rol.getNombre()%></option>
                                            <%}%>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:save('Usuario/usuario.jsp','Usuario/usuarioListado.jsp','form1','page-wrapper');"><i class="fa fa-save fa-fw"></i> Guardar</button>
                                <%if(usuario.getIdusuario()!=0){%>
                                    <button class="btn btn-outline-danger btn-sm" type="button" onclick="javascript:erase('Usuario/usuario.jsp','Usuario/usuarioListado.jsp','form1','page-wrapper');"><i class="fa fa-eraser fa-fw"></i> Borrar</button>
                                <%}%>
                                <button class="btn btn-outline-secondary btn-sm" type="button" onclick="javascript:go2to('Usuario/usuarioListado.jsp','page-wrapper');"><i class="fa fa-backward fa-fw"></i> Volver</button>                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
    jQuery.datetimepicker.setLocale('es');
    jQuery(document).ready(function () {
        'use strict';
        jQuery('#fingreso').datetimepicker({
            format:'Y-m-d H:i:s',
            lang:'es',
            maxDate:'0'
        });
        jQuery('#fegreso').datetimepicker({
            format:'Y-m-d H:i:s',
            lang:'es',
            minDate:'0'
        });
    });
</script>

<%
facade.close();
rfacade.close();
%>
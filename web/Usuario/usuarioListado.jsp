<%-- 
    Document   : usuarioListado
    Created on : Jan 9, 2016, 4:50:18 PM
    Author     : czara
--%>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Catalogos del Sistema</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                    <li class="breadcrumb-item active">Navegación</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Listado de Usuarios Disponibles</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered table-striped" id="dataTablesUsuario">
                                <thead>
                                    <th>[B] Nombre</th>
                                    <th>[B] Mail</th>
                                    <th>[B] Fecha Ingreso</th>
                                    <th>Fecha Egreso</th>
                                    <th>Rol</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-outline-primary btn-sm" type="button" onclick="javascript:go2to('Usuario/usuario.jsp','page-wrapper');"><i class="fa fa-asterisk fa-fw"></i> Nuevo</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#dataTablesUsuario').dataTable({
            "retrieve": true,
            "processing": true,  
            "serverSide": true,
            "ajax": "Usuario/processUsuario.jsp",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            }
        });
        
        $('#manuales').removeClass("active");
        $('#documentos').removeClass("active");
        $('#aplicaciones').removeClass("active");
        $('#dashboard').removeClass("active");
        $('#historico').removeClass("active");
        $('#redactar').removeClass("active");
        $('#delegar').removeClass("active");
        $('#ejecutar').removeClass("active");
        $('#grupos').removeClass("active");
        $('#procesos').removeClass("active");
        $('#roles').removeClass("active");
        $('#usuarios').removeClass("active");
        $('#conversacion_alta').removeClass('active');
        $('#conversacion_media').removeClass('active');
        $('#conversacion_baja').removeClass('active');
        $('#usuarios').addClass('active');
    });
</script>
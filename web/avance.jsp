<%-- 
    Document   : avance
    Created on : 5/07/2018, 12:49:01 AM
    Author     : CarlosAlberto
--%>

<jsp:useBean id="facade" scope="page" class="facades.TareaFacade"/>

<%
int idflujo = 0;
if(request.getParameter("idflujo")!=null){
    idflujo = Integer.parseInt(String.valueOf(request.getParameter("idflujo")));
}
java.util.List<String> cadenas = new java.util.ArrayList();
java.util.List<beans.Tarea> tareas = facade.getTareaByFlujo(idflujo);
for(beans.Tarea tarea:tareas){
    String registro = "";
    String tooltip = "<strong>En Espera</strong>";
    if(tarea.getFrecibido()!=null){
        tooltip = "<strong>Estado:</strong> <b><i>"+new catalogs.Estado().getNombre(tarea.getEstado())+"</b></i><br/>";
        tooltip += "<strong>Fecha Recibido:</strong> <i>"+(tarea.getFrecibido()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFrecibido()))+"</i><br/><br/>";
        if(tarea.getFasignado()!=null){
            tooltip += "<strong>Usuario Asignado:</strong> "+(tarea.getUsuario()==null?"PENDIENTE ASIGNACION":tarea.getUsuario().getNombre())+"<br/>";
            tooltip += "<strong>Fecha Asignado:</strong> <i>"+(tarea.getFasignado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFasignado()))+"</i><br/>";
            tooltip += "<strong>Tiempo Limite por Proceso:</strong> <i>"+(tarea.getActividad().getDuracion())+" Hrs.</i><br/>";
            tooltip += "<strong>Fecha Limite por Proceso:</strong> <i>"+(tarea.getFproceso()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFproceso()))+"</i><br/><br/>";
        }
        if(tarea.getFliberado()!=null){
            tooltip += "<strong>Fecha Liberado:</strong> <i>"+(tarea.getFliberado()==null?"":new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFliberado()))+"</i><br/>";
            registro = "{id: "+tarea.getIdtarea()+", className: 'blue', content: '"+tarea.getActividad().getNombre()+"', title: '"+tooltip+"', start: '"+new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFrecibido())+"', end: '"+new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFliberado())+"'}";
        }else{
            registro = "{id: "+tarea.getIdtarea()+", className: 'green', content: '"+tarea.getActividad().getNombre()+"', title: '"+tooltip+"', start: '"+new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tarea.getFrecibido())+"'}"; 
        }
    }else{
        registro = "{id: "+tarea.getIdtarea()+", className: 'gray', content: '"+tarea.getActividad().getNombre()+"', title: '"+tooltip+"', start: '"+new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+"'}"; 
    }
    cadenas.add(registro);
}
%>

<html>
    <head>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/loadingoverlay.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/vis.js"></script>
        <link href="<%=request.getContextPath()%>/css/vis-timeline-graph2d.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="visualization"></div>
        <script type="text/javascript">
            $.LoadingOverlay("show");
            var container = document.getElementById('visualization');
            var items = new vis.DataSet([
                <%=String.join(",",cadenas)%>
            ]);
            var options = {
                width: '100%',
                height: '100%',
                margin: {
                    item: 20
                }
            };
            var timeline = new vis.Timeline(container, items, options);
            $.LoadingOverlay("hide");
        </script>
    </body>
</html>

<%
facade.close();
%>
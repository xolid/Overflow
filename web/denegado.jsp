<%-- 
    Document   : error
    Created on : Oct 10, 2014, 11:26:57 PM
    Author     : CarlosAlberto
--%>

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Evento de seguridad detectado</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())%></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fa fa-info"></i> Acceso denegado...</h5>
                    No tiene los permisos necesarios para acceder a esta zona del sistema. <a href="<%=new properties.PropertiesManager().getProperty("URL")%>">IR A LOGIN</a>
                </div>
            </div>
        </div>
    </div>
</section>

        

<%-- 
    Document   : error1
    Created on : Feb 9, 2016, 12:56:58 AM
    Author     : czara
--%>

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Evento anomalo detectado</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())%></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fa fa-ban"></i> Error...</h5>
                    Un error a ocurrido en el sistema, favor de reportarlo al departamento responsable. <a href="<%=new properties.PropertiesManager().getProperty("URL")%>/principal.jsp">IR A PAGINA PRINCIPAL</a>
                </div>
            </div>
        </div>
    </div>
</section>
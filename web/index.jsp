<%-- 
    Document   : index
    Created on : 12/07/2018, 11:38:43 PM
    Author     : CarlosAlberto
--%>

<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="rfacade" scope="page" class="facades.RolFacade"/>

<%
session.removeAttribute("usuario");
session.removeAttribute("rol");
if(request.getParameter("mail")!=null&&request.getParameter("pass")!=null){
    String mail=request.getParameter("mail");
    String pass=tools.MD5.getMD5(request.getParameter("pass"));
    String key=(String)session.getAttribute("key");
    if(key.equals(request.getParameter("captcha"))){
        beans.Usuario usuario=ufacade.getUsuarioByMailPass(mail,pass);
        if(usuario!=null){
            listeners.ServletListener.closeSession(usuario.getIdusuario());
            session.setAttribute("usuario",usuario);
            session.setAttribute("rol", rfacade.getRolByID(usuario.getRol().getIdrol()));
            response.sendRedirect(request.getContextPath()+"/principal.jsp");
        }
    }
}
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OverFlow | Aplicación de Procesos</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/font-awesome/css/font-awesome.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adminlte.css">
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/img/favicon.ico"/>
        <!-- iCheck -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/blue.css">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="<%=request.getContextPath()%>/index.jsp"><b>Over</b>FLOW</a>
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Inicio de sesion</p>
                    <form role="form" name="form1" id="form1" method="post" action="<%=request.getContextPath()%>/index.jsp">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control input-sm" placeholder="Mail" name="mail" id="mail" type="mail" autofocus required/>
                            </div>
                            <div class="form-group">
                                <input class="form-control input-sm" placeholder="Password" name="pass" id="pass" type="password" required/>
                            </div>
                            <div class="form-group">
                                Captcha: <img src="captcha.jsp"/>
                            </div>
                            <div class="form-group">
                                <input class="form-control input-sm" placeholder="Captcha" name="captcha" id="captcha" type="text" maxlength="5" autocomplete="off" required/>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block btn-sm" onclick="javascript:login();">Ingresar</button>
                        </fieldset>
                    </form>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        <!-- /.login-box -->
        <!-- jQuery -->
        <script src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<%=request.getContextPath()%>/js/bootstrap.bundle.min.js"></script>
        <!-- iCheck -->
        <script src="<%=request.getContextPath()%>/js/icheck.min.js"></script>
        <script>
            function login(){
                if(document.getElementById('mail').validity.valid && document.getElementById('pass').validity.valid && document.getElementById('captcha').validity.valid){
                    document.forms['form1'].submit();
                }
            }
        </script>
    </body>
</html>

<%
ufacade.close();
rfacade.close();
%>
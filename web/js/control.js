/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global BootstrapDialog */

function save(url, urlRedirect, formName, div) {
    var str = validInputs(formName);
    if (str === "") {
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "Confirma desea salvar?",
            type: 'type-warning',
            closable: false,
            btnOKClass: 'btn-warning',
            callback: function(result) {
                if(result) {
                    xmlhttpPostEXEC(url, div, 'accion=1&' + inputsToParams(formName), 'xmlhttpPost2(\'' + urlRedirect + '\',\'' + div + '\',\'\')');
                }
            }
        });
    } else {
        BootstrapDialog.alert({
            title: "Mensaje del Sistema",
            message: "<strong>Se encontraron los siguientes errores:</strong><br/><br/>" + str,
            type: 'type-danger',
            closable: false,
            btnOKClass: 'btn-danger'
        });
    }
}

function saveNORedirect(url, formName, div) {
    var str = validInputs(formName);
    if (str === "") {
        BootstrapDialog.confirm({
            title: "Mensaje del Sistema",
            message: "Confirma desea salvar?",
            type: 'type-warning',
            closable: false,
            btnOKClass: 'btn-warning',
            callback: function(result) {
                if(result) {
                    xmlhttpPost2(url, div, 'accion=1&' + inputsToParams(formName));
                }
            }
        });
    } else {
        BootstrapDialog.alert({
            title: "Mensaje del Sistema",
            message: "<strong>Se encontraron los siguientes errores:</strong><br/><br/>" + str,
            type: 'type-danger',
            closable: false,
            btnOKClass: 'btn-danger'
        });
    }
}

function erase(url, urlRedirect, formName, div) {
    BootstrapDialog.confirm({
        title: "Mensaje del Sistema",
        message: "Confirma desea borrar?",
        type: 'type-warning',
        closable: false,
        btnOKClass: 'btn-warning',
        callback: function(result) {
            if(result) {
                xmlhttpPostEXEC(url, div, 'accion=2&' + inputsToParams(formName), 'xmlhttpPost2(\'' + urlRedirect + '\',\'' + div + '\',\'\')');
            }
        }
    });
}

function close(strDIV) {
    document.getElementById(strDIV).innerHTML = '';
}

function clean(formName) {
    restartInputs(formName);
}

function restartInputs(formName) {
    var elem = document.getElementById(formName).elements;
    for (var i = 0; i < elem.length; i++) {
        var inputTagName = elem[i].tagName;
        if (inputTagName === 'INPUT') {
            document.getElementById(elem[i].id).value = "";
        }
        if (inputTagName === 'SELECT') {
            document.getElementById(elem[i].id);
        }
        if (inputTagName === 'TEXTAREA') {
            document.getElementById(elem[i].id).value = "";
        }
    }
}

function inputsToParams(formName) {
    var string = '';
    var elem = document.getElementById(formName).elements;
    for (var i = 0; i < elem.length; i++) {
        var inputTagName = elem[i].tagName;
        if (inputTagName === 'INPUT' || inputTagName === 'SELECT' || inputTagName === 'TEXTAREA') {
            var inputName = elem[i].name;
            var inputValue = '';
            switch (elem[i].type) {
                case 'file':
                    inputValue = elem[i].value.replace("C:\\fakepath\\", "");
                    break;
                case 'select-multiple':
                    inputValue = getSelectedItems(elem[i]);
                    break;
                case 'datetime-local':
                    inputValue = elem[i].value.replace("T", " ");
                    break;
                case 'radio':
                    inputValue = getRadioCheckedValue(formName,inputName);
                    break;
                default:
                    inputValue = elem[i].value;
                    break;
            }
            if (string === "") {
                string = inputName + "=" + inputValue;
            } else {
                string = string + "&" + inputName + "=" + inputValue;
            }
        }
    }
    return string;
}

function getRadioCheckedValue(formName,radio){
    var oRadio = document.getElementById(formName).elements[radio];
    for(var i = 0; i < oRadio.length; i++){
        if(oRadio[i].checked){
            return oRadio[i].value;
        }
    }
   return '';
}

function validInputs(formName) {
    var string = '';
    var elem = document.getElementById(formName).elements;
    for (var i = 0; i < elem.length; i++) {
        var inputTagName = elem[i].tagName;
        if (inputTagName === 'INPUT') {
            if (!elem[i].validity.valid) {
                string += "* " + elem[i].name + " no valido.<br/>";
            }
        }
        if (inputTagName === 'SELECT') {
            if (elem[i].type === 'select-multiple') {
                if (getSelectedItemsNumber(elem[i]) <= 0 && elem[i].required) {
                    string += "* " + elem[i].name + " debe seleccionar almenos un elemento.<br/>";
                }
            } else {
                if (elem[i].options.length === 0) {
                    string += "* " + elem[i].name + " no contiene elementos.<br/>";
                }
            }
        }
        if (inputTagName === 'TEXTAREA') {
            if (elem[i].value === "" && elem[i].required) {
                string += "* " + elem[i].name + " esta vacio.<br/>";
            }
        }
    }
    return string;
}

function getSelectedItems(element) {
    var selectedList = "";
    for (var i = 0; i < element.options.length; i++) {
        if (element.options[i].selected) {
            if (selectedList === "") {
                selectedList += element.options[i].value;
            } else {
                selectedList += "," + element.options[i].value;
            }
        }
    }
    return selectedList;
}

function getSelectedItemsNumber(element) {
    var number = 0;
    for (var i = 0; i < element.options.length; i++) {
        if (element.options[i].selected) {
            number++;
        }
    }
    return number;
}

function go2to(url, div) {
    xmlhttpPost2(url, div, '');
}

function go2toEXEC(url, div, fun) {
    xmlhttpPost2EXEC(url, div, '', fun);
}

function xmlhttpPost2(strURL, strDIV, strPAR) {
    var self = this;
    if (window.XMLHttpRequest) {
        self.xmlHttpReq = new XMLHttpRequest();
    }
    else {
        if (window.ActiveXObject) {
            self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    self.xmlHttpReq.open('POST', strURL, true);
    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    self.xmlHttpReq.onreadystatechange = function() {
        if (self.xmlHttpReq.readyState === 4) {
            if(self.xmlHttpReq.status === 404){
                document.getElementById(strDIV).innerHTML = '<div class="row"><div class="col-lg-12"><h1 class="page-header">Pagina bajo construccion</h1></div><!-- /.col-lg-12 --></div><!-- /.row -->';
            }else{
                var scs=self.xmlHttpReq.responseText.extractScript();
                document.getElementById(strDIV).innerHTML=self.xmlHttpReq.responseText;
                scs.evalScript();
            }
        } else {
            document.getElementById(strDIV).innerHTML = '<div><img src="/Overflow/img/loading.gif"/></div>';
        }
    };
    self.xmlHttpReq.send(strPAR);
}

function xmlhttpPostEXEC(strURL, strDIV, strPAR, strFUN) {
    var self = this;
    if (window.XMLHttpRequest) {
        self.xmlHttpReq = new XMLHttpRequest();
    }
    else {
        if (window.ActiveXObject) {
            self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    self.xmlHttpReq.open('POST', strURL, true);
    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    self.xmlHttpReq.onreadystatechange = function() {
        if (self.xmlHttpReq.readyState === 4) {
            if(self.xmlHttpReq.status === 404){
                document.getElementById(strDIV).innerHTML = '<div class="row"><div class="col-lg-12"><h1 class="page-header">Pagina bajo construccion</h1></div><!-- /.col-lg-12 --></div><!-- /.row -->';
            }else{
                document.getElementById(strDIV).innerHTML = self.xmlHttpReq.responseText;
                window.setTimeout(strFUN, 100);
            }
        } else {
            document.getElementById(strDIV).innerHTML = '<div><img src="/Overflow/img/loading.gif"/></div>';
        }
    };
    self.xmlHttpReq.send(strPAR);
}

function xmlhttpPost2EXEC(strURL, strDIV, strPAR, strFUN) {
    var self = this;
    if (window.XMLHttpRequest) {
        self.xmlHttpReq = new XMLHttpRequest();
    }
    else {
        if (window.ActiveXObject) {
            self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    self.xmlHttpReq.open('POST', strURL, true);
    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    self.xmlHttpReq.onreadystatechange = function() {
        if (self.xmlHttpReq.readyState === 4) {
            if(self.xmlHttpReq.status === 404){
                document.getElementById(strDIV).innerHTML = '<div class="row"><div class="col-lg-12"><h1 class="page-header">Pagina bajo construccion</h1></div><!-- /.col-lg-12 --></div><!-- /.row -->';
            }else{
                var scs=self.xmlHttpReq.responseText.extractScript();
                document.getElementById(strDIV).innerHTML=self.xmlHttpReq.responseText;
                scs.evalScript();
                window.setTimeout(strFUN, 100);
            }
        } else {
            document.getElementById(strDIV).innerHTML = '<div><img src="/Overflow/img/loading.gif"/></div>';
        }
    };
    self.xmlHttpReq.send(strPAR);
}
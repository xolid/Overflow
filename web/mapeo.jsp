<%-- 
    Document   : avance
    Created on : 5/07/2018, 12:49:01 AM
    Author     : CarlosAlberto
--%>

<jsp:useBean id="facade" scope="page" class="facades.ActividadFacade"/>

<%
int idproceso = 0;
if(request.getParameter("idproceso")!=null){
    idproceso = Integer.parseInt(String.valueOf(request.getParameter("idproceso")));
}
Integer memory = null;
java.util.List<String> nodes = new java.util.ArrayList();
java.util.List<String> edges = new java.util.ArrayList();
for(beans.Actividad actividad:facade.getActividadByProceso(idproceso)){
    String node = "{id: "+actividad.getOrden()+", label: 'Actividad: "+actividad.getNombre()+"\\n Grupo: "+actividad.getGrupo().getNombre()+"\\nDuracion: "+actividad.getDuracion()+" horas',shape: 'box'}";
    nodes.add(node);
    if(memory != null){
        String edge = "{from: "+memory+", to: "+actividad.getOrden()+", arrows:'to'}";
        edges.add(edge);
        memory = actividad.getOrden();
    }else{
        memory = actividad.getOrden(); 
    }
    
}
%>

<html>
    <head>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/loadingoverlay.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/vis.js"></script>
        <link href="<%=request.getContextPath()%>/css/vis-timeline-graph2d.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        
        <div id="mynetwork"></div>
        
        <script type="text/javascript">
            $.LoadingOverlay("show");
            var nodes = new vis.DataSet([
                <%=String.join(",",nodes)%>
            ]);
            var edges = new vis.DataSet([
                <%=String.join(",",edges)%>
            ]);
            var container = document.getElementById('mynetwork');
            var data = {
                nodes: nodes,
                edges: edges
            };
            var options = {
                manipulation: false,
                height: '100%',
                layout: {
                    hierarchical: {
                        enabled: true,
                        levelSeparation: 100
                    }
                },
                physics: {
                    hierarchicalRepulsion: {
                        nodeDistance: 100
                    }
                }
            };
            var network = new vis.Network(container, data, options);
            $.LoadingOverlay("hide");
        </script>
    </body>
</html>

<%
facade.close();
%>
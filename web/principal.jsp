<%-- 
    Document   : principal
    Created on : 12/07/2018, 10:03:53 PM
    Author     : CarlosAlberto
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OverFlow | Aplicaci�n de Procesos</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/font-awesome/css/font-awesome.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adminlte.css"/>
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/img/favicon.ico"/>
        <link href="<%=request.getContextPath()%>/css/bootstrap-dialog/bootstrap-dialog.css" rel="stylesheet" type="text/css"/>  
        <!-- DataTables -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/dataTables.bootstrap4.css"/>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.datetimepicker.css" type="text/css"/>
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <div class="nav-link" id="datetime"></div>
                    </li>
                </ul>
                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">   
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <%=((beans.Usuario)session.getAttribute("usuario")).getMail()%>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <span class="dropdown-item dropdown-header"><%=((beans.Usuario)session.getAttribute("usuario")).getNombre()%></span>
                            <div class="dropdown-divider"></div>
                            <span class="dropdown-item dropdown-header"><i class="fa fa-lock fa-fw"></i> <%=((beans.Rol)session.getAttribute("rol")).getNombre()%></span>
                            <span class="dropdown-item dropdown-header" onclick="javascript:go2to('Perfil/cambiar.jsp','page-wrapper');"><i class="fa fa-user-circle-o fa-fw"></i> Perfil de usuario</span>
                            <div class="dropdown-divider"></div>
                            <span class="dropdown-item dropdown-header" data-toggle="modal" data-target="#myModalLogout"><i class="fa fa-close fa-fw"></i> Cerrar Sesion</span>
                        </div>
                    </li>
                    <%if(((beans.Rol)session.getAttribute("rol")).getAdministra()==1){ %>
                        <li class="nav-item dropdown">
                            <a class="nav-link" data-toggle="dropdown" href="#">
                                <i class="fa fa-gears"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                <span class="dropdown-item dropdown-header" onclick="javascript:go2to('Configuracion/sesiones.jsp','page-wrapper');"><i class="fa fa-users fa-fw"></i> Conexiones en Overflow</span>
                                <span class="dropdown-item dropdown-header" onclick="javascript:go2to('Configuracion/opciones.jsp','page-wrapper');"><i class="fa fa-gear fa-fw"></i> Configuraciones especiales</span>
                            </div>
                        </li>
                    <%}%>
                    <li class="nav-item">
                        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i class="fa fa-th-large"></i></a>
                    </li>
                </ul>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="<%=request.getContextPath()%>/principal.jsp" class="brand-link">
                    <img src="<%=request.getContextPath()%>/img/logo.png"
                         alt="OverFlow Logo"
                         class="brand-image img-circle elevation-3"
                         style="opacity: .8">
                    <span class="brand-text font-weight-light">OverFlow 1.0</span>
                </a>
                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
                                 with font-awesome or any other icon font library -->
                            <li class="nav-item">
                                <a href="#" class="nav-link" id="dashboard" onclick="javascript:go2to('Dashboard/dashboard.jsp', 'page-wrapper');">
                                    <i class="nav-icon fa fa-dashboard"></i>
                                    <p>Dashboard</p>
                                </a>
                            </li>
                            <%if(((beans.Rol)session.getAttribute("rol")).getRedacta()==1 || ((beans.Rol)session.getAttribute("rol")).getConsulta()==1){%>
                                <li class="nav-item">
                                    <a href="#" class="nav-link" id="historico" onclick="javascript:go2to('Historico/flujoListado.jsp', 'page-wrapper');">
                                        <i class="nav-icon fa fa-clock-o"></i>
                                        <p>Historico</p>
                                    </a>
                                </li>
                            <%}%>
                            <%if(((beans.Rol)session.getAttribute("rol")).getRedacta()==1){%>
                                <li class="nav-item">
                                    <a href="#" class="nav-link" id="redactar" onclick="javascript:go2to('Redactar/flujoListado.jsp', 'page-wrapper');">
                                        <i class="nav-icon fa fa-file-text-o"></i>
                                        <p>Redactar</p>
                                    </a>
                                </li>
                            <%}%>
                            <%if(((beans.Rol)session.getAttribute("rol")).getDelega()==1){%>
                                <li class="nav-item">
                                    <a href="#" class="nav-link" id="delegar" onclick="javascript:go2to('Delega/tareaListado.jsp', 'page-wrapper');">
                                        <i class="nav-icon fa fa-hand-o-right"></i>
                                        <p>Delegar</p>
                                    </a>
                                </li>
                            <%}%>
                            <%if(((beans.Rol)session.getAttribute("rol")).getEjecuta()==1){%>
                                <li class="nav-item">
                                    <a href="#" class="nav-link" id="ejecutar" onclick="javascript:go2to('Ejecuta/tareaListado.jsp', 'page-wrapper');">
                                        <i class="nav-icon fa fa-inbox"></i>
                                        <p>Ejecutar</p>
                                    </a>
                                </li>
                            <%}%>
                            <%if(((beans.Rol)session.getAttribute("rol")).getAdministra()==1){%>
                                <li class="nav-item has-treeview">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon fa fa-briefcase"></i>
                                        <p>
                                            Catalogos
                                            <i class="fa fa-angle-left right"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link" id="grupos" onclick="javascript:go2to('Grupo/grupoListado.jsp', 'page-wrapper');">
                                                <i class="fa fa-users nav-icon"></i>
                                                <p>Grupos</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link" id="procesos" onclick="javascript:go2to('Proceso/procesoListado.jsp', 'page-wrapper');">
                                                <i class="fa fa-arrow-circle-o-right nav-icon"></i>
                                                <p>Procesos</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item has-treeview">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon fa fa-wrench"></i>
                                        <p>
                                            Sistema
                                            <i class="fa fa-angle-left right"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link" id="roles" onclick="javascript:go2to('Rol/rolListado.jsp', 'page-wrapper');">
                                                <i class="fa fa-key nav-icon"></i>
                                                <p>Roles</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link" id="usuarios" onclick="javascript:go2to('Usuario/usuarioListado.jsp', 'page-wrapper');">
                                                <i class="fa fa-user nav-icon"></i>
                                                <p>Usuarios</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <%}%>
                            <li class="nav-header">Conversaciones</li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" id="conversacion_alta" onclick="javascript:go2to('Conversacion/conversaciones.jsp?prioridad=0','page-wrapper');">
                                    <i class="nav-icon fa fa-comments text-danger"></i>
                                    <p class="text">Alta</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" id="conversacion_media" onclick="javascript:go2to('Conversacion/conversaciones.jsp?prioridad=1','page-wrapper');">
                                    <i class="nav-icon fa fa-comments text-warning"></i>
                                    <p class="text">Media</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" id="conversacion_baja" onclick="javascript:go2to('Conversacion/conversaciones.jsp?prioridad=2','page-wrapper');">
                                    <i class="nav-icon fa fa-comments text-info"></i>
                                    <p class="text">Baja</p>
                                </a>
                            </li>
                            <li class="nav-header">Recursos</li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" id="manuales" onclick="javascript:go2to('Recurso/recursos.jsp?tipo=0','page-wrapper');">
                                    <i class="nav-icon fa fa-book"></i>
                                    <p class="text">Manuales</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" id="documentos" onclick="javascript:go2to('Recurso/recursos.jsp?tipo=1','page-wrapper');">
                                    <i class="nav-icon fa fa-file-archive-o"></i>
                                    <p class="text">Documentos</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link" id="aplicaciones" onclick="javascript:go2to('Recurso/recursos.jsp?tipo=2','page-wrapper');">
                                    <i class="nav-icon fa fa-desktop"></i>
                                    <p class="text">Aplicaciones</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" id="page-wrapper"></div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="float-right d-none d-sm-block">
                    <b>Version</b> 1.0 Producci�n
                </div>
                <strong>Copyright &copy; 2018-2024
            </footer>
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        <!-- jQuery -->
        <script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<%=request.getContextPath()%>/js/bootstrap.bundle.min.js"></script>
        <!-- FastClick -->
        <script src="<%=request.getContextPath()%>/js/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<%=request.getContextPath()%>/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<%=request.getContextPath()%>/js/demo.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/control.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/interpretador.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/loadingoverlay.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.datetimepicker.full.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/dataTables.bootstrap4.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-dialog/bootstrap-dialog.js"></script>
        <script lang="javascript">

            function date_time(id) {        
                date = new Date;
                year = date.getFullYear();
                month = date.getMonth();
                months = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                d = date.getDate();
                day = date.getDay();
                days = new Array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
                h = date.getHours();
                if(h<10) { h = "0"+h; }
                m = date.getMinutes();
                if(m<10) { m = "0"+m; }
                s = date.getSeconds();
                if(s<10) { s = "0"+s; }
                result = ''+days[day]+' '+d+' '+months[month]+' '+year+' '+h+':'+m+':'+s;
                document.getElementById(id).innerHTML = result;
                setTimeout('date_time("'+id+'");','1000');
                return true;
            }

            date_time('datetime');
            
            go2to('Dashboard/dashboard.jsp', 'page-wrapper');
            
        </script>   
        <div class="modal fade" id="myModalLogout">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Salir de Overflow</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        �Confirma desea salir de Overflow?
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <a href="<%=request.getContextPath()%>/index.jsp" class="btn btn-outline-warning">Salir</a>
                        <button type="button" class="btn btn-outline-secundary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>                                
    </body>
</html>

<%-- 
    Document   : error
    Created on : Oct 10, 2014, 11:26:57 PM
    Author     : CarlosAlberto
--%>

<jsp:useBean id="facade" scope="page" class="facades.RegistroFacade"/>
<jsp:useBean id="ffacade" scope="page" class="facades.FlujoFacade"/>

<%int idflujo = 0;
if(request.getParameter("idflujo")!=null){
    idflujo = Integer.parseInt(String.valueOf(request.getParameter("idflujo")));
}%>

<html>
    <head>
        <title>Visor de Registros</title>
        <!-- Core CSS - Include with every page -->
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/adminlte.css"/>
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700"/>
        
    </head>
    <body>
        <div class="list-group" id="lista">
            <%for(beans.Registro registro:facade.getRegistroByFlujo(idflujo)){%>
                <a href="#" class="list-group-item">
                    <i class="fa fa-file-text-o fa-fw"></i> <strong><%=registro.getDispara()%>:</strong> <em><%=registro.getAccion()%></em><br/><%=registro.getObjetivo()%>
                    <span class="pull-right text-muted small"><em><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(registro.getFecha())%></em>
                    </span>
                </a>
            <%}%>
        </div>
        <!-- Core Scripts - Include with every page -->
        <!-- jQuery -->
        <script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<%=request.getContextPath()%>/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap-dialog/bootstrap-dialog.js"></script>
    </body>
</html>
    
<%
facade.close();
ffacade.close();
%>
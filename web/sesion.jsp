<%-- 
    Document   : error
    Created on : Oct 10, 2014, 11:26:57 PM
    Author     : CarlosAlberto
--%>


<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Evento del sistema detectado</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active"><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())%></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fa fa-info"></i> Su sesion ha terminado...</h5>
                    El sistema solo permite mantener una sesion inactiva por 30 minutos, por favor vuelva a iniciar sesion. <a href="<%=new properties.PropertiesManager().getProperty("URL")%>">IR A LOGIN</a>
                </div>
            </div>
        </div>
    </div>
</section>